#include "aabb.hpp"

#include <pybind11/eigen.h>
#include <limits>

#include "eigen_common.hpp"

#include "sat.hpp"

namespace slamtb {
AABB::AABB(const Eigen::Vector3f &p0, const Eigen::Vector3f &p1) {
  min_[0] = std::min(p0[0], p1[0]);
  min_[1] = std::min(p0[1], p1[1]);
  min_[2] = std::min(p0[2], p1[2]);

  max_[0] = std::max(p0[0], p1[0]);
  max_[1] = std::max(p0[1], p1[1]);
  max_[2] = std::max(p0[2], p1[2]);
}

AABB::AABB(const torch::Tensor &points) {
  const auto mi = std::get<0>(points.min(0));
  const auto ma = std::get<0>(points.max(0));
  min_ = from_tensorv3f(mi.cpu());
  max_ = from_tensorv3f(ma.cpu());
}

void AABB::RegisterPybind(pybind11::module &m) {
  pybind11::class_<AABB>(m, "AABB")
      .def(pybind11::init<const Eigen::Vector3f &, const Eigen::Vector3f &>())
      .def("is_inside", &AABB::IsInside)
      .def("intersecs",
           pybind11::overload_cast<const Eigen::Vector3f &, float>(
               &AABB::Intersects, pybind11::const_),
           "Tests intersection with a sphere")
      .def("intersecs",
           pybind11::overload_cast<const Eigen::Vector3f &,
                                   const Eigen::Vector3f &,
                                   const Eigen::Vector3f &>(&AABB::Intersects,
                                                            pybind11::const_),
           "Tests intersection with a triangle")
      .def("intersecs",
           pybind11::overload_cast<const AABB &>(&AABB::Intersects,
                                                 pybind11::const_),
           "Tests intersection with a other AABB")
      .def("intersection", &AABB::Intersection)
      .def("get_closest_point", &AABB::GetClosestPoint)
      .def("transform", &AABB::Transform)
      .def("area", &AABB::GetArea)
      .def_property_readonly("min", &AABB::get_min)
      .def_property_readonly("max", &AABB::get_max)
      .def("__str__", &AABB::__str__)
      .def("__repr__", &AABB::__str__);
}

float AABB::GetArea() const {
  return (max_[0] - min_[0]) * (max_[1] - min_[1]) * (max_[2] - min_[2]);
}

bool AABB::IsInside(const Eigen::Vector3f &point) const {
  if (point[0] < min_[0] || point[1] < min_[1] || point[2] < min_[2])
    return false;

  if (point[0] > max_[0] || point[1] > max_[1] || point[2] > max_[2])
    return false;

  return true;
}

bool AABB::Intersects(const Eigen::Vector3f &point, float radius) const {
  Eigen::Vector3f closest(GetClosestPoint(point));

  const Eigen::Vector3f v = point - closest;
  return radius * radius > v.squaredNorm();
}

bool AABB::Intersects(const Eigen::Vector3f &p0, const Eigen::Vector3f &p1,
                      const Eigen::Vector3f &p2) const {
  const Eigen::Vector3f v0 = p1 - p0;
  const Eigen::Vector3f v1 = p2 - p1;
  const Eigen::Vector3f v2 = p0 - p2;

  const Eigen::Vector3f bn0(1.0f, 0.0f, 0.0f);
  const Eigen::Vector3f bn1(0.0f, 1.0f, 0.0f);
  const Eigen::Vector3f bn2(0.0f, 0.0f, 1.0f);

  const Eigen::Vector3f sep_axis[] = {
      bn0,           bn1,           bn2,           v0.cross(v1),  bn0.cross(v0),
      bn0.cross(v1), bn0.cross(v2), bn1.cross(v0), bn1.cross(v1), bn1.cross(v2),
      bn2.cross(v0), bn2.cross(v1), bn2.cross(v2)};

  for (int i = 0; i < 13; ++i) {
    const Eigen::Vector3f axis = sep_axis[i];
    SATInterval aabb_interval = GetAABBInterval(*this, axis);
    SATInterval trig_interval = GetTriangleInterval(p0, p1, p2, axis);

    if (!aabb_interval.HasOverlap(trig_interval)) {
      return false;
    }
  }

  return true;
}

bool AABB::Intersects(const AABB &other) const {
  return (min_[0] <= other.max_[0] && max_[0] >= other.min_[0]) &&
         (min_[1] <= other.max_[1] && max_[1] >= other.min_[1]) &&
         (min_[2] <= other.max_[2] && max_[2] >= other.min_[2]);
}

AABB AABB::Intersection(const AABB &other) const {
  if (!Intersects(other)) {
    return AABB();
  }

  return AABB(Eigen::Vector3f(std::max(min_[0], other.min_[0]),
                              std::max(min_[1], other.min_[1]),
                              std::max(min_[2], other.min_[2])),
              Eigen::Vector3f(std::min(max_[0], other.max_[0]),
                              std::min(max_[1], other.max_[1]),
                              std::min(max_[2], other.max_[2])));
}

Eigen::Vector3f AABB::GetClosestPoint(const Eigen::Vector3f &point) const {
  Eigen::Vector3f result = point;
  result[0] = (result[0] < min_[0]) ? min_[0] : result[0];
  result[1] = (result[1] < min_[1]) ? min_[1] : result[1];
  result[2] = (result[2] < min_[2]) ? min_[2] : result[2];

  result[0] = (result[0] > max_[0]) ? max_[0] : result[0];
  result[1] = (result[1] > max_[1]) ? max_[1] : result[1];
  result[2] = (result[2] > max_[2]) ? max_[2] : result[2];
  return result;
}

AABB AABB::Transform(const Eigen::Matrix4f &transform_matrix) const {
  const Eigen::Vector3f points[8] = {
      // Lower
      Eigen::Vector3f(min_[0], min_[1], min_[2]),
      Eigen::Vector3f(min_[0], min_[1], max_[2]),
      Eigen::Vector3f(max_[0], min_[1], max_[2]),
      Eigen::Vector3f(max_[0], min_[1], min_[2]),

      // Upper
      Eigen::Vector3f(min_[0], max_[1], min_[2]),
      Eigen::Vector3f(min_[0], max_[1], max_[2]),
      Eigen::Vector3f(max_[0], max_[1], max_[2]),
      Eigen::Vector3f(max_[0], max_[1], min_[2]),
  };
  Eigen::Affine3f transform;
  transform.matrix() = transform_matrix;

  Eigen::Vector3f new_min(std::numeric_limits<float>::infinity(),
                          std::numeric_limits<float>::infinity(),
                          std::numeric_limits<float>::infinity());
  Eigen::Vector3f new_max(-std::numeric_limits<float>::infinity(),
                          -std::numeric_limits<float>::infinity(),
                          -std::numeric_limits<float>::infinity());

  for (int k = 0; k < 8; ++k) {
    const auto point = transform * points[k];
    for (int l = 0; l < 3; ++l) {
      new_min[l] = std::min(new_min[l], point[l]);
      new_max[l] = std::max(new_max[l], point[l]);
    }
  }

  return AABB(new_min, new_max);
}

std::string AABB::__str__() const {
  std::stringstream str;
  str << "AABB(min=[" << min_[0] << ", " << min_[1] << ", " << min_[2]
      << ", ], max=[" << max_[0] << ", " << max_[1] << ", " << max_[2] << "])";
  return str.str();
}
}  // namespace slamtb
