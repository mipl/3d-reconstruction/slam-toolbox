#include "point_cloud.hpp"

namespace slamtb {
void PointCloudBase::RegisterPybind(pybind11::module &m) {
  pybind11::class_<PointCloudBase>(m, "PointCloudBase")
      .def(pybind11::init<torch::Tensor, std::optional<torch::Tensor>,
                          std::optional<torch::Tensor>,
                          std::optional<torch::Tensor> >())
      .def("as_dict", &PointCloudBase::AsDict)
      .def("from_dict", &PointCloudBase::FromDict)
      .def(py::pickle(
          [](const PointCloudBase &self) {  //__getstate__
            return self.AsDict();
          },
          [](py::dict dict) {  // __setstate__
            PointCloudBase self;
            self.FromDict(dict);
            return self;
          }))
      .def_readwrite("points", &PointCloudBase::points)
      .def_readwrite("colors", &PointCloudBase::colors)
      .def_readwrite("normals", &PointCloudBase::normals)
      .def_readwrite("features", &PointCloudBase::features);
}

py::dict PointCloudBase::AsDict() const {
  py::dict dict;

  dict["points"] = points;

  if (colors) dict["colors"] = *colors;

  if (normals) dict["normals"] = *normals;

  if (features) dict["features"] = *features;

  return dict;
}

void PointCloudBase::FromDict(const py::dict &dict) {
  points = py::cast<torch::Tensor>(dict["points"]);

  if (dict.contains("colors")) {
    colors = py::cast<torch::Tensor>(dict["colors"]);
  }

  if (dict.contains("normals")) {
    normals = py::cast<torch::Tensor>(dict["normals"]);
  }

  if (dict.contains("features")) {
    features = py::cast<torch::Tensor>(dict["features"]);
  }
}

}  // namespace slamtb
