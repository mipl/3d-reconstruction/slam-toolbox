#include "feature_interpolation.hpp"

#include "camera.hpp"
#include "error.hpp"
#include "feature_map.hpp"
#include "kernel.hpp"

namespace slamtb {
namespace {

template <Device dev, typename scalar_t>
struct ForwardKernel {
  const FeatureMapAccessor<dev, scalar_t> tgt_featmap;
  const typename Accessor<dev, scalar_t, 2>::T source_points;
  const KCamera<dev, scalar_t> kcam;

  typename Accessor<dev, scalar_t, 2>::T out_features;

  ForwardKernel(const FeatureMapAccessor<dev, scalar_t> tgt_featmap,
                const torch::Tensor &source_points, const torch::Tensor &kcam,
                torch::Tensor out_features)
      : tgt_featmap(tgt_featmap),
        source_points(Accessor<dev, scalar_t, 2>::Get(source_points)),
        kcam(kcam),
        out_features(Accessor<dev, scalar_t, 2>::Get(out_features)) {}

  STB_DEVICE_HOST void operator()(int source_index) {
    const Vector<scalar_t, 3> src_point =
        to_vec3<scalar_t>(source_points[source_index]);

    scalar_t u, v;
    kcam.Project(src_point, u, v);

    const auto interp = tgt_featmap.GetInterpolator(u, v);
    for (int channel = 0; channel < tgt_featmap.channel_size; ++channel) {
      scalar_t val = interp.Get(channel);
      out_features[channel][source_index] = val;
    }
  }
};

}  // namespace

void FeatureProjectionInterpolation::Forward(const FeatureMap &target_features,
                                             const torch::Tensor &source_points,
                                             const torch::Tensor &kcam,
                                             torch::Tensor out_features) {
  const auto reference_dev = source_points.device();
  STB_CHECK_DEVICE(reference_dev, kcam);
  STB_CHECK_DEVICE(reference_dev, out_features);

  if (reference_dev.is_cuda()) {
    AT_DISPATCH_FLOATING_TYPES(
        source_points.scalar_type(), "FeatureProjectionInterpolation::Forward",
        ([&] {
          ForwardKernel<kCUDA, scalar_t> kernel(
              target_features.get_cuda_accessor<scalar_t>(), source_points,
              kcam, out_features);

          Launch1DKernelCUDA(kernel, source_points.size(0));
        }));
  } else {
    AT_DISPATCH_FLOATING_TYPES(
        source_points.scalar_type(), "FeatureProjectionInterpolation::Forward",
        ([&] {
          ForwardKernel<kCPU, scalar_t> kernel(
              target_features.get_cpu_accessor<scalar_t>(), source_points, kcam,
              out_features);

          Launch1DKernelCPU(kernel, source_points.size(0));
        }));
  }
}

namespace {
template <Device dev, typename scalar_t>
struct BackwardKernel {
  const FeatureMapAccessor<dev, scalar_t> tgt_featmap;

  const typename Accessor<dev, scalar_t, 2>::T source_points;

  const typename Accessor<dev, scalar_t, 2>::T dl_feature;

  const KCamera<dev, scalar_t> kcam;
  const scalar_t grad_precision;

  typename Accessor<dev, scalar_t, 2>::T dx_points;

  BackwardKernel(const FeatureMapAccessor<dev, scalar_t> tgt_featmap,
                 const torch::Tensor &source_points,
                 const torch::Tensor &dl_feature, const torch::Tensor &kcam,
                 scalar_t grad_precision, torch::Tensor dx_points)
      : tgt_featmap(tgt_featmap),
        source_points(Accessor<dev, scalar_t, 2>::Get(source_points)),
        dl_feature(Accessor<dev, scalar_t, 2>::Get(dl_feature)),
        kcam(kcam),
        grad_precision(grad_precision),
        dx_points(Accessor<dev, scalar_t, 2>::Get(dx_points)) {}

  STB_DEVICE_HOST void operator()(int idx) {
    const Vector<scalar_t, 3> src_point = to_vec3<scalar_t>(source_points[idx]);

    scalar_t u, v;
    kcam.Project(src_point, u, v);

    auto grad = tgt_featmap.GetInterpolatorGradient(u, v, grad_precision);

    scalar_t dl_ugrad = 0;
    scalar_t dl_vgrad = 0;
    for (int channel = 0; channel < tgt_featmap.channel_size; ++channel) {
      scalar_t du, dv;

      grad.Get(channel, du, dv);

      const scalar_t channel_dl = dl_feature[channel][idx];

      dl_ugrad += du * channel_dl;
      dl_vgrad += dv * channel_dl;
    }

    scalar_t j00, j02, j11, j12;
    kcam.Dx_Project(src_point, j00, j02, j11, j12);

    dx_points[idx][0] = j00 * dl_ugrad;
    dx_points[idx][1] = j11 * dl_vgrad;
    dx_points[idx][2] = j02 * dl_ugrad + j12 * dl_vgrad;
  }
};

}  // namespace

void FeatureProjectionInterpolation::Backward(
    const FeatureMap &target_features, const torch::Tensor &source_points,
    const torch::Tensor &dl_feature, const torch::Tensor &kcam,
    double grad_precision, torch::Tensor dx_points) {
  const auto reference_dev = source_points.device();
  STB_CHECK_DEVICE(reference_dev, kcam);
  STB_CHECK_DEVICE(reference_dev, dx_points);

  if (reference_dev.is_cuda()) {
    AT_DISPATCH_FLOATING_TYPES(
        source_points.scalar_type(), "FeatureProjectionInterpolation::Backward",
        [&] {
          BackwardKernel<kCUDA, scalar_t> kernel(
              target_features.get_cuda_accessor<scalar_t>(), source_points,
              dl_feature, kcam, grad_precision, dx_points);

          KernelLauncher<kCUDA>::Launch1D(kernel, source_points.size(0));
        });
  } else {
    AT_DISPATCH_FLOATING_TYPES(
        source_points.scalar_type(), "FeatureProjectionInterpolation::Backward",
        [&] {
          BackwardKernel<kCPU, scalar_t> kernel(
              target_features.get_cpu_accessor<scalar_t>(), source_points,
              dl_feature, kcam, grad_precision, dx_points);

          KernelLauncher<kCPU>::Launch1D(kernel, source_points.size(0));
        });
  }
}

}  // namespace slamtb
