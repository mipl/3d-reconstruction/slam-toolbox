#include "point_volume.hpp"

#include <cuda_runtime.h>
#include <pybind11/eigen.h>

#include "kernel.hpp"
#include "point_cloud.hpp"
#include "vector_accessor.hpp"

namespace slamtb {

void PointVolumeBase::RegisterPybind(pybind11::module &m) {
  pybind11::class_<PointVolumeBase>(m, "PointVolumeBase")
      .def(pybind11::init<const Eigen::Vector3f &, const Eigen::Vector3f &,
                          float, int>())
      .def("accumulate", &PointVolumeBase::Accumulate)
      .def("to_point_cloud", &PointVolumeBase::ToPointCloud)
      .def_property_readonly("size", &PointVolumeBase::get_size)
      .def_property_readonly("feature_size",
                             &PointVolumeBase::get_feature_size);
}

struct AccumPointsKernel {
  const VectorTensorAccessor<kCPU, float, 3> points;
  HashVolume<PointVolumeBase::Point> &volume;
  torch::TensorAccessor<int64_t, 1> hashes;

  AccumPointsKernel(const torch::Tensor &points,
                    HashVolume<PointVolumeBase::Point> &volume,
                    torch::Tensor hashes)
      : points(points), volume(volume), hashes(hashes.accessor<int64_t, 1>()) {}

  void operator()(int idx) {
    const Eigen::Vector3f point = points[idx];

    int64_t hash = volume.Hash(point);
    PointVolumeBase::Point &point_elem = volume[hash];

    point_elem.point += point;
    point_elem.count += 1;

    hashes[idx] = hash;
  }
};

struct AccumColorsKernel {
  const VectorTensorAccessor<kCPU, uint8_t, 3> colors;
  const torch::TensorAccessor<int64_t, 1> hashes;
  HashVolume<PointVolumeBase::Point> &volume;

  AccumColorsKernel(const torch::Tensor &colors, const torch::Tensor &hashes,
                    HashVolume<PointVolumeBase::Point> &volume)
      : colors(colors), hashes(hashes.accessor<int64_t, 1>()), volume(volume) {}

  void operator()(int idx) {
    PointVolumeBase::Point &point_elem = volume[hashes[idx]];
    const auto color = colors[idx];
    point_elem.color += Eigen::Vector3f(color[0], color[1], color[2]);
  }
};

struct AccumNormalsKernel {
  const torch::TensorAccessor<float, 2> normals;
  const torch::TensorAccessor<int64_t, 1> hashes;
  HashVolume<PointVolumeBase::Point> &volume;

  AccumNormalsKernel(const torch::Tensor &normals, const torch::Tensor &hashes,
                     HashVolume<PointVolumeBase::Point> &volume)
      : normals(normals.accessor<float, 2>()),
        hashes(hashes.accessor<int64_t, 1>()),
        volume(volume) {}

  void operator()(int idx) {
    PointVolumeBase::Point &point_elem = volume[hashes[idx]];
    point_elem.normal += to_vec3<float>(normals[idx]);
  }
};

struct AccumFeaturesKernel {
  const torch::TensorAccessor<float, 2> features;
  const torch::TensorAccessor<int64_t, 1> hashes;
  HashVolume<PointVolumeBase::Point> &volume;
  const int feature_size;

  AccumFeaturesKernel(const torch::Tensor &features,
                      const torch::Tensor &hashes,
                      HashVolume<PointVolumeBase::Point> &volume,
                      int feature_size)
      : features(features.accessor<float, 2>()),
        hashes(hashes.accessor<int64_t, 1>()),
        volume(volume),
        feature_size(feature_size) {}

  void operator()(int idx) {
    PointVolumeBase::Point &point_elem = volume[hashes[idx]];

    point_elem.AllocateFeature(feature_size, torch::kCPU);
    auto acc = point_elem.feature.accessor<float, 1>();
    for (int64_t i = 0; i < feature_size; ++i) {
      acc[i] += features[i][idx];
    }
  }
};

void PointVolumeBase::Accumulate(const PointCloudBase &point_cloud) {
  torch::Tensor hashes = torch::empty({point_cloud.get_size()},
                                      torch::TensorOptions(torch::kInt64));

  {
    AccumPointsKernel accum_kernel(point_cloud.points, volume_, hashes);
    KernelLauncher<kCPU>::Launch1D(accum_kernel, point_cloud.get_size(), true);
  }

  if (point_cloud.colors) {
    AccumColorsKernel kernel(*point_cloud.colors, hashes, volume_);
    KernelLauncher<kCPU>::Launch1D(kernel, point_cloud.get_size(), true);
  }

  if (point_cloud.normals) {
    AccumNormalsKernel kernel(*point_cloud.normals, hashes, volume_);
    KernelLauncher<kCPU>::Launch1D(kernel, point_cloud.get_size(), true);
  }

  if (point_cloud.features) {
    AccumFeaturesKernel kernel(*point_cloud.features, hashes, volume_,
                               feature_size_);
    KernelLauncher<kCPU>::Launch1D(kernel, point_cloud.get_size(), true);
  }
}

struct ToCloudKernel {
  const HashVolume<PointVolumeBase::Point> &volume;
  const torch::TensorAccessor<int64_t, 1> hashes;
  VectorTensorAccessor<kCPU, float, 3> points;
  VectorTensorAccessor<kCPU, uint8_t, 3> colors;
  VectorTensorAccessor<kCPU, float, 3> normals;

  ToCloudKernel(const HashVolume<PointVolumeBase::Point> &volume,
                const torch::Tensor &hashes, torch::Tensor points,
                torch::Tensor colors, torch::Tensor normals)
      : volume(volume),
        hashes(hashes.accessor<int64_t, 1>()),
        points(points),
        colors(colors),
        normals(normals) {}

  void operator()(int idx) {
    const int64_t voxel_id = hashes[idx];

    HashVolume<PointVolumeBase::Point>::const_iterator found =
        volume.FindId(voxel_id);
    if (found == volume.end()) {
      return;
    }
    const PointVolumeBase::Point accum = found->second;
    const float inv_count = 1.0f / float(accum.count);
    points.set(idx, accum.point * inv_count);
    normals.set(idx, accum.normal * inv_count);
    const Eigen::Vector3f colorf = accum.color * inv_count;
    const Eigen::Matrix<uint8_t, 3, 1> colorb(colorf[0], colorf[1], colorf[2]);
    colors.set(idx, colorb);
  }
};

struct ToCloudFeaturesKernel {
  const HashVolume<PointVolumeBase::Point> &volume;
  const torch::TensorAccessor<int64_t, 1> hashes;
  torch::TensorAccessor<float, 2> features;

  ToCloudFeaturesKernel(const HashVolume<PointVolumeBase::Point> &volume,
                        const torch::Tensor &hashes, torch::Tensor features)
      : volume(volume),
        hashes(hashes.accessor<int64_t, 1>()),
        features(features.accessor<float, 2>()) {}

  void operator()(int idx) {
    const int64_t voxel_id = hashes[idx];

    HashVolume<PointVolumeBase::Point>::const_iterator found =
        volume.FindId(voxel_id);
    if (found == volume.end()) {
      return;
    }
    const PointVolumeBase::Point accum = found->second;
    if (!accum.feature.has_storage()) return;

    const float inv_count = 1.0f / float(accum.count);

    const auto acc = accum.feature.accessor<float, 1>();
    for (int64_t i = 0; i < features.size(0); ++i) {
      features[i][idx] = acc[i] * inv_count;
    }
  }
};

void PointVolumeBase::ToPointCloud(PointCloudBase &out) const {
  torch::Tensor voxel_ids = volume_.GetVoxelIDs();
  {
    ToCloudKernel kernel(volume_, voxel_ids, out.points, *out.colors,
                         *out.normals);
    KernelLauncher<kCPU>::Launch1D(kernel, voxel_ids.size(0), true);
  }

  if (out.features) {
    ToCloudFeaturesKernel kernel(volume_, voxel_ids, *out.features);
    KernelLauncher<kCPU>::Launch1D(kernel, voxel_ids.size(0), true);
  }
}

}  // namespace slamtb
