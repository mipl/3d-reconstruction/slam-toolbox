#include "surfel_fusion.hpp"

#include <cuda_runtime.h>
#include <pybind11/eigen.h>

#include "cuda_utils.hpp"
#include "surfel_accessor.hpp"

namespace py = pybind11;

namespace slamtb {

void SurfelIndexMap::Synchronize() { CudaSafeCall(cudaDeviceSynchronize()); }

void SurfelIndexMap::RegisterPybind(py::module &m) {
  py::class_<SurfelIndexMap>(m, "IndexMap")
      .def(py::init())
      .def("to", &SurfelIndexMap::To)
      .def("synchronize", &SurfelIndexMap::Synchronize)
      .def_readwrite("point_confidence", &SurfelIndexMap::point_confidence)
      .def_readwrite("normal_radius", &SurfelIndexMap::normal_radius)
      .def_readwrite("color", &SurfelIndexMap::color)
      .def_readwrite("indexmap", &SurfelIndexMap::indexmap)
      .def_property("width", &SurfelIndexMap::get_width, nullptr)
      .def_property("height", &SurfelIndexMap::get_height, nullptr)
      .def_property("device", &SurfelIndexMap::get_device, nullptr);
}

void SurfelModel::RegisterPybind(py::module &m) {
  py::class_<SurfelModel>(m, "SurfelModel")
      .def(py::init())
      .def_readwrite("points", &SurfelModel::points)
      .def_readwrite("confidences", &SurfelModel::confidences)
      .def_readwrite("normals", &SurfelModel::normals)
      .def_readwrite("radii", &SurfelModel::radii)
      .def_readwrite("colors", &SurfelModel::colors)
      .def_readwrite("times", &SurfelModel::times)
      .def_readwrite("features", &SurfelModel::features);
}

void SurfelFusionOp::RegisterPybind(py::module &m) {
  py::class_<SurfelFusionOp>(m, "SurfelFusionOp")
      .def_static("update", &SurfelFusionOp::Update)
      .def_static("find_updatable", &SurfelFusionOp::FindUpdatable)
      .def_static("carve_space", &SurfelFusionOp::CarveSpace)
      .def_static("find_mergeable", &SurfelFusionOp::FindMergeable)
      .def_static("merge", &SurfelFusionOp::Merge)
      .def_static("clean", &SurfelFusionOp::Clean);
}

}  // namespace slamtb
