#include "feature_interpolation.hpp"

#include "feature_map.hpp"

namespace slamtb {

void FeatureProjectionInterpolation::RegisterPybind(pybind11::module &m) {
  pybind11::class_<FeatureProjectionInterpolation>(
      m, "FeatureProjectionInterpolation")
      .def_static("forward", &FeatureProjectionInterpolation::Forward)
      .def_static("backward", &FeatureProjectionInterpolation::Backward);
}

void FeatureKNNInterpolation::RegisterPybind(pybind11::module &m) {
  pybind11::class_<FeatureKNNInterpolation>(m, "FeatureKNNInterpolation")
      .def_static("forward", &FeatureKNNInterpolation::Forward)
      .def_static("compute_epsilon_distances",
                  &FeatureKNNInterpolation::ComputeEpsilonDistances)
      .def_static("compute_dxyz_features",
                  &FeatureKNNInterpolation::ComputeDxyzFeatures)
      .def_static("backward", &FeatureKNNInterpolation::Backward);
}
}  // namespace slamtb
