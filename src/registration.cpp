#include "registration.hpp"

#include <pybind11/eigen.h>

namespace slamtb {
void Registration::RegisterPybind(pybind11::module &m) {
  py::class_<Registration>(m, "Registration")
      .def_static("compute_information_matrix",
                  &Registration::ComputeInformationMatrix);
}
}  // namespace slamtb
