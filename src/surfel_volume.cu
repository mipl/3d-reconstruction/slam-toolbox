#include "surfel_volume.hpp"

#include "aabb.hpp"
#include "accessor.hpp"
#include "kernel.hpp"
#include "math.hpp"
#include "surfel_accessor.hpp"

namespace slamtb {

namespace {
struct AccumKernel {
  const SurfelCloudAccessor<kCPU> surfels;
  const int64_t feature_size;
  HashVolume<SurfelVolume::Surfel> &volume;

  AccumKernel(const SurfelCloud &surfels,
              HashVolume<SurfelVolume::Surfel> &volume)
      : surfels(surfels),
        feature_size(surfels.get_feature_size()),
        volume(volume) {}

  void operator()(int idx) {
    const Eigen::Vector3f point = surfels.point(idx);
    SurfelVolume::Surfel &surfel = volume(point);

    surfel.point += point;
    surfel.normal += surfels.normal(idx);
    surfel.color += surfels.color(idx);
    surfel.confidence += surfels.confidences[idx];
    surfel.radius += surfels.radii[idx];

    if (surfel.time > -1)
      surfel.time = min(surfel.time, surfels.times[idx]);
    else
      surfel.time = surfels.times[idx];

    if (feature_size > 0) {
      surfel.AllocateFeature(feature_size, torch::kCPU);
      auto acc = surfel.feature.accessor<float, 1>();
      for (int64_t i = 0; i < feature_size; ++i) {
        acc[i] += surfels.features[i][idx];
      }
    }

    surfel.count += 1;
  }
};

}  // namespace

void SurfelVolume::Accumulate(const SurfelCloud &surfels) {
  AccumKernel accum_kernel(surfels, volume_);
  KernelLauncher<kCPU>::Launch1D(accum_kernel, surfels.get_size(), true);
}

namespace {
struct ToCloudKernel {
  const HashVolume<SurfelVolume::Surfel> &volume;
  const typename Accessor<kCPU, int64_t, 1>::T voxel_ids;
  SurfelCloudAccessor<kCPU> out_surfels;
  const int feature_size;

  ToCloudKernel(const HashVolume<SurfelVolume::Surfel> &volume,
                const torch::Tensor &voxel_ids, SurfelCloud &out_surfels)
      : volume(volume),
        voxel_ids(Accessor<kCPU, int64_t, 1>::Get(voxel_ids)),
        out_surfels(out_surfels),
        feature_size(out_surfels.get_feature_size()) {}

  void operator()(int idx) {
    const int64_t voxel_id = voxel_ids[idx];

    HashVolume<SurfelVolume::Surfel>::const_iterator found =
        volume.FindId(voxel_id);
    if (found == volume.end()) {
      return;
    }
    const SurfelVolume::Surfel accum = found->second;
    const float inv_count = 1.0f / float(accum.count);
    out_surfels.set_point(idx, accum.point * inv_count);
    out_surfels.set_normal(idx, accum.normal * inv_count);
    out_surfels.set_color(idx, accum.color * inv_count);
    out_surfels.confidences[idx] = accum.confidence * inv_count;
    out_surfels.radii[idx] = accum.radius * inv_count;
    out_surfels.times[idx] = accum.time;

    if (accum.feature.has_storage()) {
      const auto acc = accum.feature.accessor<float, 1>();
      for (int64_t i = 0; i < feature_size; ++i) {
        out_surfels.features[i][idx] = acc[i] * inv_count;
      }
    }
  }
};
}  // namespace

void SurfelVolume::ToSurfelCloud(SurfelCloud &out) const {
  torch::Tensor voxel_ids = volume_.GetVoxelIDs();
  out.Allocate(voxel_ids.size(0), feature_size_, torch::kCPU);

  ToCloudKernel to_cloud_kernel(volume_, voxel_ids, out);
  KernelLauncher<kCPU>::Launch1D(to_cloud_kernel, voxel_ids.size(0), true);
}
}  // namespace slamtb
