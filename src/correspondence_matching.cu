#include "matching.hpp"

#include "accessor.hpp"
#include "correspondence_matching.hpp"
#include "error.hpp"
#include "index_map.hpp"
#include "kernel.hpp"

namespace slamtb {
namespace {
template <Device dev, typename scalar_t>
struct MatchByProjectionKernel {
  const typename Accessor<dev, scalar_t, 2>::T source_points;
  const typename Accessor<dev, scalar_t, 2>::T source_normals;
  const RigidTransform<scalar_t> rt_cam;

  RobustCorrespondence<dev, scalar_t> corresp;
  IndexMap<dev> index_map;

  MatchByProjectionKernel(const torch::Tensor &source_points,
                          const torch::Tensor &source_normals,
                          const torch::Tensor &rt_cam,
                          const torch::Tensor &target_points,
                          const torch::Tensor &target_normals,
                          const torch::Tensor &target_mask,
                          const torch::Tensor &kcam, torch::Tensor index_map,
                          double distance_thresh, double normal_angle_thesh)
      : source_points(Accessor<dev, scalar_t, 2>::Get(source_points)),
        source_normals(Accessor<dev, scalar_t, 2>::Get(source_normals)),
        rt_cam(rt_cam),
        corresp(target_points, target_normals, target_mask, kcam,
                distance_thresh, normal_angle_thesh),
        index_map(index_map) {}

#pragma nv_exec_check_disable
  STB_DEVICE_HOST void operator()(int idx) {
    const Vector<scalar_t, 3> Tsrc_point =
        rt_cam.Transform(to_vec3<scalar_t>(source_points[idx]));
    const Vector<scalar_t, 3> Tsrc_normal =
        rt_cam.TransformNormal(to_vec3<scalar_t>(source_normals[idx]));

    scalar_t u, v;
    if (!corresp.Match(Tsrc_point, Tsrc_normal, u, v)) {
      return;
    }

    const int ui = int(round(u));
    const int vi = int(round(v));

    const Vector<scalar_t, 3> target(
        to_vec3<scalar_t>(corresp.tgt.points[vi][ui]));
    const scalar_t dist = (target - Tsrc_point).squaredNorm();
    index_map.SetIfLess(vi, ui, float(dist), idx);
  }
};

}  // namespace

void CorrespondenceMatching::MatchByProjection(
    const torch::Tensor &source_points, const torch::Tensor &source_normals,
    const torch::Tensor &rt_cam, const torch::Tensor &target_points,
    const torch::Tensor &target_normals, const torch::Tensor &target_mask,
    const torch::Tensor &kcam, torch::Tensor index_map, double distance_thresh,
    double normal_angle_thesh) {
  const auto reference_dev = source_points.device();

  STB_CHECK_DEVICE(reference_dev, source_normals);

  STB_CHECK(!rt_cam.is_cuda(), "`rt_cam` must be cpu");

  STB_CHECK_DEVICE(reference_dev, target_points);
  STB_CHECK_DEVICE(reference_dev, target_normals);
  STB_CHECK_DEVICE(reference_dev, target_mask);
  STB_CHECK_DEVICE(reference_dev, kcam);
  STB_CHECK_DEVICE(reference_dev, index_map);

  STB_DISPATCH_KERNEL_FLOATING_TYPES(
      source_points.scalar_type(), reference_dev, "MatchByProjection", ([&] {
        MatchByProjectionKernel<device, scalar_t> kernel(
            source_points, source_normals, rt_cam, target_points,
            target_normals, target_mask, kcam, index_map, distance_thresh,
            normal_angle_thesh);
        KernelLauncher<device>::Launch1D(kernel, source_points.size(0));
      }));
}

namespace {
template <Device dev, typename scalar_t>
struct MatchKNNNormalsKernel {
  const typename Accessor<dev, scalar_t, 2>::T knn_distances;
  const typename Accessor<dev, int64_t, 2>::T knn_index;
  const typename Accessor<dev, scalar_t, 2>::T source_normals;
  const RigidTransform<scalar_t> rt_cam;
  const typename Accessor<dev, scalar_t, 2>::T target_normals;
  IndexMap<dev> corresp_indexmap;
  const double normal_angle_thresh;

  MatchKNNNormalsKernel(const torch::Tensor &knn_distances,
                        const torch::Tensor &knn_index,
                        const torch::Tensor &source_normals,
                        const torch::Tensor &rt_cam,
                        const torch::Tensor &target_normals,
                        torch::Tensor corresp_indexmap,
                        double normal_angle_thresh)
      : knn_distances(Accessor<dev, scalar_t, 2>::Get(knn_distances)),
        knn_index(Accessor<dev, int64_t, 2>::Get(knn_index)),
        source_normals(Accessor<dev, scalar_t, 2>::Get(source_normals)),
        rt_cam(rt_cam),
        target_normals(Accessor<dev, scalar_t, 2>::Get(target_normals)),
        corresp_indexmap(corresp_indexmap),
        normal_angle_thresh(normal_angle_thresh) {}

#pragma nv_exec_check_disable
  STB_DEVICE_HOST void operator()(int source_index) {
    const float distance = knn_distances[source_index][0];
    const int64_t target_index = knn_index[source_index][0];

    const Vector<scalar_t, 3> Tsrc_normal =
        rt_cam.TransformNormal(to_vec3<scalar_t>(source_normals[source_index]));
    const Vector<scalar_t, 3> target_normal(
        to_vec3<scalar_t>(target_normals[target_index]));
    const scalar_t angle = GetVectorsAngle(Tsrc_normal, target_normal);
    if (angle >= normal_angle_thresh) return;

    corresp_indexmap.SetIfLess(target_index, 0, distance, source_index);
  }
};

}  // namespace

void CorrespondenceMatching::MatchKNNNormals(
    const torch::Tensor &knn_distances, const torch::Tensor &knn_index,
    const torch::Tensor &source_normals, const torch::Tensor &rt_cam,
    const torch::Tensor &target_normals, torch::Tensor corresp_indexmap,
    double normal_angle_thesh) {
  auto ref_device = knn_distances.device();
  STB_CHECK_DEVICE(ref_device, knn_index);
  STB_CHECK_DEVICE(ref_device, source_normals);
  STB_CHECK(!rt_cam.is_cuda(), "`rt_cam` must be cpu");
  STB_CHECK_DEVICE(ref_device, target_normals);
  STB_CHECK_DEVICE(ref_device, corresp_indexmap);

  STB_DISPATCH_KERNEL_FLOATING_TYPES(
      source_normals.scalar_type(), ref_device, "FilterKNN", ([&] {
        MatchKNNNormalsKernel<device, scalar_t> kernel(
            knn_distances, knn_index, source_normals, rt_cam, target_normals,
            corresp_indexmap, normal_angle_thesh);
        KernelLauncher<device>::Launch1D(kernel, knn_distances.size(0));
      }));
}

namespace {
template <Device dev>
struct GetCorrespondencesKernel {
  IndexMapAccessor<dev> corresp_indexmap;
  typename Accessor<dev, int64_t, 3>::T correspondences;
  typename Accessor<dev, bool, 2>::T selection_mask;

  GetCorrespondencesKernel(const torch::Tensor &corresp_indexmap,
                           torch::Tensor &correspondences,
                           torch::Tensor &selection_mask)
      : corresp_indexmap(corresp_indexmap),
        correspondences(Accessor<dev, int64_t, 3>::Get(correspondences)),
        selection_mask(Accessor<dev, bool, 2>::Get(selection_mask)) {}

#pragma nv_exec_check_disable
  STB_DEVICE_HOST void operator()(int row, int col) {
    if (corresp_indexmap.empty(row, col)) {
      correspondences[row][col][0] = -1;
      selection_mask[row][col] = false;
      return;
    }

    const int32_t source_index = corresp_indexmap(row, col);
    const int32_t target_index = corresp_indexmap.width * row + col;
    correspondences[row][col][0] = target_index;
    correspondences[row][col][1] = source_index;
    selection_mask[row][col] = true;
  }
};
}  // namespace
void CorrespondenceMatching::ConvertIndexmapToCorrespondences(
    const torch::Tensor &merge_map,
    torch::Tensor &correspondences,
    torch::Tensor &selection_mask) {
  auto ref_device = merge_map.device();
  STB_CHECK_DEVICE(ref_device, correspondences);
  STB_CHECK_DEVICE(ref_device, selection_mask);

  STB_DISPATCH_KERNEL(ref_device, [&] {
    GetCorrespondencesKernel<device> kernel(merge_map, correspondences,
                                            selection_mask);
    KernelLauncher<device>::Launch2D(kernel, merge_map.size(1),
                                     merge_map.size(0));
  });
}

}  // namespace slamtb
