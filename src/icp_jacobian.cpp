#include "icp_jacobian.hpp"

#include <pybind11/eigen.h>

#include "feature_map.hpp"

namespace slamtb {
void ICPJacobian::RegisterPybind(pybind11::module &m) {
  py::class_<ICPJacobian>(m, "ICPJacobian")
      .def_static("compute_geometric_term", &ICPJacobian::ComputeGeometricTerm)
      .def_static("compute_feature_term", &ICPJacobian::ComputeFeatureTerm);
}

void ICPOdometryJacobian::RegisterPybind(pybind11::module &m) {
  py::class_<ICPOdometryJacobian>(m, "ICPOdometryJacobian")
      .def_static("compute_feature_term",
                  &ICPOdometryJacobian::ComputeFeatureTerm)
      .def_static("compute_feature_term_so3",
                  &ICPOdometryJacobian::ComputeFeatureTermSO3);
}
}  // namespace slamtb
