#include "registration.hpp"

#include "accessor.hpp"
#include "camera.hpp"
#include "cuda_common.hpp"
#include "error.hpp"
#include "kernel.hpp"

namespace slamtb {
namespace {
template <Device dev, typename scalar_t>
struct InformationMatrixKernel {
  const typename Accessor<dev, int64_t, 2>::T correspondences;
  const typename Accessor<dev, float, 2>::T target_points;

  typename Accessor<dev, scalar_t, 3>::T GtG_partial;

  InformationMatrixKernel(const torch::Tensor &correspondences,
                          const torch::Tensor &target_points,
                          torch::Tensor GtG_partial)
      : correspondences(Accessor<dev, int64_t, 2>::Get(correspondences)),
        target_points(Accessor<dev, float, 2>::Get(target_points)),
        GtG_partial(Accessor<dev, scalar_t, 3>::Get(GtG_partial)) {}

#pragma nv_exec_check_disable
  STB_DEVICE_HOST void operator()(int corresp) {
    // https://github.com/intel-isl/Open3D/blob/b379dcc9ef4214b0be2290b1fe9c0152ab9def3a/cpp/open3d/pipelines/registration/Registration.cpp#L352
    const int32_t target_idx = correspondences[corresp][0];

    const Vector<scalar_t, 3> tgt_point(
        to_vec3<scalar_t>(target_points[target_idx]));

    Vector<scalar_t, 6> G_r0, G_r1, G_r2;
    G_r0 << 0, tgt_point[2], -tgt_point[1], 1, 0, 0;
    G_r1 << -tgt_point[2], 0, tgt_point[0], 0, 1, 0;
    G_r2 << tgt_point[1], -tgt_point[0], 0, 0, 0, 1;

    Eigen::Matrix<scalar_t, 6, 6> GtG_local = G_r0 * G_r0.transpose();
    GtG_local += G_r1 * G_r1.transpose();
    GtG_local += G_r2 * G_r2.transpose();

    for (int i = 0; i < 6; ++i) {
      for (int j = 0; j < 6; ++j) {
        GtG_partial[corresp][i][j] = GtG_local(i, j);
      }
    }
  }
};
}  // namespace

void Registration::ComputeInformationMatrix(
    const torch::Tensor &correspondences, const torch::Tensor &tgt_points,
    torch::Tensor GtG_partial) {
  const auto reference_dev = tgt_points.device();
 
  STB_CHECK_DEVICE(reference_dev, correspondences);
  STB_CHECK_DEVICE(reference_dev, tgt_points);
  STB_CHECK_DEVICE(reference_dev, GtG_partial);

  STB_DISPATCH_KERNEL_FLOATING_TYPES(
      tgt_points.scalar_type(), reference_dev, "ComputeInformationMatrix", [&] {
        InformationMatrixKernel<device, scalar_t> kernel(
            correspondences, tgt_points, GtG_partial);
        KernelLauncher<device>::Launch1D(kernel, correspondences.size(0));
      });
}
}  // namespace slamtb
