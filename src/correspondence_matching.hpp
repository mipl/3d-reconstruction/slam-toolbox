#pragma once

#include "accessor.hpp"
#include "camera.hpp"
#include "math.hpp"

namespace slamtb {
template <Device dev, typename scalar_t>
class PointGrid {
 public:
  const typename Accessor<dev, scalar_t, 3>::T points;
  const typename Accessor<dev, scalar_t, 3>::T normals;
  const typename Accessor<dev, bool, 2>::T mask;
  const int width;
  const int height;

  PointGrid(const torch::Tensor &points, const torch::Tensor normals,
            const torch::Tensor &mask)
      : points(Accessor<dev, scalar_t, 3>::Get(points)),
        normals(Accessor<dev, scalar_t, 3>::Get(normals)),
        mask(Accessor<dev, bool, 2>::Get(mask)),
        width(mask.size(1)),
        height(mask.size(0)) {}

  STB_DEVICE_HOST bool empty(int row, int col) const { return !mask[row][col]; }
};

template <Device dev, typename scalar_t>
struct RobustCorrespondence {
  const PointGrid<dev, scalar_t> tgt;
  const KCamera<dev, scalar_t> kcam;
  const scalar_t squared_distance_thresh;
  const scalar_t angle_thresh;

  RobustCorrespondence(const torch::Tensor &points,
                       const torch::Tensor &normals, const torch::Tensor &mask,
                       const torch::Tensor &kcam, double distance_thresh,
                       double angle_thresh)
      : tgt(points, normals, mask),
        kcam(kcam),
        squared_distance_thresh(distance_thresh * distance_thresh),
        angle_thresh(angle_thresh) {}

  STB_DEVICE_HOST bool Match(const Vector<scalar_t, 3> &src_point,
                             const Vector<scalar_t, 3> &src_normal, scalar_t &u,
                             scalar_t &v) const {
    kcam.Project(src_point, u, v);
    const int ui = int(round(u));
    const int vi = int(round(v));

    if (ui < 0 || ui >= tgt.width || vi < 0 || vi >= tgt.height) return false;
    if (tgt.empty(vi, ui)) return false;

    const Vector<scalar_t, 3> tgt_point = to_vec3<scalar_t>(tgt.points[vi][ui]);
    if ((tgt_point - src_point).squaredNorm() > squared_distance_thresh)
      return false;

    const Vector<scalar_t, 3> tgt_normal =
        to_vec3<scalar_t>(tgt.normals[vi][ui]);
    const scalar_t angle = GetVectorsAngle(src_normal, tgt_normal);
    if (angle >= angle_thresh) return false;
    return true;
  }
};
}  // namespace slamtb
