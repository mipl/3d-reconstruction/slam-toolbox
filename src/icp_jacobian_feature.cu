#include "icp_jacobian.hpp"

#include <pybind11/eigen.h>

#include "accessor.hpp"
#include "camera.hpp"
#include "error.hpp"
#include "feature_map.hpp"
#include "icp_jacobian_common.hpp"
#include "estimator.hpp"
#include "kernel.hpp"

namespace slamtb {

namespace {

template <Device dev, typename scalar_t, typename JacobianType>
struct FeatureJacobianKernel {
  const typename Accessor<dev, int64_t, 2>::T correspondences;
  const typename Accessor<dev, scalar_t, 2>::T src_points;
  const typename Accessor<dev, scalar_t, 2>::T src_feats;
  const RigidTransform<scalar_t> rt_cam;
  const typename Accessor<dev, scalar_t, 2>::T tgt_feats;
  const typename Accessor<dev, scalar_t, 3>::T dxyz_feats;

  const float residual_thresh;
  const HuberEstimator<scalar_t> estimator;

  typename Accessor<dev, scalar_t, 3>::T JtJ_partial;
  typename Accessor<dev, scalar_t, 2>::T Jtr_partial;
  typename Accessor<dev, scalar_t, 1>::T squared_residuals;

  FeatureJacobianKernel(
      const torch::Tensor &correspondences, const torch::Tensor &src_points,
      const torch::Tensor &src_feats, const torch::Tensor &rt_cam,
      const torch::Tensor &tgt_feats, const torch::Tensor &dxyz_feats,
      float residual_thresh, float estimator_delta, torch::Tensor JtJ_partial,
      torch::Tensor Jtr_partial, torch::Tensor squared_residuals)
      : correspondences(Accessor<dev, int64_t, 2>::Get(correspondences)),
        src_points(Accessor<dev, scalar_t, 2>::Get(src_points)),
        src_feats(Accessor<dev, scalar_t, 2>::Get(src_feats)),
        rt_cam(rt_cam),
        tgt_feats(Accessor<dev, scalar_t, 2>::Get(tgt_feats)),
        dxyz_feats(Accessor<dev, scalar_t, 3>::Get(dxyz_feats)),
        residual_thresh(residual_thresh),
        estimator(estimator_delta),
        JtJ_partial(Accessor<dev, scalar_t, 3>::Get(JtJ_partial)),
        Jtr_partial(Accessor<dev, scalar_t, 2>::Get(Jtr_partial)),
        squared_residuals(Accessor<dev, scalar_t, 1>::Get(squared_residuals)) {}

#pragma nv_exec_check_disable
  STB_DEVICE_HOST void operator()(int corresp) {
    // const int32_t target_idx = correspondences[corresp][0];
    const int32_t source_idx = correspondences[corresp][1];

    JacobianType jacobian(JtJ_partial[corresp], Jtr_partial[corresp]);

    const Vector<scalar_t, 3> Tsrc_point =
        rt_cam.Transform(to_vec3<scalar_t>(src_points[source_idx]));

    scalar_t squared_residual = 0;

    for (int channel = 0; channel < tgt_feats.size(0); ++channel) {
      const scalar_t feat_residual =
          (src_feats[channel][source_idx] - tgt_feats[channel][source_idx]);
	  const scalar_t sqr_feat_residual = feat_residual * feat_residual;
      squared_residual += sqr_feat_residual;
    }

	if (squared_residual > residual_thresh * residual_thresh) {
      squared_residuals[corresp] = 0.0;
	  return;
    }

	const scalar_t residual = sqrt(squared_residual);
	squared_residuals[corresp] = squared_residual;

	const scalar_t estimator_weight = estimator.Backward(squared_residual);
    for (int channel = 0; channel < tgt_feats.size(0); ++channel) {
      const scalar_t feat_residual =
          (src_feats[channel][source_idx] - tgt_feats[channel][source_idx]);

      Vector<scalar_t, 3> gradxyz(dxyz_feats[source_idx][channel][0],
                                  dxyz_feats[source_idx][channel][1],
                                  dxyz_feats[source_idx][channel][2]);
      const scalar_t norm = gradxyz.norm();
      if (norm > 0.0) {
        gradxyz /= norm;
      } else {
		continue;
	  }
		
      jacobian.Compute(Tsrc_point, gradxyz, residual, estimator_weight);
    }
  }
};
}  // namespace

void ICPJacobian::ComputeFeatureTerm(
    const torch::Tensor &correspondences, const torch::Tensor &src_points,
    const torch::Tensor &src_feats, const torch::Tensor &rt_cam,
    const torch::Tensor &tgt_feats, const torch::Tensor &dxyz_feats,
    double residual_thresh, double estimator_delta,
    torch::Tensor JtJ_partial, torch::Tensor Jr_partial,
    torch::Tensor squared_residuals) {
  const auto reference_dev = src_points.device();

  STB_CHECK_DEVICE(reference_dev, correspondences);
  STB_CHECK_DEVICE(reference_dev, src_feats);
  STB_CHECK(!rt_cam.is_cuda(), "`rt_cam` must be cpu");
  STB_CHECK_DEVICE(reference_dev, tgt_feats);
  STB_CHECK_DEVICE(reference_dev, dxyz_feats);

  STB_CHECK_DEVICE(reference_dev, JtJ_partial);
  STB_CHECK_DEVICE(reference_dev, Jr_partial);
  STB_CHECK_DEVICE(reference_dev, squared_residuals);

  STB_DISPATCH_KERNEL_FLOATING_TYPES(
      src_points.scalar_type(), reference_dev, "ComputeFeatureTerm", [&] {
        typedef SE3ICPJacobian<device, scalar_t> TGroup;
        FeatureJacobianKernel<device, scalar_t, TGroup> kernel(
            correspondences, src_points, src_feats, rt_cam, tgt_feats,
            dxyz_feats, residual_thresh, estimator_delta,
            JtJ_partial, Jr_partial,
            squared_residuals);

        KernelLauncher<device>::Launch1D(kernel, correspondences.size(0));
      });
}

}  // namespace slamtb
