#include "matching.hpp"

namespace slamtb {

void CorrespondenceMatching::RegisterPybind(pybind11::module &m) {
  pybind11::class_<CorrespondenceMatching>(m, "CorrespondenceMatching")
      .def_static("match_by_projection",
                  &CorrespondenceMatching::MatchByProjection)
      .def_static("convert_indexmap_to_correspondences",
                  &CorrespondenceMatching::ConvertIndexmapToCorrespondences)
      .def_static("match_knn_normals",
                  &CorrespondenceMatching::MatchKNNNormals);
}
}  // namespace slamtb
