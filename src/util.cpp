#include "util.hpp"

namespace py = pybind11;

namespace slamtb {

torch::Tensor Util::MaskedYXToLinear(const torch::Tensor &keypoint_yx,
                                     const torch::Tensor &mask) {
  const torch::TensorAccessor<int64_t, 2> yx_acc =
      keypoint_yx.accessor<int64_t, 2>();
  const torch::TensorAccessor<bool, 2> mask_acc = mask.accessor<bool, 2>();

  // The point id must match with the point in the point-cloud.
  // So it will skip the mask empty values.

  torch::Tensor point_id_grid =
      torch::full(mask.sizes(), -1, torch::TensorOptions(torch::kInt32));
  torch::TensorAccessor<int32_t, 2> point_id_grid_acc =
      point_id_grid.accessor<int32_t, 2>();
  int point_count = 0;
  for (int row = 0; row < point_id_grid.size(0); ++row) {
    for (int col = 0; col < point_id_grid.size(1); ++col) {
      if (mask_acc[row][col]) {
        point_id_grid_acc[row][col] = point_count++;
      }
    }
  }

  torch::Tensor ravel = torch::empty({keypoint_yx.size(0)}, torch::kInt64);
  auto ravel_acc = ravel.accessor<int64_t, 1>();

  for (size_t i = 0; i < yx_acc.size(0); ++i) {
    const int32_t y = yx_acc[i][0];
    const int32_t x = yx_acc[i][1];

    assert(y < point_id_grid.size(0));
    assert(x < point_id_grid.size(1));

    if (!mask_acc[y][x]) {
      ravel_acc[i] = -1;
    }

    const int point_id = point_id_grid_acc[y][x];

    ravel_acc[i] = point_id;
  }

  return ravel;
}

void Util::RegisterPybind(py::module &m) {
  py::class_<Util>(m, "Util").def_static("masked_yx_to_linear",
                                         &Util::MaskedYXToLinear);
}
}  // namespace slamtb
