#include "icp_jacobian.hpp"

#include "accessor.hpp"
#include "camera.hpp"
#include "error.hpp"
#include "estimator.hpp"
#include "icp_jacobian_common.hpp"
#include "kernel.hpp"

namespace slamtb {

namespace {
template <Device dev, typename scalar_t>
struct GeometricJacobianKernel {
  const typename Accessor<dev, int64_t, 2>::T correspondences;
  const typename Accessor<dev, scalar_t, 2>::T src_points;

  const RigidTransform<scalar_t> rt_cam;

  const typename Accessor<dev, float, 2>::T target_points;
  const typename Accessor<dev, float, 2>::T target_normals;

  typename Accessor<dev, scalar_t, 3>::T JtJ_partial;
  typename Accessor<dev, scalar_t, 2>::T Jtr_partial;
  typename Accessor<dev, scalar_t, 1>::T squared_residual;

  const HuberEstimator<scalar_t> estimator;

  GeometricJacobianKernel(const torch::Tensor &correspondences,
                          const torch::Tensor &src_points,
                          const torch::Tensor &rt_cam,
                          const torch::Tensor &target_points,
                          const torch::Tensor &target_normals,
                          torch::Tensor JtJ_partial, torch::Tensor Jtr_partial,
                          torch::Tensor squared_residual)
      : correspondences(Accessor<dev, int64_t, 2>::Get(correspondences)),
        src_points(Accessor<dev, scalar_t, 2>::Get(src_points)),
        rt_cam(rt_cam),
        target_points(Accessor<dev, float, 2>::Get(target_points)),
        target_normals(Accessor<dev, float, 2>::Get(target_normals)),
        JtJ_partial(Accessor<dev, scalar_t, 3>::Get(JtJ_partial)),
        Jtr_partial(Accessor<dev, scalar_t, 2>::Get(Jtr_partial)),
        squared_residual(Accessor<dev, scalar_t, 1>::Get(squared_residual)),
        estimator(0.25) {}

#pragma nv_exec_check_disable
  STB_DEVICE_HOST void operator()(int corresp) {
    const int32_t target_idx = correspondences[corresp][0];
    const int32_t source_idx = correspondences[corresp][1];

    squared_residual[corresp] = 0;

    const Vector<scalar_t, 3> Tsrc_point =
        rt_cam.Transform(to_vec3<scalar_t>(src_points[source_idx]));

    const Vector<scalar_t, 3> tgt_point(
        to_vec3<scalar_t>(target_points[target_idx]));
    const Vector<scalar_t, 3> tgt_normal(
        to_vec3<scalar_t>(target_normals[target_idx]));

    const scalar_t residual = (tgt_point - Tsrc_point).dot(tgt_normal);
    const scalar_t sqr_residual = residual * residual;
    SE3ICPJacobian<dev, scalar_t> jacobian(JtJ_partial[corresp],
                                           Jtr_partial[corresp]);
    jacobian.Compute(Tsrc_point, tgt_normal, residual,
                     estimator.Backward(sqr_residual));
    squared_residual[corresp] = estimator.Forward(sqr_residual);
  }
};

}  // namespace

void ICPJacobian::ComputeGeometricTerm(
    const torch::Tensor &correspondences, const torch::Tensor &src_points,
    const torch::Tensor &rt_cam, const torch::Tensor &tgt_points,
    const torch::Tensor &tgt_normals, torch::Tensor JtJ_partial,
    torch::Tensor Jr_partial, torch::Tensor squared_residual) {
  const auto reference_dev = src_points.device();

  STB_CHECK_DEVICE(reference_dev, correspondences);
  STB_CHECK_DEVICE(reference_dev, tgt_points);
  STB_CHECK_DEVICE(reference_dev, tgt_normals);

  STB_CHECK(!rt_cam.is_cuda(), "`rt_cam` must be CPU");

  STB_CHECK_DEVICE(reference_dev, JtJ_partial);
  STB_CHECK_DEVICE(reference_dev, Jr_partial);
  STB_CHECK_DEVICE(reference_dev, squared_residual);

  STB_DISPATCH_KERNEL_FLOATING_TYPES(
      src_points.scalar_type(), reference_dev, "ComputeGeometricTerm", [&] {
        GeometricJacobianKernel<device, scalar_t> kernel(
            correspondences, src_points, rt_cam, tgt_points, tgt_normals,
            JtJ_partial, Jr_partial, squared_residual);

        KernelLauncher<device>::Launch1D(kernel, correspondences.size(0));
      });
}
}  // namespace slamtb
