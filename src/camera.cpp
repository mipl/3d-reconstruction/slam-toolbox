#include "camera.hpp"

#include <pybind11/eigen.h>

#include "error.hpp"

namespace slamtb {

void ProjectOp::RegisterPybind(pybind11::module &m) {
  pybind11::class_<ProjectOp>(m, "ProjectOp")
      .def_static("forward", &ProjectOp::Forward)
      .def_static("backward", &ProjectOp::Backward);
}

Eigen::Matrix4f RigidTransformOp::FindRotationTranslation(
    const Eigen::Ref<Eigen::Matrix3f> cov_matrix,
    const Eigen::Ref<Eigen::Vector3f> source_mean,
    const Eigen::Ref<Eigen::Vector3f> target_mean) {
  Eigen::JacobiSVD<Eigen::Matrix3f> svd(
      cov_matrix, Eigen::ComputeFullU | Eigen::ComputeFullV);

  const auto Vt = svd.matrixV();
  Eigen::Matrix3f R = Vt * svd.matrixU().transpose();
  const auto r_det = R.determinant();

  if (r_det < 0) {
    auto V = svd.matrixV().transpose();
    const auto uv_det = (svd.matrixU() * V).determinant();
    // V(2, 0) *= uv_det;
    // V(2, 1) *= uv_det;
    // V(2, 2) *= uv_det;

    Eigen::Matrix3f eye;
    eye << 1, 0, 0, 0, 1, 0, 0, 0, uv_det;
    R = svd.matrixU() * eye * V;
  }

  const auto t = target_mean - R * source_mean;
  Eigen::Matrix4f transform;
  // clang-format off
  transform <<
      R(0, 0), R(0, 1), R(0, 2), t[0],
      R(1, 0), R(1, 1), R(1, 2), t[1],
      R(2, 0), R(2, 1), R(2, 2), t[2],
      0, 0, 0, 1;
  // clang-format on
  return transform;
}

void RigidTransformOp::Rodrigues(const torch::Tensor &rot_matrix,
                                 torch::Tensor rodrigues) {
  STB_CHECK(!rot_matrix.is_cuda(), "Rodrigues is cpu only");
  STB_CHECK(!rodrigues.is_cuda(), "Rodrigues is cpu only");

  AT_DISPATCH_ALL_TYPES(rot_matrix.scalar_type(), "Rodrigues", [&] {
    const torch::TensorAccessor<scalar_t, 2> rot_acc =
        rot_matrix.accessor<scalar_t, 2>();
    torch::TensorAccessor<scalar_t, 1> rodrigues_acc =
        rodrigues.accessor<scalar_t, 1>();

    Eigen::Matrix3d rot;
    for (int i = 0; i < 3; ++i) {
      for (int j = 0; j < 3; ++j) {
        rot(0, 0) = rot_acc[i][j];
      }
    }

    const auto axis = Eigen::AngleAxisd(rot).axis();
    rodrigues[0] = axis[0];
    rodrigues[1] = axis[1];
    rodrigues[2] = axis[2];
  });
}

void RigidTransformOp::RegisterPybind(pybind11::module &m) {
  pybind11::class_<RigidTransformOp>(m, "RigidTransformOp")
      .def_static("rodrigues", &RigidTransformOp::Rodrigues)
      .def_static("transform_inplace",
                  &RigidTransformOp::TransformPointsInplace)
      .def_static("transform_normals_inplace",
                  &RigidTransformOp::TransformNormalsInplace)
      .def_static("find_rotation_translation",
                  &RigidTransformOp::FindRotationTranslation);
}

}  // namespace slamtb
