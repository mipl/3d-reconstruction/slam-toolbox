#include <memory>

#include <pybind11/eigen.h>

#include "aabb.hpp"
#include "camera.hpp"
#include "feature_interpolation.hpp"
#include "feature_map.hpp"
#include "geometry.hpp"
#include "icp_jacobian.hpp"
#include "int_feature_dict.hpp"
#include "matching.hpp"
#include "point_cloud.hpp"
#include "point_volume.hpp"
#include "processing.hpp"
#include "registration.hpp"
#include "surfel.hpp"
#include "surfel_fusion.hpp"
#include "surfel_volume.hpp"
#include "triangle_mesh_octree.hpp"
#include "util.hpp"

using namespace std;
namespace py = pybind11;

PYBIND11_MODULE(_cslamtb, m) {
  using namespace slamtb;

  Util::RegisterPybind(m);
  AABB::RegisterPybind(m);

  // slamtb.frame
  Processing::RegisterPybind(m);

  // Transform
  ProjectOp::RegisterPybind(m);
  RigidTransformOp::RegisterPybind(m);
  
  // Registraion
  ICPJacobian::RegisterPybind(m);
  ICPOdometryJacobian::RegisterPybind(m);
  FeatureMap::RegisterPybind(m);
  Registration::RegisterPybind(m);
  IntFeatureDict::RegisterPybind(m);

  // Spatial
  TriangleMeshOctree::RegisterPybind(m);
  FeatureKNNInterpolation::RegisterPybind(m);
  FeatureProjectionInterpolation::RegisterPybind(m);
  CorrespondenceMatching::RegisterPybind(m);

  // Pointcloud
  PointCloudBase::RegisterPybind(m);
  PointVolumeBase::RegisterPybind(m);

  // Surfel
  SurfelOp::RegisterPybind(m);
  SurfelAllocator::RegisterPybind(m);
  SurfelIndexMap::RegisterPybind(m);
  SurfelModel::RegisterPybind(m);
  SurfelCloud::RegisterPybind(m);
  SurfelFusionOp::RegisterPybind(m);
  SurfelVolume::RegisterPybind(m);
}
