#include "feature_map.hpp"

#include "cuda_utils.hpp"

namespace slamtb {
namespace detail {
void CUDAFeatureMapImpl::Create(const torch::Tensor &feature_map_) {
  torch::Tensor feature_map =
      feature_map_.permute({0, 1, 2}).to(torch::kFloat32).contiguous();
  CudaSafeCall(cudaDeviceSynchronize());
  width = feature_map.size(2);
  height = feature_map.size(1);
  channels = feature_map.size(0);

  {
    cudaChannelFormatDesc channel_desc = cudaCreateChannelDesc<float>();
    CudaSafeCall(cudaMalloc3DArray(
        &array, &channel_desc, make_cudaExtent(width, height, channels), 0));
  }

  {
    cudaMemcpy3DParms copy_params = {0};
    memset(&copy_params, 0, sizeof(copy_params));
    copy_params.srcPtr = make_cudaPitchedPtr(feature_map.data_ptr<float>(),
                                             sizeof(float) * width
                                             //* channels
                                             ,
                                             width, height);
    copy_params.dstArray = array;
    copy_params.extent = make_cudaExtent(width, height, channels);
    copy_params.kind = cudaMemcpyDeviceToDevice;
    CudaSafeCall(cudaMemcpy3D(&copy_params));
  }

  {
    cudaResourceDesc tex_resource;
    memset(&tex_resource, 0, sizeof(cudaResourceDesc));
    tex_resource.resType = cudaResourceTypeArray;
    tex_resource.res.array.array = array;

    cudaTextureDesc tex_description;
    memset(&tex_description, 0, sizeof(cudaTextureDesc));

#if 1
    tex_description.addressMode[0] = tex_description.addressMode[1] =
        tex_description.addressMode[2] = cudaAddressModeClamp;
#else
    tex_description.addressMode[0] = tex_description.addressMode[1] =
        tex_description.addressMode[2] = cudaAddressModeBorder;
    tex_description.borderColor[0] = tex_description.borderColor[1] =
        tex_description.borderColor[2] = tex_description.borderColor[3] = 0.0f;
#endif
    tex_description.filterMode = cudaFilterModePoint;
    tex_description.readMode = cudaReadModeElementType;

    CudaSafeCall(cudaCreateTextureObject(&this->feature_map, &tex_resource,
                                         &tex_description, NULL));
  }
}

void CUDAFeatureMapImpl::Release() {
  if (feature_map != 0) {
    CudaSafeCall(cudaDestroyTextureObject(feature_map));
    CudaSafeCall(cudaFreeArray(array));
    feature_map = 0;
  }
}
}  // namespace detail

void FeatureMap::RegisterPybind(pybind11::module &m) {
  pybind11::class_<FeatureMap>(m, "FeatureMap")
      .def(py::init<>())
      .def(py::init<torch::Tensor>())
      .def("create", &FeatureMap::Create)
      .def("release", &FeatureMap::Release);
}

void FeatureMap::Create(const torch::Tensor &feature_map) {
  if (feature_map.device().is_cuda()) {
    auto cuda_impl = new detail::CUDAFeatureMapImpl;
    cuda_impl->Create(feature_map);
    impl_.reset(cuda_impl);
  } else {
    impl_.reset(new detail::CPUFeatureMapImpl(feature_map));
  }
}

}  // namespace slamtb
