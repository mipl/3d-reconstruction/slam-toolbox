#include "icp_jacobian.hpp"

#include <pybind11/eigen.h>

#include "accessor.hpp"
#include "camera.hpp"
#include "error.hpp"
#include "feature_map.hpp"
#include "icp_jacobian_common.hpp"
#include "kernel.hpp"

namespace slamtb {
namespace {

template <Device dev, typename scalar_t, typename JacobianType>
struct FeatureJacobianKernel {
  const typename Accessor<dev, int64_t, 2>::T correspondences;
  const typename Accessor<dev, scalar_t, 2>::T src_points;
  const typename Accessor<dev, scalar_t, 2>::T src_feats;
  const RigidTransform<scalar_t> rt_cam;
  const FeatureMapAccessor<dev, scalar_t> tgt_featmap;
  const KCamera<dev, scalar_t> kcam;
  const float residual_thresh;

  typename Accessor<dev, scalar_t, 3>::T JtJ_partial;
  typename Accessor<dev, scalar_t, 2>::T Jtr_partial;
  typename Accessor<dev, scalar_t, 1>::T squared_residuals;

  FeatureJacobianKernel(const torch::Tensor &correspondences,
                        const torch::Tensor &src_points,
                        const torch::Tensor &src_feats,
                        const torch::Tensor &rt_cam,
                        const FeatureMapAccessor<dev, scalar_t> tgt_featmap,
                        const torch::Tensor &kcam, float residual_thresh,
                        torch::Tensor JtJ_partial, torch::Tensor Jtr_partial,
                        torch::Tensor squared_residuals)
      : correspondences(Accessor<dev, int64_t, 2>::Get(correspondences)),
        src_points(Accessor<dev, scalar_t, 2>::Get(src_points)),
        src_feats(Accessor<dev, scalar_t, 2>::Get(src_feats)),
        rt_cam(rt_cam),
        tgt_featmap(tgt_featmap),
        kcam(kcam),
        residual_thresh(residual_thresh),
        JtJ_partial(Accessor<dev, scalar_t, 3>::Get(JtJ_partial)),
        Jtr_partial(Accessor<dev, scalar_t, 2>::Get(Jtr_partial)),
        squared_residuals(Accessor<dev, scalar_t, 1>::Get(squared_residuals)) {}

#pragma nv_exec_check_disable
  STB_DEVICE_HOST void operator()(int corresp) {
    // const int32_t target_idx = correspondences[corresp][0];
    const int32_t source_idx = correspondences[corresp][1];

    JacobianType jacobian(JtJ_partial[corresp], Jtr_partial[corresp]);

    const Vector<scalar_t, 3> Tsrc_point =
        rt_cam.Transform(to_vec3<scalar_t>(src_points[source_idx]));

    scalar_t u, v;
    kcam.Project(Tsrc_point, u, v);

    scalar_t squared_residual = 0;
    const auto interp(tgt_featmap.GetInterpolator(u, v));
    const auto dx_interp(tgt_featmap.GetInterpolatorGradient(u, v, 0.005));

    for (int channel = 0; channel < tgt_featmap.channel_size; ++channel) {
      const scalar_t feat_residual =
          (src_feats[channel][source_idx] - interp.Get(channel));
      scalar_t du, dv;

      dx_interp.Get(channel, du, dv);

      scalar_t j00_proj, j02_proj, j11_proj, j12_proj;
      kcam.Dx_Project(Tsrc_point, j00_proj, j02_proj, j11_proj, j12_proj);

      const Vector<scalar_t, 3> gradk(du * j00_proj, dv * j11_proj,
                                      du * j02_proj + dv * j12_proj);

      jacobian.Compute(Tsrc_point, gradk, feat_residual);
      squared_residual += feat_residual * feat_residual;
    }

    if (squared_residual > residual_thresh * residual_thresh) {
      jacobian.Zero();
      squared_residuals[corresp] = 0.0;
    } else {
      squared_residuals[corresp] = squared_residual;
    }
  }
};
}  // namespace

void ICPOdometryJacobian::ComputeFeatureTerm(
    const torch::Tensor &correspondences, const torch::Tensor &src_points,
    const torch::Tensor &src_feats, const torch::Tensor &rt_cam,
    const FeatureMap &target_featmap, const torch::Tensor &kcam,
    float residual_thresh, torch::Tensor JtJ_partial, torch::Tensor Jr_partial,
    torch::Tensor squared_residuals) {
  const auto reference_dev = src_points.device();

  STB_CHECK_DEVICE(reference_dev, correspondences);
  STB_CHECK_DEVICE(reference_dev, src_feats);
  STB_CHECK(!rt_cam.is_cuda(), "`rt_cam` must be cpu");
  STB_CHECK_DEVICE(reference_dev, kcam);

  STB_CHECK_DEVICE(reference_dev, JtJ_partial);
  STB_CHECK_DEVICE(reference_dev, Jr_partial);
  STB_CHECK_DEVICE(reference_dev, squared_residuals);

  if (reference_dev.is_cuda()) {
    AT_DISPATCH_FLOATING_TYPES(
        src_points.scalar_type(), "EstimateFeature", ([&] {
          typedef SE3ICPJacobian<kCUDA, scalar_t> TGroup;

          FeatureJacobianKernel<kCUDA, scalar_t, TGroup> kernel(
              correspondences, src_points, src_feats, rt_cam,
              target_featmap.get_cuda_accessor<scalar_t>(), kcam,
              residual_thresh, JtJ_partial, Jr_partial, squared_residuals);

          KernelLauncher<kCUDA>::Launch1D(kernel, correspondences.size(0));
          cudaDeviceSynchronize();
        }));
  } else {
    AT_DISPATCH_FLOATING_TYPES(
        src_points.scalar_type(), "EstimateFeature", [&] {
          typedef SE3ICPJacobian<kCPU, scalar_t> TGroup;

          FeatureJacobianKernel<kCPU, scalar_t, TGroup> kernel(
              correspondences, src_points, src_feats, rt_cam,
              target_featmap.get_cpu_accessor<scalar_t>(), kcam,
              residual_thresh, JtJ_partial, Jr_partial, squared_residuals);
          KernelLauncher<kCPU>::Launch1D(kernel, correspondences.size(0));
        });
  }
}

void ICPOdometryJacobian::ComputeFeatureTermSO3(
    const torch::Tensor &correspondences, const torch::Tensor &src_points,
    const torch::Tensor &src_feats, const torch::Tensor &rt_cam,
    const FeatureMap &target_featmap, const torch::Tensor &kcam,
    float residual_thresh, torch::Tensor JtJ_partial, torch::Tensor Jr_partial,
    torch::Tensor squared_residuals) {
  const auto reference_dev = src_points.device();

  STB_CHECK_DEVICE(reference_dev, correspondences);
  STB_CHECK_DEVICE(reference_dev, src_feats);
  STB_CHECK_DEVICE(reference_dev, rt_cam);
  STB_CHECK_DEVICE(reference_dev, kcam);

  STB_CHECK_DEVICE(reference_dev, JtJ_partial);
  STB_CHECK_DEVICE(reference_dev, Jr_partial);
  STB_CHECK_DEVICE(reference_dev, squared_residuals);

  if (reference_dev.is_cuda()) {
    AT_DISPATCH_FLOATING_TYPES(
        src_points.scalar_type(), "EstimateFeature", ([&] {
          typedef SO3ICPJacobian<kCUDA, scalar_t> TGroup;

          FeatureJacobianKernel<kCUDA, scalar_t, TGroup> kernel(
              correspondences, src_points, src_feats, rt_cam,
              target_featmap.get_cuda_accessor<scalar_t>(), kcam,
              residual_thresh, JtJ_partial, Jr_partial, squared_residuals);

          KernelLauncher<kCUDA>::Launch1D(kernel, correspondences.size(0));
        }));
  } else {
    AT_DISPATCH_FLOATING_TYPES(
        src_points.scalar_type(), "EstimateFeature", [&] {
          typedef SO3ICPJacobian<kCPU, scalar_t> TGroup;

          FeatureJacobianKernel<kCPU, scalar_t, TGroup> kernel(
              correspondences, src_points, src_feats, rt_cam,
              target_featmap.get_cpu_accessor<scalar_t>(), kcam,
              residual_thresh, JtJ_partial, Jr_partial, squared_residuals);

          KernelLauncher<kCPU>::Launch1D(kernel, correspondences.size(0));
        });
  }
}

}  // namespace slamtb
