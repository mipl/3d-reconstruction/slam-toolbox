#pragma once

#include <torch/torch.h>
#include "eigen_common.hpp"

#include "accessor.hpp"
#include "cuda_common.hpp"

namespace slamtb {
template <Device dev, typename scalar_t, unsigned long dims>
struct VectorTensorAccessor {};

template <Device dev, typename scalar_t>
struct VectorTensorAccessor<dev, scalar_t, 2> {
  typename Accessor<dev, scalar_t, 2>::T accessor;

  VectorTensorAccessor(const torch::Tensor &tensor)
      : accessor(Accessor<dev, scalar_t, 2>::Get(tensor)) {}

  inline STB_DEVICE_HOST Eigen::Matrix<scalar_t, 2, 1> operator[](
      int idx) const {
    return Eigen::Matrix<scalar_t, 2, 1>(accessor[idx][0], accessor[idx][1]);
  }

  inline STB_DEVICE_HOST void set(int idx,
                                  const Eigen::Matrix<scalar_t, 2, 1> &vec) {
    accessor[idx][0] = vec[0];
    accessor[idx][1] = vec[1];
  }

  inline STB_DEVICE_HOST int size() const { return accessor.size(0); }
};

template <Device dev, typename scalar_t>
struct VectorTensorAccessor<dev, scalar_t, 3> {
  typename Accessor<dev, scalar_t, 2>::T accessor;

  VectorTensorAccessor(const torch::Tensor &tensor)
      : accessor(Accessor<dev, scalar_t, 2>::Get(tensor)) {}

  inline STB_DEVICE_HOST Eigen::Matrix<scalar_t, 3, 1> operator[](
      int idx) const {
    return Eigen::Matrix<scalar_t, 3, 1>(accessor[idx][0], accessor[idx][1],
                                         accessor[idx][2]);
  }

  inline STB_DEVICE_HOST void set(int idx,
                                  const Eigen::Matrix<scalar_t, 3, 1> &vec) {
    accessor[idx][0] = vec[0];
    accessor[idx][1] = vec[1];
    accessor[idx][2] = vec[2];
  }

  inline STB_DEVICE_HOST int size() const { return accessor.size(0); }
};

}  // namespace slamtb
