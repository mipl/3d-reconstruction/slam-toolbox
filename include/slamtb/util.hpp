#pragma once
#include <torch/torch.h>
#include <torch/csrc/utils/pybind.h>

namespace slamtb {

struct Util {
  static void RegisterPybind(pybind11::module &m);
  static torch::Tensor MaskedYXToLinear(const torch::Tensor &keypoint_yx,
                                        const torch::Tensor &mask);
};

}  // namespace slamtb
