#pragma once

#ifdef __CUDACC__
#define STB_DEVICE __device__
#define STB_DEVICE_HOST __device__ __host__
#else
#define STB_DEVICE
#define STB_DEVICE_HOST
#endif
