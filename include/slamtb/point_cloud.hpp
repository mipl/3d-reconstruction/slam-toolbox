#pragma once

#include <optional>

#include <torch/csrc/utils/pybind.h>
#include <torch/torch.h>

namespace slamtb {

class PointCloudBase {
 public:
  static void RegisterPybind(pybind11::module &m);

  PointCloudBase() {}
  PointCloudBase(torch::Tensor points, std::optional<torch::Tensor> colors,
                 std::optional<torch::Tensor> normals,
                 std::optional<torch::Tensor> features)
      : points(points), colors(colors), normals(normals), features(features) {}

  torch::Tensor points;
  std::optional<torch::Tensor> colors, normals, features;

  int get_size() const { return points.size(0); }

  int get_feature_size() const {
    if (features) {
      features->size(0);
    }
    return 0;
  }

  /**
   * Convert to a Python' dictionary.
   * @return Dictionary.
   */
  pybind11::dict AsDict() const;

  /**
   * Loads from a dict.
   * @param dict Python's dictionary.
   */
  void FromDict(const pybind11::dict &dict);
};

}  // namespace slamtb
