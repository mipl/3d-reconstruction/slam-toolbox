#pragma once

#include <torch/torch.h>
#include <torch/csrc/utils/pybind.h>
#include "eigen_common.hpp"

#include "hash_volume.hpp"

namespace slamtb {

class PointCloudBase;

class PointVolumeBase {
 public:
  struct Point {
    Eigen::Vector3f point, normal, color;
    torch::Tensor feature;
    int count;
    Point() : point(0, 0, 0), normal(0, 0, 0), color(0, 0, 0), count(0) {}

    void AllocateFeature(int feature_size, torch::Device device) {
      if (feature.use_count() == 0) {
        feature =
            torch::zeros({feature_size},
                         torch::TensorOptions(device).dtype(torch::kFloat32));
      }
    }

    inline bool has_feature() const { return feature.use_count() > 0; }
  };

  PointVolumeBase(const Eigen::Vector3f &min_point,
              const Eigen::Vector3f &max_point, float voxel_size,
              int feature_size = -1)
      : volume_(min_point, max_point, voxel_size),
        feature_size_(feature_size) {}

  void Accumulate(const PointCloudBase &points);

  void ToPointCloud(PointCloudBase &output) const;

  size_t get_size() const { return volume_.get_size(); }

  int get_feature_size() const { return feature_size_; }

  static void RegisterPybind(pybind11::module &m);

 private:
  HashVolume<Point> volume_;
  int feature_size_;
};

}  // namespace slamtb
