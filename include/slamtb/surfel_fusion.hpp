#pragma once

#include <torch/torch.h>
#include <torch/csrc/utils/pybind.h>

#include "surfel.hpp"
#include "error.hpp"

namespace slamtb {
struct SurfelIndexMap {
  torch::Tensor point_confidence, normal_radius, color, indexmap;

  static void RegisterPybind(pybind11::module &m);

  int get_width() const { return point_confidence.size(1); }

  int get_height() const { return point_confidence.size(0); }

  void Synchronize();

  const torch::Device get_device() const { return point_confidence.device(); }

  SurfelIndexMap To(const std::string &dev) const {
    SurfelIndexMap result;
    result.point_confidence = point_confidence.to(dev);
    result.normal_radius = normal_radius.to(dev);
    if (color.has_storage()) result.color = color.to(dev);
    result.indexmap = indexmap.to(dev);
    return result;
  }

  void CheckDevice(const torch::Device &dev) const {
    STB_CHECK_DEVICE(dev, point_confidence);
    STB_CHECK_DEVICE(dev, normal_radius);
    if (color.has_storage()) STB_CHECK_DEVICE(dev, color);
    STB_CHECK_DEVICE(dev, indexmap);
  }
};

struct SurfelFusionOp {
  static void FindUpdatable(const SurfelIndexMap &model_indexmap,
                            const SurfelCloud &live_surfels,
                            const torch::Tensor &kcam, float max_normal_angle,
                            int search_size, int time, int scale,
                            torch::Tensor merge_map,
                            torch::Tensor new_surfels_map,
                            torch::Tensor merge_corresp);

  static void Update(const torch::Tensor &merge_corresp,
                     const SurfelCloud &live_surfels, SurfelModel model,
                     const torch::Tensor &rt_cam, int time);

  static void CarveSpace(SurfelModel model, torch::Tensor model_indices,
                         const SurfelIndexMap &model_indexmap,
                         const torch::Tensor kcam,
                         const torch::Tensor &world_to_cam, int time,
                         int neighbor_size, float stable_conf_thresh,
                         int stable_time_thresh, float min_z_diff,
                         torch::Tensor remove_mask);

  static torch::Tensor FindMergeable(const SurfelIndexMap &indexmap,
                                     torch::Tensor merge_map, float max_dist,
                                     float max_angle, int neighbor_size,
                                     float stable_conf_thresh);

  static void Merge(const torch::Tensor &merge_corresp,
                    SurfelModel model);

  static void Clean(SurfelModel model, torch::Tensor model_indices,
                    int time, int stable_time_thresh, float stable_conf_thresh,
                    torch::Tensor remove_mask);

  static void RegisterPybind(pybind11::module &m);
};
}  // namespace slamtb
