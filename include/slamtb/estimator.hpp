#pragma once

#include "cuda_common.hpp"

namespace slamtb {

/**
 * Huber loss p-estimator
 */
template <typename scalar_t>
struct HuberEstimator {
  scalar_t delta; /**<Huber delta parameter.*/

  /**
   * @param delta Delta constant
   */
  STB_DEVICE_HOST HuberEstimator(scalar_t delta) : delta(delta) {}

  /**
   * Compute the huber p-estimator.
   * @param residual the input residual value, usually in non-linear
   *  least squares it is the squared residual.
   * @return the p-estimator.
   */
  STB_DEVICE_HOST scalar_t Forward(scalar_t residual) const {
    if (residual <= delta) {
      return residual;
    }

    return 2.0 * sqrt(residual * delta) - delta;
  }

  /**
   * Compute the derivative in respect to the residual.
   *
   * @param residual residual input.
   * @return the derivative w.r.t residual.
   */
  STB_DEVICE_HOST scalar_t Backward(scalar_t residual) const {
    return min(scalar_t(1), sqrt(delta / residual));
  }
};

/**
 * Cauchy loss p-estimator
 */
template <typename scalar_t>
struct CauchyEstimator {
  scalar_t delta;

  /**
   * @param delta Delta constant
   */
  STB_DEVICE_HOST CauchyEstimator(scalar_t delta) : delta(delta) {}

  /**
   * Compute the Cauchy p-estimator.
   * @param residual the input residual value, usually in non-linear
   *  least squares it is the squared residual.
   * @return the p-estimator.
   */
  STB_DEVICE_HOST scalar_t Forward(scalar_t residual) const {
    return delta * log(1 + residual / delta);
  }

  /**
   * Compute the derivative in respect to the residual.
   *
   * @param residual residual input.
   * @return the derivative w.r.t residual.
   */
  STB_DEVICE_HOST scalar_t Backward(scalar_t residual) const {
    return 1 / (1 + residual / delta);
  }
};

}  // namespace slamtb
