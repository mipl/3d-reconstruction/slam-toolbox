#pragma once

#include <memory>

#include <cuda_runtime.h>
#include <torch/torch.h>
#include <torch/csrc/utils/pybind.h>

#include "accessor.hpp"
#include "cuda_common.hpp"

namespace slamtb {
namespace detail {
class IFeatureMapImpl {
 public:
  virtual void Release() = 0;
};

class CUDAFeatureMapImpl : public IFeatureMapImpl {
 public:
  CUDAFeatureMapImpl() {
    feature_map = 0;
    array = nullptr;
    width = height = channels = 0;
  }
  ~CUDAFeatureMapImpl() { Release(); }
  void Create(const torch::Tensor &feature_map);

  void Release() override;

  cudaTextureObject_t feature_map;
  cudaArray *array;
  int width, height, channels;
};

class CPUFeatureMapImpl : public IFeatureMapImpl {
 public:
  CPUFeatureMapImpl(torch::Tensor feature_map) : feature_map(feature_map) {}

  void Release() override {}

  torch::Tensor feature_map;
};
}  // namespace detail

/**
 * Base FeatureMapAccessor template.
 */
template <Device dev, typename scalar_t>
struct FeatureMapAccessor {};

/**
 * FeatureMapAccessor for CPU using bilinear inteporlation.
 */
template <Device dev, typename scalar_t>
struct FeatureMapInterp {
  const typename Accessor<dev, scalar_t, 3>::T feature_map;
  scalar_t u_ratio;
  scalar_t v_ratio;
  int vi, ui, width, height;

  STB_DEVICE_HOST FeatureMapInterp(
      const typename Accessor<dev, scalar_t, 3>::T feature_map, scalar_t u,
      scalar_t v)
      : feature_map(feature_map) {
    ui = int(u);
    vi = int(v);

    u_ratio = u - scalar_t(ui);
    v_ratio = v - scalar_t(vi);

    width = feature_map.size(2);
    height = feature_map.size(1);
  }

  STB_DEVICE_HOST inline scalar_t Get(int channel) const {
    const auto channel_map = feature_map[channel];

    const scalar_t val00 = channel_map[vi][ui];
    const scalar_t val10 =
        (ui + 1 < width) ? channel_map[vi][ui + 1] : channel_map[vi][ui];
    const scalar_t val01 =
        (vi + 1 < height) ? channel_map[vi + 1][ui] : channel_map[vi][ui];
    scalar_t val11;
    if (ui + 1 < width && vi + 1 < height) {
      val11 = channel_map[vi + 1][ui + 1];
    } else if (ui + 1 < width) {
      val11 = channel_map[vi][ui + 1];
    } else if (vi + 1 < height) {
      val11 = channel_map[vi + 1][ui];
    } else {
      val11 = channel_map[vi][ui];
    }

    const scalar_t u0_interp = val00 * (1 - u_ratio) + val10 * u_ratio;
    const scalar_t u1_interp = val01 * (1 - u_ratio) + val11 * u_ratio;
    const scalar_t val = u0_interp * (1 - v_ratio) + u1_interp * v_ratio;
    return val;
  }
};

template <Device dev, typename scalar_t>
struct FeatureMapInterpGradient {
  const FeatureMapInterp<dev, scalar_t> u0, u1, v0, v1;
  const scalar_t div;

  STB_DEVICE_HOST inline FeatureMapInterpGradient(
      const typename Accessor<dev, scalar_t, 3>::T feature_map, scalar_t u,
      scalar_t v, scalar_t h)
      : u0(feature_map, u + h, v),
        u1(feature_map, u, v),
        v0(feature_map, u, v + h),
        v1(feature_map, u, v),
        div(scalar_t(1) / h) {}

  STB_DEVICE_HOST inline void Get(int channel, scalar_t &du,
                                  scalar_t &dv) const {
    du = (u0.Get(channel) - u1.Get(channel)) * div;
    dv = (v0.Get(channel) - v1.Get(channel)) * div;
  }
};

template <typename scalar_t>
struct FeatureMapAccessor<kCPU, scalar_t> {
  const typename Accessor<kCPU, scalar_t, 3>::T feature_map;
  const int width, height, channel_size;
  FeatureMapAccessor(const torch::Tensor &feature_map)
      : feature_map(Accessor<kCPU, scalar_t, 3>::Get(feature_map)),
        width(feature_map.size(2)),
        height(feature_map.size(1)),
        channel_size(feature_map.size(0)) {}

  STB_DEVICE_HOST inline FeatureMapInterp<kCPU, scalar_t> GetInterpolator(
      scalar_t u, scalar_t v) const {
    return FeatureMapInterp<kCPU, scalar_t>(feature_map, u, v);
  }

  STB_DEVICE_HOST inline FeatureMapInterpGradient<kCPU, scalar_t>
  GetInterpolatorGradient(scalar_t u, scalar_t v, scalar_t h = 0.05) const {
    return FeatureMapInterpGradient<kCPU, scalar_t>(feature_map, u, v, h);
  }
};

#ifdef __CUDACC__
template <typename scalar_t>
struct TextureInterp {
  cudaTextureObject_t feature_map;

  scalar_t u, v;
  scalar_t u_ratio, v_ratio;
  int vi, ui;

  __device__ TextureInterp(cudaTextureObject_t feature_map, scalar_t u_,
                           scalar_t v_)
      : feature_map(feature_map), u(u_), v(v_) {
    u_ratio = u_ - trunc(u_);
    v_ratio = v_ - trunc(v_);
  }

  __device__ scalar_t inline Get(float channel) const {
    const scalar_t val00 = tex3D<float>(feature_map, u, v, channel);
    const scalar_t val10 = tex3D<float>(feature_map, u + 1, v, channel);
    const scalar_t val01 = tex3D<float>(feature_map, u, v + 1, channel);
    const scalar_t val11 = tex3D<float>(feature_map, u + 1, v + 1, channel);

    const scalar_t u0_interp = val00 * (1 - u_ratio) + val10 * u_ratio;
    const scalar_t u1_interp = val01 * (1 - u_ratio) + val11 * u_ratio;
    const scalar_t val = u0_interp * (1 - v_ratio) + u1_interp * v_ratio;
    return val;
  }
};

template <typename scalar_t>
struct TextureInterpGradient {
  const TextureInterp<scalar_t> u0, u1, v0, v1;
  const scalar_t div;

  __device__ inline TextureInterpGradient(cudaTextureObject_t feature_map,
                                          scalar_t u, scalar_t v, scalar_t h)
      : u0(feature_map, u + h, v),
        u1(feature_map, max(u - h, scalar_t(0)), v),
        v0(feature_map, u, v + h),
        v1(feature_map, u, max(v - h, scalar_t(0))),
        div(scalar_t(1) / (scalar_t(2) * h)) {}

  STB_DEVICE_HOST inline void Get(float channel, scalar_t &du,
                                  scalar_t &dv) const {
    du = (u0.Get(channel) - u1.Get(channel)) * div;
    dv = (v0.Get(channel) - v1.Get(channel)) * div;
  }
};

template <typename scalar_t>
struct FeatureMapAccessor<kCUDA, scalar_t> {
  cudaTextureObject_t feature_map;
  const int width, height, channel_size;

  FeatureMapAccessor(cudaTextureObject_t feature_map, int width, int height,
                     int channel_size)
      : feature_map(feature_map),
        width(width),
        height(height),
        channel_size(channel_size) {}

  __device__ inline TextureInterp<scalar_t> GetInterpolator(scalar_t u,
                                                            scalar_t v) const {
    return TextureInterp<scalar_t>(feature_map, u, v);
  }

  __device__ inline TextureInterpGradient<scalar_t> GetInterpolatorGradient(
      scalar_t u, scalar_t v, scalar_t h = 0.05) const {
    return TextureInterpGradient<scalar_t>(feature_map, u, v, h);
  }
};
#endif

class FeatureMap {
 public:
  static void RegisterPybind(pybind11::module &m);

  FeatureMap() {}
  FeatureMap(const torch::Tensor &feature_map) { Create(feature_map); }

  ~FeatureMap() { Release(); }

  void Create(const torch::Tensor &feature_map);

  void Release() {
    if (impl_ != nullptr) {
      impl_->Release();
    }
  }

#ifdef __CUDACC__
  template <Device dev, typename scalar_t>
  FeatureMapAccessor<dev, scalar_t> get_accessor() const {}

  template <typename scalar_t>
  FeatureMapAccessor<kCUDA, scalar_t> get_cuda_accessor() const {
    auto cuda_impl =
        std::dynamic_pointer_cast<detail::CUDAFeatureMapImpl>(impl_);

    return FeatureMapAccessor<kCUDA, scalar_t>(
        cuda_impl->feature_map, cuda_impl->width, cuda_impl->height,
        cuda_impl->channels);
  }

  template <typename scalar_t>
  FeatureMapAccessor<kCUDA, scalar_t> get_accessor() const {
    return get_cuda_accessor<scalar_t>();
  }
#endif

  template <typename scalar_t>
  FeatureMapAccessor<kCPU, scalar_t> get_cpu_accessor() const {
    return FeatureMapAccessor<kCPU, scalar_t>(
        std::dynamic_pointer_cast<detail::CPUFeatureMapImpl>(impl_)
            ->feature_map);
  }

  template <typename scalar_t>
  FeatureMapAccessor<kCPU, scalar_t> get_accessor() const {
    return get_cpu_accessor<scalar_t>();
  }

 private:
  std::shared_ptr<detail::IFeatureMapImpl> impl_;
};
}  // namespace slamtb
