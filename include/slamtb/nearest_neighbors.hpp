#pragma once

#include <torch/torch.h>
#include <torch/csrc/utils/pybind.h>

#include "accessor.hpp"
#include "math.hpp"

namespace slamtb {
#if 0
template <Device dev, typename scalar_t>
struct Interpolator {
  const int64_t point_index;
  const typename Accessor<dev, scalar_t, 2>::T nn_distances;
  const typename Accessor<dev, int64_t, 2>::T nn_index;
  const typename Accessor<dev, scalar_t, 2>::T features;
  scalar_t total_similarity;

  STB_DEVICE_HOST Interpolator(int64_t point_index,
                               const torch::Tensor &nn_distances,
                               const torch::Tensor &nn_index,
                               const torch::Tensor &features)
      : total_similarity(0) {
    for (int k = 0; k < nn_distances.size(1); ++k) {
      const scalar_t distance = nn_distances[point_index][k];
      if (distance < NumericLimits<dev, scalar_t>::infinity()) {
        total_similarity += 1 / (1 + distance);
      }
    }
  }

  STB_DEVICE_HOST scalar_t Forward(int channel) const {
    scalar_t feature = 0;
    for (int k = 0; k < nn_index.size(1); ++k) {
      const int64_t index = nn_index[point_index][k];
      if (index >= features.size(1)) break;

      const scalar_t distance = nn_distances[point_index][k];
      const scalar_t similarity = 1 / (1 + distance);

      scalar_t weight = similarity / total_similarity;
      feature += features[channel][point_index] * weight;
    }
    return feature;
  }
};

template <Device dev, typename scalar_t>
struct InterpolatorBackward {
 public:
  const typename Accessor<dev, scalar_t, 2>::Ts epsilon_distances;
  const typename Accessor<dev, int64_t, 1>::Ts nn_index;
  const typename Accessor<dev, scalar_t, 2>::T features;
  const scalar_t div;
  const int64_t max_index;

  STB_DEVICE_HOST InterpolatorBackward(
      const typename Accessor<dev, scalar_t, 2>::Ts epsilon_distances,
      const typename Accessor<dev, int64_t, 1>::Ts nn_index,
      const typename Accessor<dev, scalar_t, 2>::T features, scalar_t delta_h)
      : epsilon_distances(epsilon_distances),
          nn_index(nn_index),
          features(features),
          div(scalar_t(1) / (2 * delta_h)),
          max_index(features.size(1)) {}
  STB_DEVICE_HOST void Backward(int channel, scalar_t &dx, scalar_t &dy,
                                scalar_t &dz) const {
    dx = (Get(channel, 0) - Get(channel, 1)) * div;
    dy = (Get(channel, 2) - Get(channel, 3)) * div;
    dz = (Get(channel, 4) - Get(channel, 5)) * div;
  }

 private:
  STB_DEVICE_HOST inline scalar_t Get(int channel, int which_epsilon) const {
    const auto distance_weights = epsilon_distances[which_epsilon];
    scalar_t feature_avg = 0;

    for (int k = 0; k < nn_index.size(0); ++k) {
      const int64_t index = nn_index[k];
      if (index < max_index) {
        feature_avg += features[channel][index] * distance_weights[k];
      }
    }

    return feature_avg;
  }
};

class UnorderedFeatureMap {
 public:
  scalar_t Get() const;

 private:
};

#endif

struct NearestNeighborsOp {
  static void Forward(const torch::Tensor &nn_distances,
                      const torch::Tensor &nn_index,
                      const torch::Tensor &features,
                      torch::Tensor out_features);

  static void ComputeEpsilonDistances(const torch::Tensor &target_xyz,
                                      const torch::Tensor &source_xyz,
                                      const torch::Tensor &nn_index,
                                      const torch::Tensor &epsilon_distances);
  
  static void Backward(const torch::Tensor &epsilon_distances,
                       const torch::Tensor &nn_index,
                       const torch::Tensor &features,
                       const torch::Tensor &dl_features, torch::Tensor dl_xyz);

  static void ComputeDxyzFeatures(const torch::Tensor &epsilon_distances,
                                  const torch::Tensor &nn_index,
                                  const torch::Tensor &features,
                                  torch::Tensor dl_xyz);

  static void RegisterPybind(pybind11::module &m);
};
}  // namespace slamtb
