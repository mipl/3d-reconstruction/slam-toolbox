#pragma once

#include <torch/torch.h>
#include <torch/csrc/utils/pybind.h>

namespace slamtb {

class FeatureMap;

struct CorrespondenceMatching {
  static void MatchByProjection(
      const torch::Tensor &source_points, const torch::Tensor &source_normals,
      const torch::Tensor &rt_cam, const torch::Tensor &target_points,
      const torch::Tensor &target_normals, const torch::Tensor &target_mask,
      const torch::Tensor &kcam, torch::Tensor corresp_minmap,
      double distance_thresh, double normal_angle_thesh);

  static void MatchKNNNormals(const torch::Tensor &knn_distances,
                              const torch::Tensor &knn_index,
                              const torch::Tensor &source_normals,
                              const torch::Tensor &rt_cam,
                              const torch::Tensor &target_normals,
                              torch::Tensor corresp_minmap,
                              double normal_angle_thesh);
  static void ConvertIndexmapToCorrespondences(const torch::Tensor &merge_map,
                                               torch::Tensor &correspondences,
                                               torch::Tensor &selection_mask);

  static void RegisterPybind(pybind11::module &m);
};

}  // namespace slamtb
