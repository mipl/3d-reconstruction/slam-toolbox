#pragma once

#include <torch/torch.h>
#include <torch/csrc/utils/pybind.h>

namespace slamtb {

class FeatureMap;

struct FeatureProjectionInterpolation {
  static void Forward(const FeatureMap &target_features,
                      const torch::Tensor &source_points,
                      const torch::Tensor &kcam,
                      torch::Tensor out_features);

  static void Backward(const FeatureMap &target_features,
                       const torch::Tensor &source_points,
                       const torch::Tensor &dl_feature,
                       const torch::Tensor &kcam, double grad_precision,
                       torch::Tensor dx_points);

  static void RegisterPybind(pybind11::module &m);
};

struct FeatureKNNInterpolation {
  static void Forward(const torch::Tensor &nn_distances,
                      const torch::Tensor &nn_index,
                      const torch::Tensor &features,
                      torch::Tensor out_features);

  static void ComputeEpsilonDistances(const torch::Tensor &target_xyz,
                                      const torch::Tensor &source_xyz,
                                      const torch::Tensor &nn_index,
                                      const torch::Tensor &epsilon_distances);

  static void Backward(const torch::Tensor &epsilon_distances,
                       const torch::Tensor &nn_index,
                       const torch::Tensor &features,
                       const torch::Tensor &dl_features, torch::Tensor dl_xyz);

  static void ComputeDxyzFeatures(const torch::Tensor &epsilon_distances,
                                  const torch::Tensor &nn_index,
                                  const torch::Tensor &features,
                                  torch::Tensor dl_xyz);

  static void RegisterPybind(pybind11::module &m);
};

}  // namespace slamtb
