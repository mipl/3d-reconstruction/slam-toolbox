#pragma once

#include <torch/torch.h>
#include <torch/csrc/utils/pybind.h>

namespace slamtb {
struct Registration {
  static void ComputeInformationMatrix(const torch::Tensor &correspondences,
                                       const torch::Tensor &tgt_points,
                                       torch::Tensor GtG_partial);
  
  static void RegisterPybind(pybind11::module &m);
};
}  // namespace slamtb
