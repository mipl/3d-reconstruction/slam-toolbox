about:
	@echo "Docker image building"

###
# Base image
##

base-build:
	docker build -t otaviog/slam-toolbox-base:latest --target base .

base-start: base-build
	docker run -it otaviog/slam-toolbox-base:latest bash

###
# Devcontainer image
##
dev-build:
	docker build -t otaviog/slam-toolbox-devcontainer:latest --target devcontainer .

dev-start: dev-build
	docker run --gpus all --user=`id -u`:`id -g` --env="DISPLAY"\
		-e NVIDIA_DRIVER_CAPABILITIES=all\
		-e XAUTHORITY\
		--volume="/etc/group:/etc/group:ro"\
		--volume="/etc/passwd:/etc/passwd:ro"\
		--volume="/etc/shadow:/etc/shadow:ro"\
		--volume="/etc/sudoers.d:/etc/sudoers.d:ro"\
		--volume="/tmp/.X11-unix:/tmp/.X11-unix:rw"\
		--volume=`pwd`:/workspaces/slam-toolbox\
		--workdir=/workspaces/slam-toolbox\
		-it otaviog/slam-toolbox-devcontainer:latest /bin/bash

###
# Main image
##
main-build:
	docker build -t otaviog/slam-toolbox:cudagl-11.2 --target main .

main-push:
	docker push otaviog/slam-toolbox:cudagl-11.2

main-start:
	docker run --gpus all --env="DISPLAY" --user=`id -u`:`id -g`\
		-e NVIDIA_DRIVER_CAPABILITIES=all\
		-e XAUTHORITY\
		--volume="/etc/group:/etc/group:ro"\
		--volume="/etc/passwd:/etc/passwd:ro"\
		--volume="/etc/shadow:/etc/shadow:ro"\
		--volume="/etc/sudoers.d:/etc/sudoers.d:ro"\
		--volume="/tmp/.X11-unix:/tmp/.X11-unix:rw"\
		--volume=`pwd`:`pwd`\
		--workdir=`pwd`\
		-it otaviog/slam-toolbox:cudagl-11.2 bash
