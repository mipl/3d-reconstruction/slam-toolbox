"""A setuptools based setup module for fusion-toolbox
"""

import sys
from os import path

from setuptools import find_packages

try:
    from skbuild import setup
except ImportError:
    print('scikit-build is required to build from source.', file=sys.stderr)
    print('Please run:', file=sys.stderr)
    print('', file=sys.stderr)
    print('  python -m pip install scikit-build')
    sys.exit(1)


HERE = path.abspath(path.dirname(__file__))

# Get the long description from the README file
with open(path.join(HERE, 'README.md'), encoding='utf-8') as f:
    LONG_DESCRIPTION = f.read()

REQUIREMENTS = [
    'kornia',
    'numpy',
    'pykdtree',
    'matplotlib',
    'natsort',
    'numpy-quaternion',
    'scipy',
    'torch'
]


setup(
    name='slam-toolbox',
    version='1.0.0',
    author='Otavio Gomes',
    author_email='otavio.gomes@gmail.com',
    description='Toolbox for building RGBD SLAM prototyping',
    long_description=LONG_DESCRIPTION,
    url='https://gitlab.com/mipl/3d-reconstruction/slam-toolbox',
    license='MIT',
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 3.5'
    ],
    keywords='slam 3d rgbd surfel registration features toolbox',
    packages=find_packages(exclude=['*._test']),
    install_requires=REQUIREMENTS,
    package_data={
        'slamtb.fusion.surfel': ['shaders/*.vert',
                                 'shaders/*.frag',
                                 'shaders/*.geo'],
        'slamtb.viz': ['shaders/*.vert',
                       'shaders/*.frag',
                       'shaders/*.geo']
    },
    entry_points={
        'console_scripts': [
            'stb-dataset-view=slamtb.app.dataset_view.__main__:_main',
        ]
    }
)
