# Contributing to SLAM Toolbox

## Setup Local Developer Mode

The `environment.yml` contains the dependencies. Installing command line:


**Create environment - Option 1**: Using a local `conda` installation

```
slam-toolbox$ conda env create -f environment.yml
```

**Create environment - Option 2**: Using a local `conda` installation

```shell
slam-toolbox$ wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
slam-toolbox$ bash Miniconda3-latest-Linux-x86_64.sh -b -p env
slam-toolbox$ env/bin/activate
slam-toolbox$ conda env update --file environment.yml
```

**Install in setup mode**

```shell
(env) slam-toolbox$ python setup.py develop -- -- -j5
```

## Setup Docker Developer Mode (VSCode)

In `Remote-Containers: Open Folder In Container`, select the `Dockerfile`.
On the `.devcontainer/devcontainer.json` add some useful configurations:

```json
...
	"extensions": [
		"ms-vscode.cpptools",
		"ms-python.python",
		"ms-python.vscode-pylance",
		"ms-vscode.cmake-tools",
		"xaver.clang-format"
	],
	"build": { "target": "devcontainer" },
	"runArgs": [
		"--gpus", "all",
		"-e", "XAUTHORITY",
		"-e", "DISPLAY=${env:DISPLAY}",
		"-v", "/tmp/.X11-unix:/tmp/.X11-unix:rw"
	],

	"postStartCommand": "python /workspaces/slam-toolbox/setup.py develop -- -- -j5"
...
```

**Install in setup mode**

```shell
slam-toolbox$ python setup.py develop -- -- -j5
```