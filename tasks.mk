about:
	@echo "Project maintaining tasks."

pylint:
	python3 -m pylint slamtb

pep8:
	python3 -m autopep8 --recursive --in-place slamtb

unit-test:
	python3 -m unittest discover

doc-create:
	rm -f doc/source/slamtb.*
	sphinx-apidoc -o doc/source slamtb
	make -C doc/ html

doc-open:
	sensible-browser doc/build/html/index.html

clang-format:
	clang-format -i **/*.cpp **/*.hpp

cpp-doc-create:
	doxygen Doxyfile

cpp-doc-open:
	sensible-browser doc/cpp/html/index.html

local-ci:
	gitlab-ci-multi-runner exec docker build:cpp\
		--docker-pull-policy=never

