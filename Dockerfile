FROM nvidia/cudagl:11.2.2-devel-ubuntu20.04 AS base
LABEL maintaner=otavio.b.gomes@gmail.com

RUN apt update && DEBIAN_FRONTEND=noninteractive apt -yq install curl unzip libopenni2-dev\
    libusb-1.0-0-dev git

WORKDIR /
RUN curl -L -O https://repo.anaconda.com/miniconda/Miniconda3-py38_4.10.3-Linux-x86_64.sh 
RUN bash Miniconda3-py38_4.10.3-Linux-x86_64.sh -b -p /miniconda3 && rm Miniconda3-py38_4.10.3-Linux-x86_64.sh
ENV PATH="/miniconda3/bin:${PATH}"

RUN conda install cudatoolkit=11.1 -c pytorch -c conda-forge
RUN conda install pytorch torchvision cudatoolkit=11.1 -c pytorch -c conda-forge

WORKDIR /env

# TODO: remove the next line
RUN python -m pip install scikit-build
RUN git clone https://gitlab.com/mipl/3d-reconstruction/tensorviz.git
RUN conda env update -n base --file tensorviz/environment.yml
RUN pip3 install --no-cache-dir ./tensorviz

RUN git clone --recursive https://github.com/otaviog/onireader.git
RUN pip3 install --no-cache-dir ./onireader

ADD environment.yml .
RUN conda env update -n base --file environment.yml

WORKDIR /workspaces
RUN rm -rf /env

RUN sed --in-place 's/host_defines\.h/cuda_runtime_api\.h/g' /miniconda3/include/eigen3/Eigen/Core
# Hack: Upgrade numpy due to weird error on numpy-quaternion
RUN pip3 install --no-cache-dir -U numpy

RUN pip3 install --no-cache-dir open3d

RUN conda clean --all -y
RUN rm -r /miniconda3/lib/libcuda* /miniconda3/lib/libcuf* /miniconda3/lib/libcub* /miniconda3/lib/libcuso*\
    /miniconda3/lib/libcurand* /miniconda3/pkgs/cudatoolkit* /miniconda3/pkgs/cudnn*

###
# Devcontainer
FROM base AS devcontainer
LABEL maintaner=otavio.b.gomes@gmail.com

RUN apt update && DEBIAN_FRONTEND=noninteractive apt -yq install sudo git byobu emacs\
    clang-format gdb irony-server clang doxygen graphviz cmake-qt-gui nsight-compute

RUN chmod 777 /miniconda3/bin /miniconda3/lib/python3.8/site-packages

ADD requirements-dev.txt .
RUN pip install --no-cache-dir -U pip && pip install --no-cache-dir -r requirements-dev.txt

ARG USERNAME=dev
ARG USER_UID=1000
ARG USER_GID=$USER_UID

# Create the user
RUN groupadd --gid $USER_GID $USERNAME \
    && useradd --uid $USER_UID --gid $USER_GID -m $USERNAME \
    && echo $USERNAME ALL=\(root\) NOPASSWD:ALL > /etc/sudoers.d/$USERNAME \
    && chmod 0440 /etc/sudoers.d/$USERNAME

USER $USERNAME

SHELL ["/bin/bash", "-c"]

ENV NVIDIA_DRIVER_CAPABILITIES ${NVIDIA_DRIVER_CAPABILITIES},display

#####
# Builder image
##
FROM base AS builder

ADD . /slam-toolbox
WORKDIR /slam-toolbox
RUN python setup.py bdist_wheel -- -- -j4

#####
# Main image
##

FROM nvidia/cudagl:11.2.2-runtime-ubuntu20.04 AS main

COPY --from=base /miniconda3 /miniconda3
ENV PATH="/miniconda3/bin/:${PATH}"

COPY --from=builder /slam-toolbox/dist/*.whl .
RUN pip install *.whl && rm *.whl

COPY --from=base /usr/lib/x86_64-linux-gnu/libOpenGL.so.0 /usr/lib/x86_64-linux-gnu/libOpenGL.so.0
ENV NVIDIA_DRIVER_CAPABILITIES ${NVIDIA_DRIVER_CAPABILITIES},display
