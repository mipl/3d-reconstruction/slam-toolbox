# SLAM Toolbox

A toolkit for prototyping RGBD-based simultaneous localization and
mapping (SLAM) algorithms in Python and PyTorch.

The aim of this project is to faciliciate loading of datasets, common preprocessing, visualization and evaluation of 3D reconstruction pipelines. 
This project also explores registration algorithms based on ICP and surfel-based representation of scenes.

This work was supported by the Eldorado Research Institute.


## Source Instalation 

Currently, only source builds are supported.

Requirements:

* Ubuntu>=18.04
* Conda


```shell
$ sudo apt install libopenni2-dev libusb-1.0-0-dev git
```

```shell
$ git clone https://gitlab.com/mipl/3d-reconstruction/slam-toolbox

slam-toolbox$ conda env update -n base --file environment.yml

```

## Docker Environment

The docker image contains a SLAM-toolbox install. The script `slamtb-docker` can launch scripts



## Sample code

* [Dataset parsing](doc/samples/Data.ipynb)
* [Visualization](doc/samples/Visualization.ipynb)
![](doc/samples/viz.png)
* [Processing](doc/samples/Processing.ipynb)
* [Data structures](doc/samples/Data Structures.ipynb)
* [Registration](doc/samples/Registration.ipynb)
* [Surfel fusion]()
* [Evaluation]()

