"""
Fusion of RGBD frames onto a surfel model.
"""
from .fusion import SurfelFusion
