"""Test the fusion process using ground-truth camera poses.
"""

import argparse
from pathlib import Path
import math

import tenviz

from slamtb.data.ftb import load_ftb
from slamtb.data import set_start_at_eye
from slamtb.framepointcloud import FramePointCloud
from slamtb.surfel import SurfelModel
from slamtb.ui import FrameUI, SurfelReconstructionUI, RunMode
from slamtb._utils import profile
from slamtb.fusion.surfel.fusion import SurfelFusion
from slamtb.feature import SIFT


def _test():
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument("--dataset", help="Input FTB dataset")
    parser.add_argument("--device", default="cuda:0")
    parser.add_argument("--stable-conf-thresh", default=10.0, type=float)
    parser.add_argument("--stable-time-thresh", default=20, type=int)
    parser.add_argument("--search-size", default=2, type=int)
    parser.add_argument("--max-merge-distance", default=0.01, type=float)
    parser.add_argument("--depth-cutoff", type=float)
    parser.add_argument("--start-frame", default=0, type=int)
    parser.add_argument("--carve-z-tollerance", default=5e-2, type=float)
    parser.add_argument("--title", default="Surfel fusion test")
    args = parser.parse_args()

    if args.dataset is None:
        test_data = Path(__file__).parent / "../../../../test-data/rgbd"

        dataset = load_ftb(test_data / "sample1")  # 30 frames
        dataset = set_start_at_eye(dataset)
    else:
        dataset = load_ftb(args.dataset)

    gl_context = tenviz.Context()

    model = SurfelModel(gl_context, 1024*1024*50)

    fusion = SurfelFusion(model,
                          normal_max_angle=math.radians(80),
                          stable_conf_thresh=args.stable_conf_thresh,
                          stable_time_thresh=args.stable_time_thresh,
                          search_size=args.search_size,
                          max_merge_distance=args.max_merge_distance,
                          carve_z_toll=args.carve_z_tollerance)
    sensor_ui = FrameUI(args.title)
    rec_ui = SurfelReconstructionUI(model, RunMode.STEP,
                                    stable_conf_thresh=fusion.stable_conf_thresh,
                                    title=args.title)

    if args.start_frame >= len(dataset):
        raise RuntimeError(
            f"Starting frame {args.start_frame} is higher than dataset size")

    sift = SIFT()
    device = args.device
    with profile(Path(__file__).parent / "fusion.prof"):
        for i, _ in enumerate(rec_ui):
            frame_num = args.start_frame + i
            if frame_num == len(dataset):
                break

            frame = dataset[frame_num]
            sensor_ui.update(frame)

            if args.depth_cutoff is not None:
                frame.clamp_max_depth(args.depth_cutoff)
            frame.bilateral_depth_filter()

            fpcl = FramePointCloud.from_frame(frame).to(device)
            fpcl.estimate_normals()

            stats = fusion.fuse(fpcl, frame.info.rt_cam,
                                sparse_features=sift.extract_features(frame.rgb_image))
            print(f"Frame {i} - {stats}")
            print(f"Sparse features count: {len(model.sparse_features)}")


if __name__ == '__main__':
    _test()
