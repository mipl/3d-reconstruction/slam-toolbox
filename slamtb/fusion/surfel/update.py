"""Routine for adding new surfels to a model.
"""
import torch

from slamtb._cslamtb import SurfelFusionOp
from slamtb._utils import empty_ensured_size

from ._merge_map import create_merge_map


class Update:
    """
    Update a surfel model with live surfels and
    returns surfels that should be added.

    Attributes:
        search_size (int): Indexmap pixel neighborhood search size.
        max_normal_angle (float): Maximum angle (radians) between normals
         of surfels that should be merged.
    """

    def __init__(self, search_size, max_normal_angle):
        self.search_size = search_size
        self.max_normal_angle = max_normal_angle
        self._new_surfels_map = None

    def __call__(self, model_indexmap, live_surfels, kcam,
                 rt_cam, time, surfel_model):
        ref_device = model_indexmap.point_confidence.device

        self._new_surfels_map = empty_ensured_size(
            self._new_surfels_map,
            live_surfels.size, dtype=torch.bool,
            device=ref_device)

        model_merge_map = create_merge_map(
            model_indexmap.width, model_indexmap.height, ref_device)
        with surfel_model.gl_context.current():
            with surfel_model.map_as_tensors(ref_device) as mapped_model:

                scale = int(model_indexmap.height / kcam.image_height)
                merge_corresp = torch.full((model_indexmap.width * model_indexmap.height, 2),
                                           -1, device=ref_device, dtype=torch.int64)

                SurfelFusionOp.find_updatable(model_indexmap,
                                              live_surfels._to_cpp(),
                                              kcam.matrix.to(ref_device),
                                              self.max_normal_angle, self.search_size, time,
                                              scale, model_merge_map, self._new_surfels_map,
                                              merge_corresp)

                merge_corresp = merge_corresp[merge_corresp[:, 0] > -1, :]
                SurfelFusionOp.update(merge_corresp,
                                      live_surfels._to_cpp(),
                                      mapped_model,
                                      rt_cam.cam_to_world.float(), time)
                if live_surfels.sparse_features is not None:
                    surfel_model.sparse_features.merge(
                        merge_corresp.cpu(), live_surfels.sparse_features)

        new_surfels = live_surfels[torch.nonzero(
            self._new_surfels_map, as_tuple=False).squeeze()]
        new_surfels.itransform(rt_cam.cam_to_world)
        return new_surfels
