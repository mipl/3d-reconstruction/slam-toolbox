"""
Implements fusion mapping basead on the following works:

    - Keller, Maik, Damien Lefloch, Martin Lambers, Shahram Izadi,
      Tim Weyrich, and Andreas Kolb. "Real-time 3d reconstruction
      in dynamic scenes using point-based fusion." In 2013
      International Conference on 3D Vision-3DV 2013, pp. 1-8.
      IEEE, 2013.
    - Whelan, Thomas, Renato F. Salas-Moreno, Ben Glocker, Andrew
      J. Davison, and Stefan Leutenegger. "ElasticFusion:
      Real-time dense SLAM and light source estimation." The
      International Journal of Robotics Research 35, no. 14
      (2016): 1697-1716.
"""
import math
from typing import Optional

import torch

from slamtb.surfel import SurfelCloud, SurfelModel
from slamtb.feature import IntFeatureDict
from slamtb.framepointcloud import FramePointCloud
from slamtb.camera import RTCamera

from .indexmap import SurfelIndexMapRaster
from .update import Update
from .merge import Merge
from .carve_space import CarveSpace
from .clean import Clean
from .stats import FusionStats


class SurfelFusion:
    """Implements fusion/integration of RGBD frame point ground into a surfel model.

    Besides integration dense frames, this implementation also fuses sparse 3D points with
     features.

    Attributes:
        model: The target surfel model which the input frames will be integrated into.
        normal_max_angle: Maximum angle to merge surfels.
        stable_conf_thresh: Stable confidence threshold,
         surfels with lower confidence than this and at certain time are
         removed.
        stable_time_thresh: Stable time threshold. Minimum
         time to remove surfels.
        search_size: Window search size on the indexmap.
        indexmap_scale: How times the frame dimensions that
         the indexmap image should be.
        max_merge_distance: Surfels with greater distance than
         this aren't merged.
        carve_z_toll: Z distance before carving out a in front
         surfel of another.
    """

    def __init__(self, model: SurfelModel, normal_max_angle: float = math.radians(30),
                 stable_conf_thresh: float = 10, stable_time_thresh: float = 20,
                 search_size: int = 2, indexmap_scale: float = 4,
                 max_merge_distance: float = 0.003,
                 carve_z_toll: float = 5e-2):
        self.model = model
        self.model_raster = SurfelIndexMapRaster(model)

        self._update = Update(
            max_normal_angle=normal_max_angle,
            search_size=search_size)

        self._carve = CarveSpace(stable_conf_thresh=stable_conf_thresh,
                                 stable_time_thresh=stable_time_thresh,
                                 z_tollerance=carve_z_toll)
        self._merge = Merge(max_merge_distance, normal_max_angle=normal_max_angle,
                            search_size=search_size,
                            stable_conf_thresh=stable_conf_thresh)
        self._clean = Clean(stable_conf_thresh=stable_conf_thresh,
                            stable_time_thresh=stable_time_thresh)

        self.indexmap_scale = indexmap_scale
        self._time = 0

    def fuse(self, frame_pcl: FramePointCloud, rt_cam: RTCamera,
             confidence_weight: float = 1.0,
             features: Optional[torch.Tensor] = None,
             sparse_features: IntFeatureDict = None) -> FusionStats:
        """
        Fuse a RGB-D frame into the model's geometry.

        Args:
            frame_pcl: The input RGB-D frame.
            rt_cam: Rigid transformation camera for the input frame.
            confidence_weight: Weight for the surfel confidences computed in this frame.
            features: Optional dense features per pixel.
        """
        live_surfels = SurfelCloud.from_frame_pcl(
            frame_pcl, time=self._time,
            dense_features=features, confidence_weight=confidence_weight,
            sparse_features=sparse_features)

        gl_proj_matrix = frame_pcl.kcam.get_opengl_projection_matrix(
            0.01, 100.0, dtype=torch.float)
        height, width = frame_pcl.image_points.shape[:2]

        if self._time == 0:
            live_surfels.itransform(rt_cam.camera_to_world)
            self.model.add_surfels(live_surfels, update_gl=True)
            self._time += 1
            self.model.max_time = 1
            self.model.max_confidence = live_surfels.confidences.max()

            return FusionStats(live_surfels.size, 0, 0)

        stats = FusionStats()

        ###
        # Update
        indexmap_size = int(
            width*self.indexmap_scale), int(height*self.indexmap_scale)
        self.model_raster.raster(gl_proj_matrix, rt_cam,
                                 indexmap_size[0], indexmap_size[1])
        model_indexmap = self.model_raster.to_indexmap()

        new_surfels = self._update(
            model_indexmap, live_surfels, frame_pcl.kcam,
            rt_cam, self._time, self.model)
        self.model.add_surfels(new_surfels, update_gl=True)
        stats.added_count = new_surfels.size

        ####
        # Clean
        stats.removed_count = self._clean(
            frame_pcl.kcam, frame_pcl.rt_cam,
            model_indexmap, self._time, self.model, update_gl=True)

        ####
        # Merge & Carve Raster
        self.model_raster.raster(gl_proj_matrix, rt_cam,
                                 indexmap_size[0], indexmap_size[1])
        model_indexmap = self.model_raster.to_indexmap()

        # Merge
        stats.merged_count = self._merge(
            model_indexmap, self.model, update_gl=True)

        # Carve
        # stats.carved_count += self._carve(frame_pcl.kcam, rt_cam, model_indexmap,
        #                                  self._time, self.model)

        self.model.update_gl()
        self._time += 1
        self.model.max_time = self._time

        return stats

    @property
    def stable_conf_thresh(self) -> float:
        """The surfel confidence threshold value for not being removed.
        """
        return self._clean.stable_conf_thresh

    def reset(self):
        """
        Deletes all surfels and resets the fusion state.
        """
        self._time = 0
        self.model.clear()
