"""
Operator for deleting unstable surfels.
"""
import torch

from slamtb._cslamtb import SurfelFusionOp, IndexMap
from slamtb.camera import KCamera, RTCamera
from slamtb.surfel import SurfelModel


class Clean:
    """
    Surfel cleaning operator. It will delete those surfels that are bellow the stable
    confidence threshold and above the stable time threshold.

    Attributes:
        stable_conf_thresh: The stable confidence threshold that surfels should have to stay.
        stable_time_thresh: Maximum age that a surfel can be unstable.
    """

    def __init__(self, stable_conf_thresh: float = 10,
                 stable_time_thresh: float = 20):
        self.stable_conf_thresh = stable_conf_thresh
        self.stable_time_thresh = stable_time_thresh

    def __call__(self, kcam: KCamera, rt_cam: RTCamera, indexmap: IndexMap, time: int,
                 model: SurfelModel, update_gl: bool = False):
        ref_device = model.device
        allocated_indices = model.allocated_indices().to(ref_device)

        if allocated_indices.size(0) == 0:
            return 0

        remove_mask = torch.zeros(allocated_indices.size(0), device=ref_device,
                                  dtype=torch.bool)

        with model.gl_context.current():
            with model.map_as_tensors(ref_device) as mapped_model:
                SurfelFusionOp.clean(mapped_model, allocated_indices,
                                     time, self.stable_time_thresh,
                                     self.stable_conf_thresh,
                                     remove_mask)

        removed_indices = allocated_indices[remove_mask]
        if removed_indices.size(0) == 0:
            return 0

        model.free(removed_indices, update_gl)
        return removed_indices.size(0)
