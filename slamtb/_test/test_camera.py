"""Testing of the slamtb.camera module.
"""
import unittest
from pathlib import Path
import math

import torch
import fire

from slamtb.camera import (Project, RigidTransform)
from slamtb.data.ftb import load_ftb
from slamtb.frame import FramePointCloud
from slamtb.metrics import (translational_difference,
                            rotational_difference)
from slamtb.testing import (load_sample1_dataset as load_sample_dataset,
                            get_sample1_matchings as get_sample_matchings)


class TestCamera(unittest.TestCase):
    def test_rigid_transform(self):
        transform = RigidTransform.create_rand(180, 10)

        points = torch.rand(100, 4)
        points[:, 3] = 1

        reference_result = transform.matrix @ points.view(-1, 4, 1)
        reference_result = reference_result.squeeze()[:, :3]

        # Standard
        result = transform @ points[:, :3]
        torch.testing.assert_allclose(reference_result, result)

        # Inplace
        result = points[:, :3].clone()
        result = transform.inplace(result)
        torch.testing.assert_allclose(reference_result, result)

        # Inplace normals
        normals = torch.rand(100, 3)
        normals = normals / normals.norm(2, dim=1).view(-1, 1)
        reference_result = transform.transform_normals(normals)

        for device in ["cpu", "cuda:0"]:
            result = normals.clone().to(device)
            transform.inplace_normals(result)
            torch.testing.assert_allclose(reference_result, result.cpu())

    def test_rigid_transform_fit(self):
        dataset = load_sample_dataset()

        frame0_idx, yx0, frame1_idx, yx1 = get_sample_matchings()

        frame0 = FramePointCloud.from_frame(dataset[frame0_idx])
        frame1 = FramePointCloud.from_frame(dataset[frame1_idx])

        points0 = frame0.points[yx0[:, 0], yx0[:, 1], :]
        points1 = frame1.points[yx1[:, 0], yx1[:, 1], :]

        transform = RigidTransform.fit_points(points0, points1)

        gt_transform = frame1.rt_cam.matrix

        self.assertLess(translational_difference(
            gt_transform, transform.matrix), 1)
        self.assertLess(math.degrees(
            rotational_difference(gt_transform, transform.matrix)), 10)

    def test_project(self):
        proj = Project.apply
        for dev in ["cpu:0", "cuda:0"]:
            torch.manual_seed(10)
            input = (torch.rand(3, dtype=torch.double, requires_grad=True),
                     torch.tensor([[45.0, 0, 214],
                                   [0, 47.5, 186]], dtype=torch.double))

            torch.autograd.gradcheck(proj, input, eps=1e-6, atol=1e-4,
                                     raise_exception=True)


class InteractiveTests:
    def project(self):
        import matplotlib.pyplot as plt
        import numpy as np

        from slamtb.frame import FramePointCloud

        dataset = load_ftb(Path(__file__).parent /
                           "../../test-data/rgbd/sample2")
        frame = dataset[0]
        pcl = FramePointCloud.from_frame(frame).unordered_point_cloud(
            world_space=False, compute_normals=False)

        uvs = Project.apply(pcl.points, frame.info.kcam.matrix)

        img = np.zeros((frame.rgb_image.shape[0], frame.rgb_image.shape[1]))

        for uv in uvs:

            u = int(round(uv[0].item()))
            v = int(round(uv[1].item()))
            f = frame.rgb_image[v, u, 0]

            img[v, u] = f

        plt.figure()
        plt.imshow(frame.rgb_image[:, :, 0])

        plt.figure()
        plt.imshow(img)
        plt.show()


if __name__ == '__main__':
    fire.Fire(InteractiveTests())
