from pathlib import Path

import fire
import torch
import quaternion
import numpy as np

import tenviz

from slamtb.frame import FramePointCloud
from slamtb.viz.surfelrender import show_surfels
from slamtb.data import set_start_at_eye
from slamtb.data.ftb import load_ftb
from ..surfel import SurfelModel, SurfelCloud, SurfelVolume, merge_surfels


class TestDatatype:
    def adding(self):
        device = "cpu:0"
        test_data = Path(__file__).parent / "../../test-data/rgbd"
        dataset = load_ftb(test_data / "sample2")

        live_surfels = SurfelCloud.from_frame_pcl(
            FramePointCloud.from_frame(dataset[0]))

        gl_context = tenviz.Context()
        model = SurfelModel(gl_context, live_surfels.size)
        model.add_surfels(live_surfels, update_gl=True)
        show_surfels(gl_context, [model])

    def transform(self):
        dataset = load_ftb(Path(__file__).parent /
                           "../../test-data/rgbd/sample2")
        surfels0 = SurfelCloud.from_frame(dataset[0])
        surfels1 = surfels0.clone()

        transformation = torch.eye(4)
        np.random.seed(5)
        transformation[:3, :3] = torch.from_numpy(quaternion.as_rotation_matrix(
            quaternion.from_euler_angles(np.random.rand(3))))
        transformation[0, 3] = 2
        transformation[1, 3] = 0
        transformation[2, 3] = 2

        surfels1.itransform(transformation)

        show_surfels(tenviz.Context(), [
                     surfels0, surfels1, surfels0.transform(transformation)])

    def downsample(self):
        dataset = load_ftb(Path(__file__).parent /
                           "../../test-data/rgbd/sample2")

        surfels = SurfelCloud.from_frame(dataset[0])
        surfels.features = torch.rand(16, surfels.size)

        downsampled = [surfels.downsample(voxel_size)
                       for voxel_size in [0.0005, 0.005, 0.05]]

        show_surfels(tenviz.Context(), [surfels] + downsampled,
                     view_matrix=torch.tensor(
                         [[-0.974194, 0, -0.225711, 1.39023],
                          [-0.0282925, 0.992113, 0.122114, -0.611669],
                          [0.223931, 0.125348, -0.966511, 0.792993],
                          [0, 0, 0, 1]]))

    def merge(self):
        dataset = set_start_at_eye(load_ftb(Path(__file__).parent /
                                            "../../test-data/rgbd/sample2"))
        frame0 = dataset[0]
        frame1 = dataset[14]

        torch.manual_seed(10)

        surfels0 = SurfelCloud.from_frame(frame0)
        surfels0.features = torch.rand(16, surfels0.size)
        surfels0.itransform(frame0.info.rt_cam.cam_to_world)

        surfels1 = SurfelCloud.from_frame(frame1)
        surfels1.itransform(frame1.info.rt_cam.cam_to_world)
        surfels1.features = torch.rand(16, surfels1.size)

        surfels2 = merge_surfels([surfels0, surfels1], 0.0005)

        show_surfels(tenviz.Context(),
                     [surfels0, surfels1, surfels2],
                     view_matrix=torch.tensor(
                         [[-0.974194, 0, -0.225711, 1.39023],
                          [-0.0282925, 0.992113, 0.122114, -0.611669],
                          [0.223931, 0.125348, -0.966511, 0.792993],
                          [0, 0, 0, 1]]))


if __name__ == '__main__':
    fire.Fire(TestDatatype)
