#!/usr/bin/env python

from pathlib import Path

import fire
import tenviz

from slamtb.geometry import sample_points_in_mesh
from slamtb.pointcloud import PointCloud
from slamtb.viz import geoshow


def test_sample_points_in_mesh():
    mesh_file = Path(__file__).absolute().parent.parent.parent / \
        "test-data/bunny/bun_zipper_res4.ply"

    mesh = tenviz.io.read_3dobject(mesh_file).torch()
    mesh.normals = tenviz.geometry.compute_normals(mesh.verts, mesh.faces)
    points, normals = sample_points_in_mesh(
        mesh.verts, mesh.normals, mesh.faces, 6000)

    pcl = PointCloud(points, normals=normals)

    geoshow([mesh, pcl])


if __name__ == '__main__':
    fire.Fire({
        "point-sampling": test_sample_points_in_mesh
    })
