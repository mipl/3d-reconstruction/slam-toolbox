#!/usr/bin/env python
"""
Testing of PointCloud class
"""
import unittest
import pickle

from fire import Fire
import torch

from slamtb.pointcloud import PointCloud
from slamtb.viz import geoshow
from slamtb.testing import load_sample2_dataset


class PointCloudTest(unittest.TestCase):
    """Tests the point cloud structure.
    """

    def test_init(self):
        """Tests the initialization
        """
        pcl1 = PointCloud(torch.rand(10, 3),
                          torch.ones(10, 3, dtype=torch.uint8))

        self.assertEqual(10, pcl1.size)
        self.assertIsNotNone(pcl1.points)
        self.assertIsNotNone(pcl1.colors)
        self.assertIsNone(pcl1.normals)
        self.assertIsNone(pcl1.features)
        self.assertEqual(0, pcl1.feature_size)

    def test_downsample(self):
        """Check downsampling
        """
        dataset = load_sample2_dataset()

        pcl = PointCloud.from_frame(dataset[0])[0]
        downsample1 = pcl.downsample(0.01)

        pcl.features = torch.rand(10, pcl.size)
        downsample2 = pcl.downsample(0.01)

        pcl.features = None
        pcl.colors = None
        pcl.normals = None

        downsample3 = pcl.downsample(0.01)

        self.assertEqual(downsample1.size, downsample2.size)
        self.assertEqual(downsample1.size, downsample3.size)

    def test_pickle(self):
        """Test pickling support
        """
        dataset = load_sample2_dataset()

        pcl = PointCloud.from_frame(dataset[0])[0]
        pcl.features = torch.rand(10, pcl.size)

        stream = pickle.dumps(pcl)
        pcl2 = pickle.loads(stream)

        torch.testing.assert_allclose(pcl.points, pcl2.points)
        torch.testing.assert_allclose(pcl.colors, pcl2.colors)
        torch.testing.assert_allclose(pcl.normals, pcl2.normals)
        torch.testing.assert_allclose(pcl.features, pcl2.features)


class _Testing:
    @staticmethod
    def downsample():
        """Visually tests the point cloud downsampling"""

        dataset = load_sample2_dataset()
        pcl = PointCloud.from_frame(dataset[0])[0]

        pcl_downsample = pcl.downsample(0.01)
        geoshow([pcl, pcl_downsample])


if __name__ == '__main__':
    Fire(_Testing())
