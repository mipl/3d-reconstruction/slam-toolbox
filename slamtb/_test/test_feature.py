#!/usr/bin/env python
"""Tests the sparse feature set
"""
import typing
import unittest
import io
import sys

from fire import Fire
import torch
import matplotlib.pyplot as plt

from slamtb.feature import (IntFeatureDict, KeypointFeatures,
                            SIFT, match_features)
from slamtb.testing import load_sample1_dataset
from slamtb.registration.sparse import (estimate_essential_matrix,
                                        SparseFeatureRegistration)
from slamtb.viz.matchingsviewer import draw_trackings_3d, draw_matchings_2d
from slamtb.frame import FramePointCloud
from slamtb.util import masked_yx_to_linear


class TestIntFeatureDict(unittest.TestCase):
    """Tests the sparse feature set
    """

    def setUp(self):
        """Set up a test point set"""
        yx = torch.tensor([[6, 7],
                           [10, 8],
                           [14, 22],
                           [18, 13],
                           [23, 21]], dtype=torch.int32)
        features = torch.rand(5, 16, dtype=torch.float)
        mask = torch.ones(25, 25, dtype=torch.bool)
        mask[1, 1] = False
        mask[5, 5] = False
        mask[10, 9] = False
        mask[19, 0] = False

        # index = 155, 256, 369, 460 and 592
        self.sparse_set = IntFeatureDict.from_keypoint_features(
            KeypointFeatures(yx, features), mask)

    def test_init(self):
        """Verifies the constructors"""
        dict_ver = self.sparse_set.as_dict()
        self.assertEqual([155, 256, 369, 460, 592], list(dict_ver.keys()))

    def test_merge(self):
        """Verify merging two sparse feature set"""
        mask = torch.ones(25, 25, dtype=torch.bool)
        mask[1, 1] = False
        mask[5, 5] = False
        mask[10, 9] = False
        mask[19, 0] = False
        # point_ids = 179, 208, 366, 560, 570
        other_sparse_set = IntFeatureDict(
            torch.tensor([[6, 7],  # Hit
                          [10, 8],  # Hit
                          [14, 22],  # Hit
                          [19, 14],  # Miss
                          [24, 22]],  # Miss,
                         dtype=torch.int32),
            torch.rand(5, 16, dtype=torch.float),
            mask)

        merge_corresp = torch.tensor(
            [[155, 155],
             [256, 256],
             [369, 485]],
            dtype=torch.int64)
        self.sparse_set.merge(merge_corresp, other_sparse_set)
        dict_ver = self.sparse_set.as_dict()

        self.assertEqual(2.0, dict_ver[155][0])
        self.assertEqual(2.0, dict_ver[256][0])
        self.assertEqual(2.0, dict_ver[369][0])
        self.assertEqual(1.0, dict_ver[460][0])
        self.assertEqual(1.0, dict_ver[592][0])

        self.assertEqual(5, len(dict_ver))

    def test_intern_merge(self):
        """Verify merging intern features"""

        self.sparse_set.merge(
            torch.tensor([[155, 256],
                          [369, 592]], dtype=torch.int64))
        self.assertEqual(3, len(self.sparse_set))

        dict_ver = self.sparse_set.as_dict()
        self.assertTrue(155 in dict_ver)
        self.assertTrue(369 in dict_ver)
        self.assertTrue(460 in dict_ver)

    def test_add(self):
        """Test adding.
        """
        mask = torch.ones(25, 25, dtype=torch.bool)
        other_sparse_set = IntFeatureDict(
            torch.tensor([[12, 14],  # 202
                          [8, 2],  # 314
                          [24, 21]],  # 621
                         dtype=torch.int32),
            torch.rand(7, 16, dtype=torch.float), mask)

        dense_ids = torch.arange(25*25, dtype=torch.int64) + 500
        self.sparse_set.add(dense_ids, other_sparse_set)

        dict_ver = self.sparse_set.as_dict()

        self.assertEqual(8, len(dict_ver))
        self.assertTrue(702 in dict_ver)
        self.assertTrue(814 in dict_ver)
        self.assertTrue(1121 in dict_ver)

    def test_serialization(self):
        """Tests the serialization and deserialization for pickling it.
        """
        file = io.BytesIO()
        torch.save(self.sparse_set, file)
        file.seek(0, 0)
        new = torch.load(file).as_dict()
        old = self.sparse_set.as_dict()

        self.assertEqual(old.keys(), new.keys())


class _TestPointCloud:
    def __init__(self, frame, kfeats):
        fpcl = FramePointCloud.from_frame(frame)

        kfeats = kfeats.masked_select(kfeats.responses > 0.025)
        self.features = IntFeatureDict.from_keypoint_features(
            kfeats, fpcl.mask)
        self.pcl = fpcl.unordered_point_cloud(
            world_space=False, compute_normals=False)

def _track_linear_matchings(match_list: typing.List[torch.Tensor]) -> torch.Tensor:
    @dataclass
    class _TrackItem:
        frame_index: int
        descriptor_row: int

    trackings = {}
    track_count = 0

    track_map = {}
    for i, matches in enumerate(match_list):
        matches = match_list[i]
        j = i + 1

        next_track_map = {}
        for idx1, idx2 in matches:
            idx1 = idx1.item()
            idx2 = idx2.item()
            if idx1 not in track_map:
                trackings[track_count] = [
                    _TrackItem(i, idx1), _TrackItem(j, idx2)]
                track_map[idx1] = track_count
                next_track_map[idx2] = track_count
                track_count += 1
            else:
                track_id = track_map[idx1]
                trackings[track_id].append(_TrackItem(j, idx2))

        track_map = next_track_map

    tracking_matrix = torch.full((len(trackings), len(match_list) + 1),
                                 -1, dtype=torch.int64)
    for i, track in enumerate(trackings.values()):
        for item in track:
            tracking_matrix[i, item.frame_index] = item.descriptor_row

    return tracking_matrix

class _Testing:
    @staticmethod
    def matching():
        """
        Demonstrate feature matching and related visualizations.
        """
        dataset = load_sample1_dataset()

        frames = [dataset[0], dataset[10], dataset[15]]
        point_clouds = [FramePointCloud.from_frame(frame)
                        for frame in frames]

        sift = SIFT()
        keypoint_features = [sift.extract_features(
            frame.rgb_image) for frame in frames]
        keypoint_features = [kfeats.masked_select(pcl.mask)
                             for kfeats, pcl in zip(keypoint_features, point_clouds)]
        keypoint_features = [kfeats.masked_select(kfeats.responses > 0.025)
                             for kfeats in keypoint_features]
        match_list = []
        for i in range(len(keypoint_features) - 1):
            matches = match_features(keypoint_features[i].descriptors,
                                     keypoint_features[i + 1].descriptors, 100)
            _, inliers = estimate_essential_matrix(
                keypoint_features[i].keypoints,
                keypoint_features[i+1].keypoints,
                frames[0].info.kcam, matches)
            matches = matches[inliers, :]

            plt.figure()
            plt.title(f"Frame {i} to {i+1}")
            draw_matchings_2d(frames[i].rgb_image, frames[i+1].rgb_image,
                              keypoint_features[i].keypoints, keypoint_features[i+1].keypoints,
                              matches)

            match_list.append(matches)
        plt.show()

        trackings = _track_linear_matchings(match_list)

        yx_to_pcl_index = [masked_yx_to_linear(kfeats.keypoints, pcl.mask)
                           for kfeats, pcl in zip(keypoint_features, point_clouds)]

        for i in range(trackings.size(0)):
            for j, yx_index in enumerate(yx_to_pcl_index):
                yx_id = trackings[i, j]
                if yx_id < 0:
                    continue

                point_id = yx_index[yx_id]
                if point_id > -1:
                    trackings[i, j] = point_id

        draw_trackings_3d(
            [pcl.unordered_point_cloud(world_space=False, compute_normals=False)
             for pcl in point_clouds],
            trackings)

    @staticmethod
    def sparse_matching():
        """
        Demonstrate feature matching and related visualizations.
        """
        dataset = load_sample1_dataset()

        sift = SIFT()
        pcls = [_TestPointCloud(frame, sift.extract_features(frame.rgb_image))
                for frame in [dataset[0], dataset[10], dataset[15]]]

        match_list = []
        for i in range(len(pcls) - 1):
            matches = match_features(pcls[i].features.descriptors(),
                                     pcls[i + 1].features.descriptors(),
                                     100)
            reg = SparseFeatureRegistration()
            _, inliers = reg.estimate(
                pcls[i].pcl.points, pcls[i + 1].pcl.points, matches)
            matches = matches[inliers, :]
            match_list.append(matches)
        plt.show()

        trackings = _track_linear_matchings(match_list)

        for i in range(trackings.size(0)):
            for j, pcl in enumerate(pcls):
                continous_index = trackings[i, j]
                if continous_index < 0:
                    continue

                point_id = pcl.features.keys()[continous_index]
                if point_id > -1:
                    trackings[i, j] = point_id

        draw_trackings_3d(
            [pcl.pcl for pcl in pcls],
            trackings)

    @staticmethod
    def unittest():
        """
        Run the unittest on this file.
        """
        unittest.main(argv=[sys.argv[0]])


if __name__ == '__main__':
    Fire(_Testing)
