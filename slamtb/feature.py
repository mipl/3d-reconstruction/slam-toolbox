"""
Feature extractor and matching utilities.
"""

import typing

import torch
import numpy as np
import cv2

from slamtb.util import masked_yx_to_linear
from ._cslamtb import IntFeatureDict as _IntFeatureDict


class KeypointFeatures:
    """
    Stores features descriptors and its xy coordinates extracted from images.

    Attributes:

        points: YX coordinates, [Nx2] int32 tensor.
        descriptors: Descriptors, [NxD] float tensor.
        responses: Responses indicating stronger keypoints,
         [N] float tensor.
    """

    def __init__(self, keypoints: torch.Tensor,
                 descriptors: torch.Tensor,
                 responses: typing.Union[torch.Tensor, None] = None):
        self.keypoints = keypoints
        self.descriptors = descriptors
        self.responses = responses

    def masked_select(self, mask: torch.Tensor):
        """
        Mask-select keypoint/descriptor/response according to a mask.

        Args:

            mask: A bool mask of shape [M] or [HxW].

        Returns:
            Sliced features.
        """
        if mask.ndim == 1:
            return KeypointFeatures(self.keypoints[mask, :],
                                    self.descriptors[mask, :],
                                    self.responses[mask])

        mask = torch.tensor([mask[yx[0], yx[1]]
                             for yx in self.keypoints],
                            dtype=torch.bool)

        return KeypointFeatures(self.keypoints[mask, :],
                                self.descriptors[mask, :],
                                self.responses[mask])

    def descriptor_size(self) -> int:
        """
        Returns the size of the descriptors' row.
        """
        return self.descriptors.size(1)

    def __len__(self):
        """
        Returns the number of feature points.
        """
        return self.keypoints.size(0)

    def __str__(self):
        return f"KeypointFeatures(len()={len(self)}, descriptor_size={self.descriptor_size})"

    def __repr__(self):
        return self.__str__()


class IntFeatureDict(_IntFeatureDict):
    """
    Stores features like a dictionary of ints to torch tensors, however,
    the features should all have the same dimensions.
    """
    @classmethod
    def from_keypoint_features(cls, features2d: KeypointFeatures, mask: torch.Tensor):
        """
        Creates a feature set where the keys are 2D flatten indices.

        Args:
            features2d: Features indexed by xy coordinates.

            mask: Boolean 2D mask image informing valid image indices.
        """
        linear = masked_yx_to_linear(
            features2d.keypoints, mask)
        valid = linear > -1

        return cls(linear[valid], features2d.descriptors[valid, :])


class SIFT:
    """
    Wrapper around the OpenCV library to extract SIFT features.
    """

    def __init__(self):
        self.extractor = cv2.SIFT_create()

    def extract_features(self, rgb_image: np.ndarray):
        """
        Extract feature.

        Args:
            rgb_image: A GRAY or RGB image [HxWx3] uint8.

        Returns: (:obj:`Features2D`):
            Extracted 2D features.

        """

        if rgb_image.ndim > 2:
            image = cv2.cvtColor(rgb_image, cv2.COLOR_RGB2GRAY)

        keypoint_objs, descriptors = self.extractor.detectAndCompute(
            image, None)

        select_kps = []
        keypoint_set = set()
        for i, kpt in enumerate(keypoint_objs):
            keypoint = (int(kpt.pt[0]), int(kpt.pt[1]))

            if keypoint in keypoint_set:
                continue

            select_kps.append(i)
            keypoint_set.add(keypoint)

        descriptors = descriptors[np.array(select_kps, dtype=np.int64), :]

        keypoints = torch.empty((len(select_kps), 2), dtype=torch.int64)
        responses = torch.empty(len(select_kps), dtype=torch.float)

        for i, j in enumerate(select_kps):
            kpt = keypoint_objs[j]

            keypoints[i, 0] = kpt.pt[1]
            keypoints[i, 1] = kpt.pt[0]
            responses[i] = kpt.response

        return KeypointFeatures(keypoints, torch.from_numpy(descriptors),
                                responses)


class ORB:
    """
    Wrapper around the OpenCV library to extract ORB features.
    """

    def __init__(self):
        self.extractor = cv2.ORB_create()

    def extract_features(self, rgb_image: np.ndarray):
        """
        Extract feature.

        Args:
            rgb_image: A GRAY or RGB image [HxWx3] uint8.

        Returns: (:obj:`Features2D`):
            Extracted 2D features.

        """

        if rgb_image.ndim > 2:
            image = cv2.cvtColor(rgb_image, cv2.COLOR_RGB2GRAY)

        keypoint_objs, descriptors = self.extractor.detectAndCompute(
            image, None)

        keypoints = torch.empty((len(keypoint_objs), 2), dtype=torch.int64)
        responses = torch.empty(len(keypoint_objs), dtype=torch.float)

        for i, kpt in enumerate(keypoint_objs):
            kpt = keypoint_objs[i]

            keypoints[i, 0] = kpt.pt[1]
            keypoints[i, 1] = kpt.pt[0]
            responses[i] = kpt.response

        return KeypointFeatures(keypoints, torch.from_numpy(descriptors),
                                responses)


def match_features(descriptors1: torch.Tensor, descriptors2: torch.Tensor,
                   max_distance: float, distance_mode=cv2.NORM_L2) -> torch.Tensor:
    """
    Wrapper around OpenCV's brute force matcher for matching descriptors.

    Args:
        descriptors1: First descriptor set, shape must be [NxD].
        descriptors2: Second descriptor set, shape must be [MxD].
        max_distance: Matchings with higher distance are discarded.
        distance_mode: OpenCV's distance mode.

    Returns:
        Matched features indices, shape is [Qx2] of type int64.
         First column corresponds to the first descriptor1 indices,
         and the so on.
    """

    matcher = cv2.BFMatcher(distance_mode)
    matchings = matcher.match(descriptors1.numpy(), descriptors2.numpy())
    matchings = torch.tensor(
        # desc1 idx, desc2 idx
        [(match.queryIdx, match.trainIdx) for match in matchings
         if match.distance < max_distance], dtype=torch.int64)

    return matchings
