"""Camera's intrinsic and extrinsic handling.
"""
import math
import copy
import random
from typing import Any, Dict, Iterable, List, Tuple

import numpy as np
from scipy.spatial.transform import Rotation as _R
import quaternion
import torch
import tenviz

from slamtb._utils import ensure_torch, ensure_numpy
from slamtb._cslamtb import (ProjectOp as _ProjectOp,
                             RigidTransformOp as _RigidTransformOp)

_GL_HAND_MTX = torch.eye(4, dtype=torch.float32)
_GL_HAND_MTX[2, 2] = -1


class KCamera:
    """Intrinsic pinhole camera model for projecting and backprojecting points.

    Attributes:
        matrix (:obj:`torch.Tensor`): A (3 x 3) intrinsic camera
         matrix. Should convert 3D points in camera space into uv image space.
        undist_coeff (List[float], optional): Radial distortion
         coefficients.
        image_size ((int, int), optional): Pixel width and height of the
         target image.

    """

    def __init__(self, matrix, undist_coeff=None, image_size=None):
        self.matrix = ensure_torch(matrix)

        if undist_coeff is not None:
            self.undist_coeff = undist_coeff
        else:
            self.undist_coeff = []

        self.image_size = image_size

    @classmethod
    def from_json(cls, json: Dict):
        """Constructs from the FTB's JSON representation.
        """
        return cls(torch.tensor(json['matrix'], dtype=torch.float).view(-1, 3),
                   undist_coeff=json.get('undist_coeff', None),
                   image_size=json.get('image_size', None))

    def to_json(self) -> Dict:
        """Converts the camera intrinsic to its FTB JSON dict representation.

        Returns:
            Dict ready for json dump.
        """
        json = {
            'matrix': self.matrix.tolist(),
        }

        if self.undist_coeff is not None:
            json['undist_coeff'] = self.undist_coeff

        if self.image_size is not None:
            json['image_size'] = self.image_size

        return json

    @classmethod
    def from_params(cls, flen_x: float, flen_y: float, center_point: Tuple[float, float],
                    undist_coeff: List[float] = None, image_size: Tuple[int, int] = None):
        """
        Computes the intrinsic matrix from given focal lengths and center
        point.

        Args:

            flen_x: X-axis focal length.
            flen_y: Y-axis focal length.
            center_point: Camera's central point in
             image space.
            undist_coeff (optional): Radial distortion
             coefficients.
            image_size (optional): Width and height of the
             target image.
        """
        center_x, center_y = center_point
        k_trans = torch.tensor([[1.0, 0.0, center_x],
                                [0.0, 1.0, center_y],
                                [0.0, 0.0, 1.0]])
        k_scale = torch.tensor([[flen_x, 0.0, 0.0],
                                [0.0, flen_y, 0.0],
                                [0.0, 0.0, 1.0]])
        return cls(k_trans @ k_scale, undist_coeff, image_size)

    @classmethod
    def from_estimation_by_fov(cls, hfov: float, vfov: float, img_width: int, img_height: int):
        """Create intrinsic using the ratio of image pixel dimensions and
        field of view angles. This is useful for guessing intrinsic
        from a supplied field of views. However, without the actual
        sensor width and height, it's not possible to generate
        accurate focal lengths.

        Args:
            hfov: Horizontal field of view in radians.
            vfov: Vertical field of view in radians.
            img_width: Image width in pixels.
            img_height: Image height in pixels.
        """

        flen_x = img_width * .5 / math.tan(hfov/2)
        flen_y = img_height * .5 / math.tan(vfov/2)

        return cls.from_params(flen_x, flen_y, (img_width*.5, img_height*.5))

    def backproject(self, points: torch.Tensor) -> torch.Tensor:
        """Back project 2D image coordinates and its z value into 3D camera space.

        Args:
            points: Array (N x 3) of 2D image points and z values.
             The columns are expected to represent u, v and z.

        Returns:
            Array (N x 3) of 3D points in camera space.
        """

        xyz_coords = points.clone()
        fx = self.matrix[0, 0]
        fy = self.matrix[1, 1]
        cx = self.matrix[0, 2]
        cy = self.matrix[1, 2]

        z = xyz_coords[:, 2]
        xyz_coords[:, 0] = (xyz_coords[:, 0] - cx) * z / fx
        xyz_coords[:, 1] = (xyz_coords[:, 1] - cy) * z / fy

        return xyz_coords

    def project(self, points: torch.Tensor) -> torch.Tensor:
        """Project 3D points from camera space to image space.

        Applies division by z.

        Args:
            points: Array (Nx3) of 3D points in camera space.

        Returns:
            Array (Nx2) of 2D image points.

        """

        points = (self.matrix @ points.reshape(-1, 3, 1)).reshape(-1, 3)

        z = points[:, 2]

        points[:, :2] /= z.reshape(-1, 1)
        return points

    def scaled(self, xscale: float, yscale: float = None):
        """
        Returns intrinsic parameters adjusted for a new size scale.

        Args:
            xscale:  Horizontal scaling factor. Global scale
             if yscale is not specified.
            yscale (optional): Vertical scaling factor,
             if not specified, then the same scale of `xscale` is used.

        Returns:
            Scaled intrinsic parameters.
        """
        if yscale is None:
            yscale = xscale

        image_size = None
        if self.image_size is not None:
            image_size = (int(self.image_size[0]*xscale),
                          int(self.image_size[1]*yscale))

        return KCamera.from_params(
            self.matrix[0, 0]*xscale, self.matrix[1, 1]*yscale,
            (self.matrix[0, 2]*xscale, self.matrix[1, 2]*yscale),
            self.undist_coeff,
            image_size)

    def clone(self):
        """
        Create a copy of this instance.

        Returns:
            Copy of this instance.
        """
        return KCamera(self.matrix.clone(),
                       copy.deepcopy(self.undist_coeff),
                       self.image_size)

    def get_projection_params(self, near: float, far: float) -> tenviz.Projection:
        """
        Converts this camera intrinsic to its OpenGL projection parameters.

        Args:
            near: Near clipling plane distance.
            far: Far cliping plane distance.

        Returns:
            The projection frustum parameters.
        """

        return tenviz.Projection.from_intrinsics(
            self.matrix, near, far)

    def get_opengl_projection_matrix(self, near: float, far: float,
                                     dtype: torch.dtype = torch.float) -> torch.Tensor:
        """Converts this camera intrinsic to its OpenGL matrix version.

        Args:

            near: Near clipling plane distance.
            far: Far cliping plane distance.
            dtype (optional): Specifies the returned matrix dtype.

        Returns:
            A (4 x 4) OpenGL projection matrix.
        """
        return torch.from_numpy(
            self.get_projection_params(near, far).to_matrix()).to(dtype)

    @property
    def image_width(self) -> int:
        """
        Returns:
            image width in pixels.

        """
        return self.image_size[0]

    @property
    def image_height(self) -> int:
        """
        Returns:
            image height in pixels.

        """
        return self.image_size[1]

    @property
    def pixel_center(self) -> Tuple[float, float]:
        """The center pixel coordinates.

        Returns:
            Its X and Y coordinates.
        """
        return (self.matrix[0, 2].item(), self.matrix[1, 2].item())

    @property
    def focal_length(self) -> float:
        """Returns the mean focal length from X and Y axis of the camera.
        """
        return (self.matrix[0, 0].item() + self.matrix[1, 1].item()) * 0.5

    def __str__(self):
        return (
            "Intrinsic matrix: {self.matrix}"
            ", radial distortion coefficients: {self.undist_coeff}"
            ", image size: {self.image_size}").format(self=self)

    def __repr__(self):
        return str(vars(self))

    def __eq__(self, other):
        if not isinstance(other, KCamera):
            return False

        return (torch.all(self.matrix == other.matrix)
                and (self.undist_coeff == other.undist_coeff)
                and (self.image_size == other.image_size))


class Project(torch.autograd.Function):
    """
    Differentiable projection operator for pinhole camera model
    """
    # pylint: disable=abstract-method, arguments-differ
    @staticmethod
    def forward(ctx, points: torch.Tensor, intrinsics: torch.Tensor) -> torch.Tensor:
        """

        Args:
            ctx: torch's context
            points: Array (N x 3) of 3D points in camera space
            intrinsics: Matrix (3 x 3) of camera intrinsics.

        Returns:
            Array (N x 2) of 2D points in image space.

        """
        ctx.save_for_backward(points, intrinsics)
        return _ProjectOp.forward(points, intrinsics)

    @staticmethod
    def backward(ctx, dy_grad: torch.Tensor) -> Tuple[torch.Tensor, None]:
        """
        Backward implementation.

        Args:
            dy_grad:
        """
        points, intrinsics = ctx.saved_tensors
        return _ProjectOp.backward(dy_grad, points, intrinsics), None


def normal_transform_matrix(matrix: torch.Tensor) -> torch.Tensor:
    r"""Returns the transpose of the inverse of the given matrix. The
    resulting matrix will preserve normal vector orientation and size.

    Args:
        matrix: A (4 x 4) or (3 x 4) affine transformation matrix.

    Returns:
        Rotation only (3 x 3) matrix for .

    """
    return torch.inverse(matrix[:3, :3]).transpose(1, 0)


def ensure_4x4_matrix(matrix: torch.Tensor) -> torch.Tensor:
    """
    Returns a matrix or matrices ensuring that they are 4x4 dimensions
    even if they are 3x4.

    If the passed argument are already 4x4, then returns itself,
    otherwise returns a copy.

    Args:
        matrix: Matrix (4x4 or 3x4 tensor) or matrices (Nx4x4 or Nx3x4).

    Returns:
        Ensured matrix to have 4x4 dimension.
    """
    if matrix.ndim == 2:
        if matrix.size(0) == 4 and matrix.size(1) == 4:
            return matrix
        return torch.tensor(
            [[matrix[0, 0], matrix[0, 1], matrix[0, 2], matrix[0, 3]],
             [matrix[1, 0], matrix[1, 1], matrix[1, 2], matrix[1, 3]],
             [matrix[2, 0], matrix[2, 1], matrix[2, 2], matrix[2, 3]],
             [0, 0, 0, 1]], dtype=matrix.dtype, device=matrix.device)

    row_dim = matrix.size(1)
    col_dim = matrix.size(2)

    if row_dim == 4 and col_dim == 4:
        return matrix

    matrices = torch.eye(4, 4).repeat(matrix.size(0), 1, 1)

    for i in range(matrix.size(0)):
        matrices[i, :row_dim, :col_dim] = matrix[i, :, :]

    return matrices


class RigidTransform:
    """Helper object for multiplying (4 x 4) or (3 x 4) matrices and (N x
    3) points. Has the matmul operator for applying the transform.

    Example:

        >>> result = RigidTransform(torch.rand(4, 4)) @ torch.rand(100, 3)
        >>> result.size(0), result.size(1)
        100, 3

    Attributes:
        matrix (torch.Tensor): The rigid transform matrix in (4 x 4)
         or (3 x 4) shapes.

    """

    def __init__(self, matrix):
        self.matrix = matrix
        self._normal_matrix = None

    @classmethod
    def from_random(cls, max_rot: float, max_translation: float,
                    dtype: torch.dtype = torch.float32):
        """Create a random rigid transformation.

        Args:
            max_rot: Max random rotation in radians.
            max_translation: Max translation distance in any axis.
            dtype: Type of its inner matrix.

        Returns:
            (:obj:`RigidTransform`): A random (and valid) rigid transformation.
        """
        transform = torch.eye(4, dtype=dtype)
        rand_quat = quaternion.from_euler_angles(
            random.uniform(-1, 1)*max_rot,
            random.uniform(-1, 1)*max_rot,
            random.uniform(-1, 1)*max_rot)
        transform[:3, :3] = torch.from_numpy(
            quaternion.as_rotation_matrix(rand_quat)).double()

        transform[0, 3] = max_translation*random.uniform(-1, 1)
        transform[1, 3] = max_translation*random.uniform(-1, 1)
        transform[2, 3] = max_translation*random.uniform(-1, 1)

        return cls(transform)

    @classmethod
    def from_axis_angle(cls, axis, position: Iterable = None, dtype: torch.dtype = torch.float32):
        """
        Creates a transformation from a rotation axis-angle

        Args:

            axis: A three dimensional axis angle rotation vector with
             its direction being the axis to rotatate, and its magnitude
             the rotation angle in radians.

            position: The three dimensional translation, default is 0self.

            dtype: The type of the torch's inner matrix.
        """
        rot = torch.from_numpy(_R.from_rotvec(ensure_numpy(axis)).as_matrix())
        if position is None:
            position = [0, 0, 0]
        return cls.from_matrix(rot, position, dtype=dtype)

    @classmethod
    def from_euler_angles(cls, rotations, position: Iterable = None,
                          dtype: torch.dtype = torch.float32):
        """
        Creates a transformation from rotation axi

        Args:

            rotations: A Three dimensional vector with the radians rotation angle around
             the x-axis, y-axis and z-axis.
            position: The three dimensional translation, default is 0.
            dtype: The type of the torch's inner matrix.
        """

        rot = torch.from_numpy(_R.from_euler(
            'xyz', ensure_numpy(rotations), degrees=False).as_matrix())
        if position is None:
            position = (0, 0, 0)
        return cls.from_matrix(rot, position, dtype=dtype)

    @classmethod
    def from_matrix(cls, rotation_matrix: torch.Tensor, position: Iterable = None,
                    dtype: torch.dtype = torch.float32):
        """Construct from a position vector and a rotation matrix.

        Args:

            rotation_matrix (:obj:`torch.Tensor`): Rotation matrix (3 x 3).
            position ((float, float, float)): Translation part.
            dtype: The type of the torch's inner matrix.
        """
        if position is None:
            position = (0, 0, 0)

        posx, posy, posz = position
        matrix = torch.tensor([[1.0, 0.0, 0.0, posx],
                               [0.0, 1.0, 0.0, posy],
                               [0.0, 0.0, 1.0, posz],
                               [0.0, 0.0, 0.0, 1.0]], dtype=dtype)
        matrix[:3, :3] = rotation_matrix
        return cls(matrix)

    @classmethod
    def from_quaternion(cls, qw: float, qx: float, qy: float, qz: float,
                        x: float, y: float, z: float):
        """
        Constructs from position and quaternion.

        """
        rot_mtx = torch.from_numpy(quaternion.as_rotation_matrix(
            np.quaternion(qw, qx, qy, qz))).double()

        cam_mtx = np.eye(4, dtype=torch.double)
        cam_mtx[0:3, 0:3] = rot_mtx
        cam_mtx[0:3, 3] = [x, y, z]

        return cls(cam_mtx)

    @classmethod
    def from_translation(cls, position, dtype: torch.dtype = torch.float32):
        """
        Creates a translation only rigid transformation

        Args:
            position: Any 3 value subscriptable object.
            dtype: The type of the torch's inner matrix.
        """
        posx, posy, posz = position
        return cls(torch.tensor(
            [[1.0, 0.0, 0.0, posx],
             [0.0, 1.0, 0.0, posy],
             [0.0, 0.0, 1.0, posz],
             [0.0, 0.0, 0.0, 1.0]], dtype=dtype))

    @classmethod
    def fit_points(cls, source_points: torch.Tensor, target_points: torch.Tensor):
        """Builds the rigid transformation that align the corresponding source
        points into the target points.

        The algorithm used using the SVD method described in: Eggert,
        David W., Adele Lorusso, and Robert B. Fisher. "Estimating 3-D
        rigid body transformations: a comparison of four major
        algorithms." Machine vision and applications 9, no. 5-6
        (1997): 272-290.

        Args:

            source_points: Source points, [Nx3] vector.
            target_points: Target points, [Nx3]
             vector. Each index must be correspoding pair to the
             `source_points`.

        """

        source_mean = source_points.mean(0)
        target_mean = target_points.mean(0)

        source_points = source_points - source_mean
        target_points = target_points - target_mean

        # pylint: disable=invalid-name
        cov_matrix = (source_points.unsqueeze(
            2) @ target_points.unsqueeze(1)).sum(0)

        cov_matrix = np.array(np.asfortranarray(cov_matrix.float().numpy()))
        source_mean = np.array(source_mean.float().view(3, 1).numpy())
        target_mean = np.array(target_mean.float().view(3, 1).numpy())

        matrix = _RigidTransformOp.find_rotation_translation(
            cov_matrix, source_mean, target_mean)

        return cls(torch.from_numpy(matrix))

    def __matmul__(self, points: torch.Tensor) -> torch.Tensor:
        points = self.matrix[:3, :3].matmul(points.view(-1, 3, 1))
        points += self.matrix[:3, 3].view(3, 1)

        return points.squeeze()

    def transform_normals(self, normals: torch.Tensor) -> torch.Tensor:
        """Transform a normal vector array by the instance's matrix.

        Args:
            normals: A (Nx3) normal vectors array.

        Returns:
            Transformed normal vectors (Nx3).
        """
        return (self.normal_matrix @ normals.view(-1, 3, 1)).view(-1, 3)

    def inplace(self, points: torch.Tensor) -> torch.Tensor:
        """Transform the points in-place.

        Args:
            points: Input points array [Nx3].
        Returns:
            The passed instance after the multiplication.
        """
        points = points.view(-1, 3)
        _RigidTransformOp.transform_inplace(self.matrix, points)
        return points

    @property
    def normal_matrix(self) -> torch.Tensor:
        """The transpose of the inverse of the input matrix for
        correct transforming normal vectors.

        Returns:
            A (3 x 3) matrix.

        """
        if self._normal_matrix is None:
            self._normal_matrix = normal_transform_matrix(self.matrix)

        return self._normal_matrix

    def inplace_normals(self, normals: torch.Tensor) -> torch.Tensor:
        """Transform an array of normal vectors in-place.

        Args:
            normals (torch.Tensor): Input normals array (Nx3).
        Returns:
            The input vector itself.
        """
        normals = normals.view(-1, 3)
        _RigidTransformOp.transform_normals_inplace(self.matrix, normals)
        return normals

    def rodrigues(self) -> torch.Tensor:
        """Computes the Rodrigues' rotation representation of its matrix rotation part.

        Returns:
            A 3 sized tensor containing the Rodrigues' rotational representation.

        """
        rodrigues = torch.empty(3, dtype=self.matrix.dtype)
        _RigidTransformOp.rodrigues(self.matrix.cpu(), rodrigues)
        return rodrigues

    @property
    def translation(self) -> torch.Tensor:
        """Translation matrix part.

        Returns:
            A 3 sized tensor containing the X, Y and Z translation.

        """
        return self.matrix[:3, 3]

    def relative_to(self, rigid_transform):
        """
        Computes the relative transform from this matrix to other (absolute) transformation.

        Args:
            rigid_transform: The reference transformation.
        """
        return RigidTransform(rigid_transform.matrix.inverse() @ self.matrix)

    def clone(self):
        """
        Returns:
            A copy of the instance.
        """
        return RigidTransform(self.matrix.clone())


class RTCamera:
    """Extrinsic camera wrapper.

    Attributes:

        matrix (:obj:`torch.Tensor`): A (4 x 4) matrix that transforms from camera space into world
         space. Type is double precision floating point (torch.double).

    """

    def __init__(self, matrix: torch.Tensor = None):
        if matrix is not None:
            self.matrix = ensure_torch(matrix, dtype=torch.double)
        else:
            self.matrix = torch.eye(4, dtype=torch.double)

    @classmethod
    def from_rigid_transform(cls, rigid_transform: RigidTransform):
        """Construct using a matrix from rigid transformation.

        Note: the matrix is not copied, we pass by reference.
        """
        return cls(rigid_transform)

    @classmethod
    def from_json(cls, json: Dict):
        """
        Constructs from FTB's JSON representation

        Args:
            json: JSON dictionary.

        Returns:

        """
        return cls(torch.tensor(json['matrix'], dtype=torch.double).view(-1, 4))

    def to_json(self) -> Dict:
        """
        Converts to FTB's JSON representation. See `slamtb.data.load_ftb`.


        Returns:
            A JSON dict.

        """
        return {'matrix': self.matrix.tolist()}

    @property
    def camera_to_world(self) -> torch.Tensor:
        """Matrix with camera to world transformation

        Returns:
            A [4x4] float64 matrix.
        """
        return self.matrix

    @property
    def world_to_camera(self) -> torch.Tensor:
        """Matrix with world to camera transformation.

        Returns:
            A [4x4] float64 matrix.
        """
        return torch.inverse(self.matrix)

    def relative_to(self, other_cam):
        """
        Computes the relative transform from this camera to other (absolute) camera.

        Args:
            other_cam: The reference camera.
        """
        return RTCamera(other_cam.matrix.inverse() @ self.matrix)

    @property
    def opengl_view_matrix(self) -> torch.Tensor:
        """
        Get a OpenGL ready view matrix like this instance.

        Returns:
            A [4x4] float32 matrix.

        """
        return _GL_HAND_MTX @ self.world_to_camera.float()

    def left_matmul(self, matrix: Any):
        """
        Multiply with this order: matrix @ self.matrix

        Args:
            matrix: Tensor matrix, RTCamera or RigidTransform instances.

        Returns:
            New transformed camera.
        """

        if isinstance(matrix, (RTCamera, RigidTransform)):
            matrix = matrix.matrix

        return RTCamera(matrix.cpu().double() @ self.matrix)

    def __matmul__(self, matrix: Any):
        """
        Transforms the camera by other matrix.

        Args:
            matrix: Tensor matrix, RTCamera or RigidTransform instances.

        Returns:
            New transformed camera.
        """

        if isinstance(matrix, (RTCamera, RigidTransform)):
            matrix = matrix.matrix

        return RTCamera(self.matrix @ matrix.cpu().double())

    @property
    def center(self) -> Tuple[float, float, float]:
        """
        The camera's center 3D point.

        Returns:
            X, Y and Z camera position.
        """
        return (self.matrix[0, 3].item(), self.matrix[1, 3].item(), self.matrix[2, 3].item())

    def clone(self):
        """
        Clone the instance.

        Returns:
            Instance's copy.
        """
        return RTCamera(self.matrix.clone())

    def __str__(self):
        return str(self.matrix)

    def __repr__(self):
        return str(self)
