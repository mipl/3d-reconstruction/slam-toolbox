#!/usr/bin/env python

"""Interactive testing of ICP algorithms based on Gauss-Newton.
"""

from pathlib import Path
import math

import numpy as np
import fire

from slamtb.data import set_start_at_eye
from slamtb.processing import ColorSpace
from slamtb.registration import MultiscaleRegistration
from slamtb.registration.icp import ICP
from slamtb.testing import (load_sample1_dataset,
                            load_sample2_dataset)

from slamtb.registration._test._testing import (run_trajectory_test, run_pair_test,
                                                run_pcl_pair_test)


SYNTHETIC_FRAME_ARGS = dict(frame0_idx=0, frame1_idx=7, color_space=ColorSpace.RGB,
                            blur=False, filter_depth=False,
                            view_matrix=np.array(
                                [[-0.997461, 0, -0.0712193, 0.612169],
                                 [-0.0168819, 0.971499, 0.23644, -1.29119],
                                 [0.0691895, 0.237042, -0.969033, -0.336442],
                                 [0, 0, 0, 1]]))

REAL_FRAME_ARGS = dict(frame1_idx=14, color_space=ColorSpace.GRAY,
                       blur=True, filter_depth=True)


class _Tests:
    """Tests ICP class.
    """

    @staticmethod
    def depth_real():
        """Use only depth information of a real scene.
        """
        run_pair_test(
            ICP(15, geom_weight=1, feat_weight=0,
                        distance_threshold=10, normal_angle_thresh=2),
            set_start_at_eye(load_sample1_dataset()),
            **REAL_FRAME_ARGS)

    @staticmethod
    def depth_synthetic():
        """Use only depth information of a synthetic scene.
        """

        run_pair_test(
            ICP(15, geom_weight=1.0, feat_weight=0,
                        distance_threshold=0.1,
                        normal_angle_thresh=math.pi/4.0),
            load_sample2_dataset(),
            **SYNTHETIC_FRAME_ARGS)

    @staticmethod
    def rgb_real():
        """Use only RGB information of a real scene.
        """
        run_pair_test(
            ICP(30, geom_weight=0, feat_weight=1,
                        distance_threshold=1.0,
                        normal_angle_thresh=math.pi/2),
            load_sample1_dataset(),
            **REAL_FRAME_ARGS)

    @staticmethod
    def rgb_synthetic():
        """Use only RGB information of a synthetic scene.
        """
        run_pair_test(
            ICP(30, geom_weight=0, feat_weight=1,
                        distance_threshold=2,
                        normal_angle_thresh=math.pi/2,
                        feat_residual_thresh=0.25),
            load_sample2_dataset(),
            **SYNTHETIC_FRAME_ARGS)

    @staticmethod
    def rgbd_real():
        """Use RGB+depth information of a real scene.
        """
        run_pair_test(
            ICP(40, geom_weight=.5, feat_weight=.5),
            load_sample1_dataset(),
            **REAL_FRAME_ARGS)

    @staticmethod
    def rgbd_synthetic():
        """Use RGB+depth information of a synthetic scene.
        """
        run_pair_test(
            ICP(40, geom_weight=1, feat_weight=1),
            load_sample2_dataset(),
            **SYNTHETIC_FRAME_ARGS)

    @staticmethod
    def ms_depth_real():
        """Use multiscale depth information of a real scene.
        """
        run_pair_test(
            MultiscaleRegistration([
                (1.0, ICP(15, geom_weight=1, feat_weight=0)),
                (0.5, ICP(10, geom_weight=1, feat_weight=0)),
                (0.5, ICP(5, geom_weight=1, feat_weight=0))]),
            load_sample1_dataset(),
            **REAL_FRAME_ARGS)

    @staticmethod
    def ms_depth_synthetic():
        """Use multiscale depth information of a synthetic scene.
        """
        run_pair_test(
            MultiscaleRegistration([
                (1.0, ICP(15, geom_weight=1, feat_weight=0)),
                (0.5, ICP(20, geom_weight=1, feat_weight=0)),
                (0.5, ICP(20, geom_weight=1, feat_weight=0))]),
            load_sample2_dataset(),
            **SYNTHETIC_FRAME_ARGS)

    @staticmethod
    def ms_rgb_real():
        """Use multiscale RGB information of a real scene.
        """
        run_pair_test(
            MultiscaleRegistration([
                (1.0, ICP(20, geom_weight=0, feat_weight=1)),
                (0.5, ICP(10, geom_weight=0, feat_weight=1)),
                (0.5, ICP(5, geom_weight=0, feat_weight=1))]),
            load_sample1_dataset(),
            **REAL_FRAME_ARGS)

    @staticmethod
    def ms_rgb_synthetic():
        """Use multiscale RGB information of a synthetic scene.
        """
        run_pair_test(
            MultiscaleRegistration([
                (1.0, ICP(20, geom_weight=0, feat_weight=1)),
                (0.5, ICP(10, geom_weight=0, feat_weight=1)),
                (0.5, ICP(5, geom_weight=0, feat_weight=1))]),
            load_sample2_dataset(),
            **SYNTHETIC_FRAME_ARGS)

    @staticmethod
    def ms_rgbd_real():
        """Use multiscale RGB+depth information of a real scene.
        """
        run_pair_test(
            MultiscaleRegistration([
                (1.0, ICP(20, geom_weight=1, feat_weight=1)),
                (0.5, ICP(20, geom_weight=1, feat_weight=1)),
                (0.5, ICP(30, geom_weight=1, feat_weight=1))
            ]),
            load_sample1_dataset(),
            **REAL_FRAME_ARGS, profile_file="ms-rgbd-icp.prof")

    @staticmethod
    def ms_rgbd_synthetic():
        """Use multiscale RGB+depth information of a synthetic scene.
        """
        geom_weight = 10
        feat_weight = 0
        run_pair_test(
            MultiscaleRegistration([
                (1.0, ICP(10, geom_weight=geom_weight,
                                  feat_weight=feat_weight)),
                (0.5, ICP(20, geom_weight=geom_weight,
                                  feat_weight=feat_weight)),
                (0.5, ICP(20, geom_weight=geom_weight,
                                  feat_weight=feat_weight)),
                (0.5, ICP(30, geom_weight=geom_weight,
                                  feat_weight=feat_weight))]),
            load_sample2_dataset(),
            **SYNTHETIC_FRAME_ARGS)

    @staticmethod
    def fail():
        """Test for fail alignment.
        """
        run_pair_test(ICP(10), load_sample1_dataset())

    @staticmethod
    def so3():
        """Test rotation only alignment.
        """
        run_pair_test(
            ICP(20, geom_weight=10, feat_weight=1, use_so3=True),
            load_sample2_dataset(),
            **REAL_FRAME_ARGS)

    @staticmethod
    def trajectory():
        """Test mulstiscale RGB and depth alignment on a a synthetic trajectory.
        """

        icp = MultiscaleRegistration([
            (1.0, ICP(10, geom_weight=10, feat_weight=1)),
            (0.5, ICP(15, geom_weight=10, feat_weight=1)),
            (0.5, ICP(15, geom_weight=10, feat_weight=1))])
        run_trajectory_test(icp, load_sample1_dataset(),
                            filter_depth=REAL_FRAME_ARGS['filter_depth'],
                            blur=REAL_FRAME_ARGS['blur'],
                            color_space=REAL_FRAME_ARGS['color_space'],
                            profile_file="ms-rgbd-icp-trajectory.cprof")

    @staticmethod
    def pcl_depth_synthetic():
        """Test pcl"""
        icp = ICP(15, geom_weight=1.0, feat_weight=0.0,
                  distance_threshold=0.1,
                  normal_angle_thresh=math.pi/4.0)

        run_pcl_pair_test(
            icp, load_sample2_dataset(),
            profile_file=(Path(__file__).parent / "pcl_rgbd_real.prof"),
            **SYNTHETIC_FRAME_ARGS)

    @staticmethod
    def pcl_rgb_synthetic():
        """Test pcl"""
        icp = ICP(200, geom_weight=0.0, feat_weight=1.0,
                  distance_threshold=0.1,
                  feat_residual_thresh=200.5)

        run_pcl_pair_test(
            icp, load_sample2_dataset(),
            profile_file=(Path(__file__).parent / "pcl_rgbd_real.prof"),
            **SYNTHETIC_FRAME_ARGS
        )

    @staticmethod
    def pcl_rgb_real():
        """Test pcl"""
        icp = ICP(30, geom_weight=0.0, feat_weight=1.0,
                  distance_threshold=0.1,
                  normal_angle_thresh=math.pi/2,
                  feat_residual_thresh=1.0)

        icp = MultiscaleRegistration([
            (0.01, ICP(30, geom_weight=0.0, feat_weight=1.0,
                       distance_threshold=0.1,
                       normal_angle_thresh=math.pi/2,
                       feat_residual_thresh=1.0)),
            (0.02, ICP(30, geom_weight=0.0, feat_weight=1.0,
                       distance_threshold=0.1,
                       normal_angle_thresh=math.pi/2,
                       feat_residual_thresh=1.0)),
            (0.03, ICP(30, geom_weight=0.0, feat_weight=1.0,
                       distance_threshold=0.1,
                       normal_angle_thresh=math.pi/2,
                       feat_residual_thresh=1.0))
        ])
        run_pcl_pair_test(
            icp, load_sample1_dataset(),
            profile_file=(Path(__file__).parent / "pcl_rgbd_real.prof"),
            filter_depth=True, blur=True,
            color_space=ColorSpace.GRAY,
            frame0_idx=0,
            frame1_idx=8)


if __name__ == '__main__':
    fire.Fire(_Tests)
