"""Testing procedures for registration methods.
"""
from pathlib import Path
import math

import torch
from tqdm import tqdm

from slamtb.metrics import (relative_rotational_error,
                            relative_translational_error,
                            translational_distance,
                            rotational_distance)
from slamtb.framepointcloud import FramePointCloud
from slamtb.surfel import SurfelCloud
from slamtb.camera import RTCamera
from slamtb.registration import RegistrationVerifier
from slamtb.trajectory import Odometry
from slamtb.viz import geoshow
from slamtb.testing import preprocess_frame, ColorSpace, get_device
from slamtb._utils import profile as _profile


def run_pair_test(icp, dataset, profile_file=None, filter_depth=True, blur=True,
                  color_space=ColorSpace.LAB, frame0_idx=0, frame1_idx=8,
                  view_matrix=None):
    """Test with two frames.
    """
    device = get_device()
    print("Using device:", device)
    frame_args = {
        'filter_depth': filter_depth,
        'blur': blur,
        'color_space': color_space
    }

    target_frame, target_feats = preprocess_frame(
        dataset[frame0_idx], **frame_args)
    source_frame, source_feats = preprocess_frame(
        dataset[frame1_idx], **frame_args)

    target_fpcl = FramePointCloud.from_frame(target_frame).to(device)
    source_fpcl = FramePointCloud.from_frame(source_frame).to(device)

    verifier = RegistrationVerifier(match_ratio_threshold=None)
    with _profile(Path(__file__).parent / str(profile_file),
                  profile_file is not None):
        for _ in range(1 if profile_file is None else 10):
            result = icp.estimate_frame(source_frame, target_frame,
                                        source_feats=source_feats.to(
                                            device),
                                        target_feats=target_feats.to(
                                            device),
                                        device=device)
        relative_rt = result.transform.cpu().float()

    if not verifier(result):
        print("!!!Registration failed")

    gt_transform = (target_frame.info.rt_cam.matrix.inverse()
                    @ source_frame.info.rt_cam.matrix)
    print("Translational error: ",
          translational_distance(gt_transform.inverse().double() @ relative_rt.double()))
    print("Rotational error (degrees): ", math.degrees(
        rotational_distance(gt_transform.inverse().double() @ relative_rt.double())))

    target_pcl = target_fpcl.unordered_point_cloud(world_space=False)
    source_pcl = source_fpcl.unordered_point_cloud(world_space=False)
    aligned_pcl = source_pcl.transform(relative_rt.to(device).float())

    print("Key 1 - toggle target PCL")
    print("Key 2 - toggle source PCL")
    print("Key 3 - toggle aligned source PCL")

    geoshow([target_pcl, source_pcl, aligned_pcl],
            title=icp.__class__.__name__, invert_y=True,
            view_matrix=view_matrix)


def run_pcl_pair_test(registration, dataset, profile_file=None, filter_depth=True, blur=True,
                      color_space=ColorSpace.LAB, frame0_idx=0, frame1_idx=8,
                      view_matrix=None):
    """Run testing alignment using the `estimate_pcl` method.
    """

    device = get_device()
    frame_args = {
        'filter_depth': filter_depth,
        'blur': blur,
        'color_space': color_space,
        'compute_normals': True
    }

    target_frame, target_features = preprocess_frame(
        dataset[frame0_idx], **frame_args)
    source_frame, source_features = preprocess_frame(
        dataset[frame1_idx], **frame_args)

    target_frame.depth_image[300, 300] = 0
    target_frame.depth_image[300, 301] = 0

    target_pcl = SurfelCloud.from_frame(
        target_frame, dense_features=target_features).to(device)
    source_pcl = SurfelCloud.from_frame(
        source_frame, dense_features=source_features).to(device)

    with _profile(profile_file):
        result = registration.estimate_pcl(source_pcl, target_pcl)

    if not RegistrationVerifier()(result):
        print("!!!Registration failed")

    gt_transform = (target_frame.info.rt_cam.matrix.inverse()
                    @ source_frame.info.rt_cam.matrix)

    print("Translational error: ", translational_distance(
        gt_transform.inverse() @ result.transform))
    print("Rotational error (degrees): ", math.degrees(
        rotational_distance(gt_transform.inverse() @ result.transform)))

    print("Key 1 - toggle target PCL")
    print("Key 2 - toggle source PCL")
    print("Key 3 - toggle aligned source PCL")

    aligned_pcl = source_pcl.transform(result.transform)
    geoshow([target_pcl.as_point_cloud(),
             source_pcl.as_point_cloud(),
             aligned_pcl.as_point_cloud()], invert_y=True,
            title=registration.__class__.__name__,
            view_matrix=view_matrix)


def run_trajectory_test(icp, dataset, filter_depth=True, blur=True,
                        color_space=ColorSpace.LAB, profile_file=None):
    """Trajectory test."""

    device = get_device()

    verifier = RegistrationVerifier()

    preprocess_args = {
        'filter_depth': filter_depth,
        'blur': blur,
        'color_space': color_space,
        'max_depth': 3
    }
    prev_frame, prev_features = preprocess_frame(
        dataset[0], **preprocess_args)

    pcls = [FramePointCloud.from_frame(
        prev_frame).unordered_point_cloud(world_space=False)]

    pred_traj = {0: RTCamera}
    gt_traj = {0: prev_frame.info.rt_cam}

    odometry = Odometry()
    with _profile(Path(__file__).parent / str(profile_file),
                  profile_file is not None):
        for i in tqdm(range(1, len(dataset))):
            next_frame, next_features = preprocess_frame(
                dataset[i], **preprocess_args)

            result = icp.estimate_frame(next_frame, prev_frame,
                                        source_feats=next_features.to(
                                            device),
                                        target_feats=prev_features.to(
                                            device),
                                        device=device)
            if not verifier(result):
                print(f"{i} tracking fail")
            odometry.update(result.transform)

            pred_traj[i] = odometry.to_camera()
            gt_traj[i] = next_frame.info.rt_cam
            torch.cuda.synchronize()
            pcl = FramePointCloud.from_frame(
                next_frame).unordered_point_cloud(world_space=False)
            pcl = pcl.transform(odometry.to_camera().matrix.float())
            pcls.append(pcl)
            torch.cuda.synchronize()
            prev_frame, prev_features = next_frame, next_features

    print("Translational error: ", relative_translational_error(
        gt_traj, pred_traj).mean().item())
    print("Rotational error: ", math.degrees(relative_rotational_error(
        gt_traj, pred_traj).mean().item()))

    geoshow(pcls, title=icp.__class__.__name__, invert_y=True)
