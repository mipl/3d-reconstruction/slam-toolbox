#!/usr/bin/env python

"""Testing of the sparse registration method.
"""
import math


from fire import Fire

from slamtb.registration.sparse import (SparseFeatureRegistration,
                                        Open3DFastGlobalRegistration)
from slamtb.registration import RegistrationVerifier
from slamtb.metrics import (translational_distance,
                            rotational_distance)
from slamtb.viz import geoshow
from slamtb.testing import (load_sample1_dataset as load_sample_dataset)
from slamtb.pointcloud import PointCloud
from slamtb.feature import SIFT, IntFeatureDict


def _test_frame_registration_impl(registration):
    dataset = load_sample_dataset()
    target_frame = dataset[0]
    source_frame = dataset[15]

    sift = SIFT()
    source_features = sift.extract_features(source_frame.rgb_image)
    target_features = sift.extract_features(target_frame.rgb_image)

    result = registration.estimate_frame(
        source_frame, target_frame, source_features, target_features)
    relative_rt = result.transform.cpu()

    verifier = RegistrationVerifier()
    if not verifier(result):
        print("Tracking failed")

    if target_frame.info.rt_cam is not None:
        gt_transform = target_frame.info.rt_cam.matrix.inverse(
        ) @ source_frame.info.rt_cam.matrix

        print("Translational error: ", translational_distance(
            gt_transform.double() @ relative_rt.inverse()))
        print("Rotational error: ", math.degrees(
            rotational_distance(gt_transform.double() @ relative_rt.inverse())))

    target_pcl = PointCloud.from_frame(target_frame, world_space=False)[0]
    source_pcl = PointCloud.from_frame(source_frame, world_space=False)[0]
    aligned_pcl = source_pcl.transform(relative_rt.float())

    print("Key 1 - toggle target PCL")
    print("Key 2 - toggle source PCL")
    print("Key 3 - toggle aligned source PCL")

    geoshow([target_pcl, source_pcl, aligned_pcl],
            title=registration.__class__.__name__, invert_y=True)


def _test_pcl_registration_impl(registration):
    dataset = load_sample_dataset()
    target_frame = dataset[0]
    source_frame = dataset[15]

    target_pcl, target_mask = PointCloud.from_frame(
        target_frame, world_space=False)
    source_pcl, source_mask = PointCloud.from_frame(
        source_frame, world_space=False)

    sift = SIFT()
    target_feats = IntFeatureDict.from_keypoint_features(
        sift.extract_features(target_frame.rgb_image), target_mask)
    source_feats = IntFeatureDict.from_keypoint_features(
        sift.extract_features(source_frame.rgb_image), source_mask)

    registration = Open3DFastGlobalRegistration(
        0.05)  # SparseFeatureRegistration()
    result, _ = registration.estimate_pcl(source_pcl.featured_select(source_feats),
                                          target_pcl.featured_select(target_feats))
    relative_rt = result.transform.cpu().float()

    verifier = RegistrationVerifier()
    if not verifier(result):
        print("Tracking failed")

    if target_frame.info.rt_cam is not None:
        gt_transform = target_frame.info.rt_cam.matrix.inverse(
        ) @ source_frame.info.rt_cam.matrix

        print("Translational error: ", translational_distance(
            gt_transform.double() @ relative_rt.inverse().double()))
        print("Rotational error: ", math.degrees(
            rotational_distance(gt_transform @ relative_rt.inverse().double())))

    aligned_pcl = source_pcl.transform(relative_rt.float())

    print("Key 1 - toggle target PCL")
    print("Key 2 - toggle source PCL")
    print("Key 3 - toggle aligned source PCL")

    geoshow([target_pcl, source_pcl, aligned_pcl],
            title=registration.__class__.__name__, invert_y=True,
            view_matrix=None)


if __name__ == '__main__':
    Fire({
        "frame-ransac": lambda: _test_frame_registration_impl(
            SparseFeatureRegistration()),
        "pcl-ransac": lambda: _test_pcl_registration_impl(
            SparseFeatureRegistration()),
        "frame-fast-global": lambda: _test_frame_registration_impl(
            Open3DFastGlobalRegistration(0.5)),
        "pcl-fast-global": lambda: _test_pcl_registration_impl(
            Open3DFastGlobalRegistration(0.5))
    })
