"""Pose estimation via iterative closest points algorithm.
"""
import math
from typing import Optional, Tuple, Union

import torch
from tenviz.pose import SE3, SO3

from slamtb.frame import Frame
from slamtb.framepointcloud import FramePointCloud
from slamtb.pointcloud import PointCloud
from slamtb.spatial.matching import MatchByProjection, MatchKNNNormals
from slamtb.spatial.kdtree import KDTree
from slamtb.camera import KCamera, RigidTransform
from slamtb._utils import empty_ensured_size, zero_ensured_size
from slamtb._cslamtb import (ICPJacobian as _ICPJacobian,
                             ICPOdometryJacobian as _ICPOdometryJacobian,
                             FeatureMap)
from slamtb.surfel import SurfelCloud

from .result import RegistrationResult, compute_information_matrix


# pylint: disable=invalid-name

class _BaseJacobianStep:
    def __init__(self, use_so3):
        self.JtJ = None
        self.Jtr = None
        self.residual = None
        self.use_so3 = use_so3
        self.correspondences = None

    def allocate_jacobian_tensors(self, correspondence_size: int, dtype: torch.dtype,
                                  device: torch.device):
        """
        Allocates tensors the JtJ, Jtr and residuals for holding a given number of correspondences.

        If the number of correspondences is lesser than the current ones, then previous data
        will be reused.

        Args:
            correspondence_size: Number of correspondences.
        """
        num_params = 6 if not self.use_so3 else 3
        self.JtJ = zero_ensured_size(
            self.JtJ, correspondence_size, num_params, num_params,
            device=device, dtype=dtype)
        self.Jtr = zero_ensured_size(
            self.Jtr, correspondence_size, num_params,
            device=device, dtype=dtype)
        self.residual = empty_ensured_size(
            self.residual, correspondence_size,
            device=device, dtype=dtype)

    def compute_geometric_term(self, target_points: torch.Tensor, target_normals: torch.Tensor,
                               source_points: torch.Tensor, transform: torch.Tensor) -> Tuple[
            torch.Tensor, torch.Tensor, float]:
        """
        Computes the ICP geometric term.

        Args:
            target_points: A [Mx3] tensor.
            target_normals: A [Mx3] tensor.
            source_points: A [Nx3] tensor.
            transform: A [4x4] tensor.

        Returns:
            The JtJ [6x6] tensor, Jtr [6x1] tensor, residual [1] for the Gauss-Newton optimization.
        """
        dtype = source_points.dtype
        _ICPJacobian.compute_geometric_term(
            self.correspondences,
            source_points,
            transform.to(dtype),
            target_points.view(-1, 3), target_normals.view(-1, 3),
            self.JtJ, self.Jtr, self.residual)

        Jtr = self.Jtr.sum(0)
        JtJ = self.JtJ.sum(0)
        residual = self.residual.sum().cpu().item()

        return JtJ, Jtr, residual


class _ICPOdometryJacobianStep(_BaseJacobianStep):
    """
    Jacobian computations of feature-term for the odometry case.
    """

    def __init__(self, target_points: torch.Tensor, target_normals: torch.Tensor,
                 feature_map: FeatureMap, target_mask: torch.Tensor, kcam: KCamera,
                 distance_threshold: float, normal_angle_thresh: float,
                 feat_residual_thresh: float, use_so3: bool):
        super().__init__(use_so3)

        dtype = target_points.dtype
        device = target_points.device
        self.target_points = target_points
        self.target_normals = target_normals
        self.feature_map = feature_map
        self.target_mask = target_mask

        self.kcam = kcam.matrix.to(dtype).to(device)

        self.matcher = MatchByProjection(
            distance_threshold, normal_angle_thresh)
        self.feat_residual_thresh = feat_residual_thresh

    def update_correspondences(self, source_points: torch.Tensor, source_normals: torch.Tensor,
                               transform: torch.Tensor) -> int:
        """
        Recomputes the correspondences using matching by projection.

        Returns:
            The number of correspondences.
        """
        self.correspondences = self.matcher.match(
            source_points, source_normals, transform,
            self.target_points, self.target_normals, self.target_mask,
            self.kcam)
        self.allocate_jacobian_tensors(self.correspondences.size(0),
                                       source_points.dtype,
                                       source_points.device)
        return self.correspondences.size(0)

    def compute_feature(self, source_points: torch.Tensor, source_feats: torch.Tensor,
                        transform: torch.Tensor) -> Tuple[
            torch.Tensor, torch.Tensor, float]:
        """
        Computes the feature term for the JtJ, Jtr and residual in the Gauss-Newton optimization.

        Returns:
            The JtJ [6x6] tensor, Jtr [6x1] tensor, residual [1] for the Gauss-Newton optimization.
        """
        dtype = source_points.dtype

        if not self.use_so3:
            _ICPOdometryJacobian.compute_feature_term(
                self.correspondences, source_points, source_feats,
                transform.to(dtype), self.feature_map, self.kcam,
                self.feat_residual_thresh,
                self.JtJ, self.Jtr, self.residual)
        else:
            _ICPOdometryJacobian.compute_feature_term_so3(
                self.correspondences, source_points, source_feats,
                transform.to(dtype), self.feature_map, self.kcam,
                self.feat_residual_thresh,
                self.JtJ, self.Jtr, self.residual)

        Jtr = self.Jtr.sum(0)
        JtJ = self.JtJ.sum(0)
        residual = self.residual.sum().cpu().item()

        return JtJ, Jtr, residual


class _ICPJacobianStep(_BaseJacobianStep):
    """
    Jacobian computations for the feature-term.
    """

    def __init__(self, target_points: torch.Tensor, target_normals: torch.Tensor,
                 target_feats: torch.Tensor, k: int, distance_threshold: float,
                 normal_angle_thresh: float, feat_residual_thresh: float,
                 estimate_delta: float, use_so3: bool):
        super().__init__(use_so3)

        self.knn = KDTree(target_points)
        self.target_normals = target_normals
        self.target_feats = target_feats

        self.k = k
        self.distance_threshold = distance_threshold
        self.normal_angle_thresh = normal_angle_thresh
        self.feat_residual_thresh = feat_residual_thresh
        self.estimate_delta = estimate_delta

        self.correspondences = None
        self.query = None

    def update_correspondences(self, source_points, source_normals, transform):
        """
        Recomputes the correspondences using KDTree matching.

        Returns:
            The number of correspondences.
        """
        dtype = source_points.dtype
        device = source_points.device
        self.query = self.knn.query(
            RigidTransform(transform.to(dtype).to(device)) @ source_points,
            k=self.k,
            distance_upper_bound=self.distance_threshold)

        self.correspondences = MatchKNNNormals(self.normal_angle_thresh).match(
            self.query.distances, self.query.index, source_normals,
            self.target_normals, transform)
        self.allocate_jacobian_tensors(self.correspondences.size(0),
                                       dtype, device)
        return self.correspondences.size(0)

    def compute_feature(self, source_points: torch.Tensor, source_feats: torch.Tensor,
                        transform: torch.Tensor) -> Tuple[
            torch.Tensor, torch.Tensor, float]:
        """
        Computes the feature term for the JtJ, Jtr and residual in the Gauss-Newton optimization.

        Returns:
            The JtJ [6x6] tensor, Jtr [6x1] tensor, residual [1] for the Gauss-Newton optimization.
        """
        dtype = source_points.dtype

        interp_target_features = self.query.interpolate_features(
            self.target_feats)
        dxyz_features = self.query.dxyz_features(self.target_feats)

        _ICPJacobian.compute_feature_term(
            self.correspondences, source_points, source_feats,
            transform.to(dtype), interp_target_features, dxyz_features,
            self.feat_residual_thresh, self.estimate_delta,
            self.JtJ, self.Jtr, self.residual)

        Jtr = self.Jtr.sum(0)
        JtJ = self.JtJ.sum(0)
        residual = self.residual.sum().cpu().item()

        return JtJ, Jtr, residual


class ICP:
    """Iterative closest points algorithm using the point-to-plane error,
    and Gauss-Newton optimization procedure.

    Attributes:

        num_iters (int): Number of iterations for the Gauss-Newton
         optimization algorithm.

        geom_weight (float): Geometry term weighting, 0.0 to disable
         use of depth data.

        feat_weight (float): Feature term weighting, 0.0 to ignore
         point features.

        so3 (bool): SO3 optimization, i.e., rotation only.

        distance_threshold (float): Maximum distance to match a pair
         of source and target points.

        normal_angle_thresh (float): Maximum angle in radians between
         normals to match a pair of source and target points.

        feat_residual_thresh (float): Maximum residual between features.

        verbose: Whether to print verbose message.
    """

    def __init__(self, num_iters: int, geom_weight: float = 1.0, feat_weight: float = 1.0,
                 use_so3=False,
                 distance_threshold: float = .5, normal_angle_thresh: float = math.pi/10,
                 feat_residual_thresh: float = 2.75, verbose: bool = False):
        self.num_iters = num_iters
        self.geom_weight = geom_weight
        self.feat_weight = feat_weight
        self.use_so3 = use_so3
        self.distance_threshold = distance_threshold
        self.normal_angle_thresh = normal_angle_thresh
        self.feat_residual_thresh = feat_residual_thresh
        self.verbose = verbose

    def _estimate(self, source_points: torch.Tensor, source_normals: torch.Tensor,
                  jacobian_step: _BaseJacobianStep,
                  source_feats: torch.Tensor = None, target_points: torch.Tensor = None,
                  target_normals: torch.Tensor = None,
                  transform: torch.Tensor = None):

        transform = (torch.eye(4, dtype=torch.double) if transform is None
                     else transform.cpu().double())

        has_geom = self.geom_weight > 0.0

        has_features = not None in (
            source_feats, ) and self.feat_weight > 0.0
        best_result = RegistrationResult()

        for iteration in range(self.num_iters):
            JtJ = torch.zeros(6, 6, dtype=torch.double)
            Jr = torch.zeros(6, dtype=torch.double)
            residual = 0

            match_count = jacobian_step.update_correspondences(
                source_points, source_normals, transform.float())

            feat_residual = 0
            if has_features:
                feat_JtJ, feat_Jr, feat_residual = jacobian_step.compute_feature(
                    source_points, source_feats, transform)
                JtJ = feat_JtJ.cpu().double()*self.feat_weight*self.feat_weight
                Jr = feat_Jr.cpu().double()*self.feat_weight
                residual = feat_residual*self.feat_weight

            if has_geom:
                geom_JtJ, geom_Jr, geom_residual = jacobian_step.compute_geometric(
                    target_points, target_normals,
                    source_points, transform)

                JtJ += geom_JtJ.cpu().double()*self.geom_weight*self.geom_weight
                Jr += geom_Jr.cpu().double()*self.geom_weight
                residual += geom_residual*self.geom_weight

            #JtJ = JtJ + (count / self.num_iters)*torch.diag(torch.diag(JtJ))
            try:
                upper_JtJ = torch.cholesky(JtJ)
                Jr = Jr.cpu().view(-1, 1).double()
                update = torch.cholesky_solve(
                    Jr, upper_JtJ).squeeze()
            except:
                break

            if not self.use_so3:
                update_matrix = SE3.exp(
                    update).to_matrix()
                transform = update_matrix @ transform
            else:
                update_matrix = SO3.exp(
                    update).to_matrix()
                transform = update_matrix @ transform

            residual = residual / match_count
            if self.verbose:
                print(
                    f"ICP {iteration} feat.-residual {feat_residual/match_count} "
                    f"residual {residual} {match_count}")

            if residual < best_result.residual:
                best_result = RegistrationResult(
                    transform.cpu(), JtJ, residual, match_count / source_points.size(0))

        best_result.information = compute_information_matrix(
            jacobian_step.correspondences, target_points)

        return best_result

    def estimate_frame(self, source_frame: Union[Frame, FramePointCloud],
                       target_frame: Union[Frame, FramePointCloud],
                       source_feats: Optional[torch.Tensor] = None,
                       target_feats: Optional[torch.Tensor] = None,
                       transform: torch.Tensor = None,
                       device: torch.device = "cpu") -> RegistrationResult:
        """Estimates the odometry registration between two frames.

        Args:
            source_frame: Source frame.
            target_frame: Target frame.
            source_feats: Source feature map [C x H x W].
            target_feats: Target feature map [C x H x W].
            transform: Initial [4x4] rigid transformation matrix.
            device: Torch device to execute the algorithm.

        Returns:
            Resulting transformation.
        """

        if isinstance(source_frame, Frame):
            source_frame = FramePointCloud.from_frame(source_frame).to(device)

        if isinstance(target_frame, Frame):
            target_frame = FramePointCloud.from_frame(target_frame).to(device)

        has_feats = (source_feats is not None
                     and target_feats is not None
                     and self.feat_weight > 0.0)

        feature_map = None
        if has_feats:
            feature_map = FeatureMap(target_feats)

        jacobian_step = _ICPOdometryJacobianStep(
            target_frame.points, target_frame.normals,
            feature_map, target_frame.mask,
            target_frame.kcam,
            self.distance_threshold, self.normal_angle_thresh,
            self.feat_residual_thresh, self.use_so3)

        source_mask = source_frame.mask.view(-1)
        source_points = source_frame.points.view(-1, 3)[source_mask, :]
        source_normals = source_frame.normals.view(-1, 3)[source_mask, :]

        if has_feats:
            source_feats = source_feats.view(
                source_feats.size(0), -1)[:, source_mask]

        try:
            result = self._estimate(
                source_points,
                source_normals,
                jacobian_step,
                source_feats=source_feats,
                target_points=target_frame.points,
                target_normals=target_frame.normals,
                transform=transform)
        finally:
            if has_feats:
                feature_map.release()

        return result

    def estimate_pcl(self, source_pcl: Union[PointCloud, SurfelCloud],
                     target_pcl: Union[PointCloud, SurfelCloud], transform: torch.Tensor = None,
                     k_neighbors: int = 5):
        """Estimates the odometry registration between two frames.

        Args:
            source_pcl: Source point cloud.
            target_pcl: Target point cloud.
            transform: Initial [4x4] rigid transformation matrix.
            k_neighbors: The max number of interpolating neighbors for the feature term.

        Returns:
            Resulting transformation.
        """

        jacobian_step = _ICPJacobianStep(
            target_pcl.points,
            target_pcl.normals,
            target_pcl.features,
            k_neighbors,
            self.distance_threshold,
            self.normal_angle_thresh,
            self.feat_residual_thresh,
            0.05,
            self.use_so3)

        return self._estimate(source_pcl.points,
                             source_pcl.normals,
                             jacobian_step,
                             source_feats=source_pcl.features,
                             target_points=target_pcl.points,
                             target_normals=target_pcl.normals,
                             transform=transform)
