"""
Registration tools with keypoint features.
"""
from typing import Optional, Union, Tuple

from skimage.measure import ransac
import cv2
import torch

from slamtb.frame import Frame
from slamtb.framepointcloud import FramePointCloud
from slamtb.camera import KCamera, RigidTransform
from slamtb.feature import KeypointFeatures, match_features
from slamtb.pointcloud import PointCloud
from slamtb.surfel import SurfelCloud

from .result import RegistrationResult, compute_information_matrix


def estimate_essential_matrix(keypoints1: torch.Tensor, keypoints2: torch.Tensor, kcam: KCamera,
                              matchings: torch.Tensor) -> Tuple[torch.Tensor, torch.Tensor]:
    """
    Convenience wrapper around OpenCV cv2.findEssentialMat.

    Args:
        keypoints1: A [Nx2] tensor of 2D coordinates.
        keypoints2: Another [Nx2] tensor of 2D coordinates.
        kcam: Camera intrinsic.
        matchings: A [Mx2] int32 tensor matching indices. Columns 1 and 2 corresponds to,
         respectively, `keypoints1` and `keypoints2`.

    Returns:
        A [4x4] rigid transformation matrix and a mask of size [N] of inliers correspondences.
    """

    keypoints1 = keypoints1[matchings[:, 0], :]
    keypoints2 = keypoints2[matchings[:, 1], :]

    keypoints1 = keypoints1[:, [1, 0]]
    keypoints2 = keypoints2[:, [1, 0]]

    matrix, mask = cv2.findEssentialMat(
        keypoints1.cpu().numpy(), keypoints2.cpu().numpy(),
        pp=kcam.pixel_center,
        focal=kcam.focal_length,
        method=cv2.RANSAC,
        prob=0.99,
        threshold=1.0)

    if mask is None:
        return None, None

    return torch.from_numpy(matrix), torch.from_numpy(mask).bool().squeeze()


class _FitRigidTransform:
    """
    Interface for Scikit-image RANSAC.
    """

    def __init__(self, params=None):
        self.params = params

    def estimate(self, source_points, target_points):
        """Estimate method implementation required by scikit-image's ransac.
        """

        self.params = RigidTransform.fit_points(source_points, target_points)
        det = torch.det(self.params.matrix[:3, :3]).item()
        return (det != 0 and self.params.matrix[0, 0].item() > 0
                and self.params.matrix[1, 1].item() > 0
                and self.params.matrix[2, 2].item() > 0)

    def residuals(self, source_points, target_points):
        """Returns the per element residuals required by scikit-image's ransac.
        """

        tpoints = source_points.clone()
        self.params.inplace(tpoints)

        return (target_points - tpoints).norm(2, dim=1).numpy()


class SparseFeatureRegistration:
    """
    Registration pipeline for based on point correspondences and RANSAC for filtering outlier.
    """

    def __init__(self, distance_mode=cv2.NORM_L2,
                 residual_threshold: float = 0.005, max_trials: int = 1000,
                 max_feat_distance: float = float('inf'),
                 max_spatial_distance: Optional[float] = None):
        self.distance_mode = distance_mode
        self.residual_threshold = residual_threshold
        self.max_trials = max_trials
        self.max_feat_distance = max_feat_distance
        self.max_spatial_distance = max_spatial_distance

        self._matchings = None

    def estimate(self, source_points: torch.Tensor, target_points: torch.Tensor,
                 matchings: torch.Tensor) -> Tuple[RegistrationResult, torch.Tensor]:
        """
        Estimate the rigid transformation from 3D points and established matchings.

        Args:
            source_points: Source points, [Nx3] tensor of 3D points.
            target_points: Target points, [Nx3] tensor of 3D points.
            matchings: A [Mx2] int32 tensor matching indices. Columns 1 and 2 corresponds to,
             respectively, `keypoints1` and `keypoints2`.
        Returns:
            The registration result and a [M] tensor mask of inlier points
        """
        match_source_points = source_points[matchings[:, 0], :].cpu()
        match_target_points = target_points[matchings[:, 1], :].cpu()

        best_model, inliers = ransac(
            (match_source_points, match_target_points),
            _FitRigidTransform,
            min_samples=5, residual_threshold=self.residual_threshold,
            max_trials=self.max_trials)

        self._matchings = matchings[inliers, :]
        if best_model is None:
            return RegistrationResult(torch.eye(4, dtype=torch.float64),
                                      information=torch.eye(6, dtype=torch.float64)), None
        transform = best_model.params.matrix

        information = compute_information_matrix(
            self._matchings, target_points)

        inlier_rmse = (best_model
                       .residuals(match_source_points[inliers, :], match_target_points[inliers, :])
                       .mean())

        return RegistrationResult(transform.double(), information,
                                  match_ratio=inliers.sum() / inliers.size,
                                  inlier_rmse=inlier_rmse,
                                  source_size=source_points.size(0),
                                  target_size=target_points.size(0)), inliers

    def estimate_frame(self, source_frame: Union[Frame, FramePointCloud],
                       target_frame: Union[Frame, FramePointCloud],
                       source_feats2d: KeypointFeatures,
                       target_feats2d: KeypointFeatures) -> RegistrationResult:
        """
        Estimate the rigid transformation from two frames.

        Args:
            source_frame: Source frame.
            target_frame: Target frame.
            source_feats2d: Source 2D feature descriptors.
            target_feats2d: Target 2D feature descriptors.
        Returns:
            The registration result.
        """
        source_pcl, source_feats2d = PointCloud.from_keypoints_2d(
            source_frame, source_feats2d, with_normals=False)
        target_pcl, target_feats2d = PointCloud.from_keypoints_2d(
            target_frame, target_feats2d, with_normals=False)

        matchings = match_features(source_feats2d.descriptors, target_feats2d.descriptors,
                                   self.max_feat_distance, self.distance_mode)

        _, inliers = estimate_essential_matrix(source_feats2d.keypoints, target_feats2d.keypoints,
                                               source_frame.info.kcam, matchings)

        if inliers is None:
            return RegistrationResult()  # Error state

        return self.estimate(source_pcl.points, target_pcl.points, matchings[inliers, :])[0]

    def estimate_pcl(self, source_pcl: Union[PointCloud, SurfelCloud],
                     target_pcl: Union[PointCloud, SurfelCloud]) -> RegistrationResult:
        """
        Estimate the registration from two point clouds using the matching of their features.
        """
        matchings = match_features(source_pcl.features, target_pcl.features,
                                   self.max_feat_distance, self.distance_mode)

        if self.max_spatial_distance is not None:
            spatial_distances = (source_pcl.points[matchings[:, 0], :] -
                                 target_pcl.points[matchings[:, 1], :]).norm(2, dim=1)
            matchings = matchings[spatial_distances <
                                  self.max_spatial_distance, :]

        return self.estimate(source_pcl.points, target_pcl.points, matchings)


class Open3DFastGlobalRegistration:
    """
    Wrapper around Open3D's registration_fast_based_on_feature_matching.
    """

    def __init__(self, max_feat_distance):
        """
        Max L2 distance for correspondence of two feature descriptors.
        """
        self.max_feat_distance = max_feat_distance

    def estimate_frame(self, source_frame: Union[Frame, FramePointCloud],
                       target_frame: Union[Frame, FramePointCloud],
                       source_feats2d: KeypointFeatures,
                       target_feats2d: KeypointFeatures) -> RegistrationResult:
        """
        Estimate the rigid transformation from two frames.

        Args:
            source_frame: Source frame.
            target_frame: Target frame.
            source_feats2d: Source 2D feature descriptors.
            target_feats2d: Target 2D feature descriptors.
        Returns:
            The registration result.
        """

        source_pcl, source_feats2d = PointCloud.from_keypoints_2d(
            source_frame, source_feats2d)
        target_pcl, target_feats2d = PointCloud.from_keypoints_2d(
            target_frame, target_feats2d, )

        return self.estimate_pcl(source_pcl, target_pcl)[0]

    def estimate_pcl(self, source_pcl: PointCloud,
                     target_pcl: PointCloud) -> Tuple[RegistrationResult, None]:
        """
        Estimate the registration from two point clouds using the matching of their features.
        """
        import open3d  # pylint: disable=import-outside-toplevel
        source_pcl, source_feats = source_pcl.to_open3d()
        target_pcl, target_feats = target_pcl.to_open3d()

        o3d_result = open3d.pipelines.registration.registration_fast_based_on_feature_matching(
            source_pcl, target_pcl, source_feats, target_feats,
            open3d.pipelines.registration.FastGlobalRegistrationOption(self.max_feat_distance))

        result = RegistrationResult(
            torch.from_numpy(o3d_result.transformation.copy()),
            None,
            residual=0.0, match_ratio=o3d_result.fitness,
            inlier_rmse=o3d_result.inlier_rmse)

        return result, None
