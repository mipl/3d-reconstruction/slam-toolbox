"""
Function for running registration algorithm at different scales.
"""


from typing import List, Optional, Tuple, Union

import torch

from slamtb.frame import Frame
from slamtb.framepointcloud import FramePointCloud
from slamtb.pointcloud import PointCloud
from slamtb.processing import feature_pyramid, DownsampleXYZMethod
from slamtb.registration.result import RegistrationResult
from slamtb.surfel import SurfelCloud


class MultiscaleRegistration:
    """
    Wrappers multiple registration estimator at different scales.
    """

    def __init__(self, estimators: List[Tuple[float, object]],
                 downsample_xyz_method=DownsampleXYZMethod.Nearest,
                 verbose: bool = False):
        """
        Setups the scale pyramid.

        Args:
            estimators: Each element contains a scale value and its registration estimator.
             The pairs should be specified with their scales from higher to lower.
            downsample_xyz_method: Which method to interpolate the XYZ points and normals.

        """
        self.estimators = estimators
        self.downsample_xyz_method = downsample_xyz_method
        self.verbose = verbose

    def estimate_frame(self, source_frame: Union[Frame, FramePointCloud],
                       target_frame: Union[Frame, FramePointCloud],
                       source_feats: torch.Tensor = None,
                       target_feats: torch.Tensor = None, transform: Optional[torch.Tensor] = None,
                       device: torch.device = torch.device("cpu")) -> RegistrationResult:
        """
        Runs multiple scale estimation from two frames.

        Args:
            source_frame: Source frame.
            target_frame: Target frame.
            source_feats: A [CxHxW] featuremap tensor.
            target_feats: A [CxHxW] featuremap tensor.
            transform: A [4x4] matrix with the initial transformation if any.
            device: Target device to run the estimators.

        Returns:
            The final registration result.
        """
        if isinstance(source_frame, Frame):
            source_frame = FramePointCloud.from_frame(source_frame).to(device)

        if isinstance(target_frame, Frame):
            target_frame = FramePointCloud.from_frame(target_frame).to(device)

        scales = [scale for scale, _ in self.estimators]
        source_feat_pyr = target_feat_pyr = [None]*len(scales)

        source_pyr = source_frame.pyramid(
            scales, downsample_xyz_method=self.downsample_xyz_method, colored=False)
        if source_feats is not None:
            source_feat_pyr = feature_pyramid(source_feats, scales)

        target_pyr = target_frame.pyramid(
            scales,
            downsample_xyz_method=self.downsample_xyz_method, colored=False)
        if target_feats is not None:
            target_feat_pyr = feature_pyramid(target_feats, scales)

        for ((_, estimator), source, src_feats,
             target, tgt_feats) in zip(self.estimators[::-1],
                                       source_pyr, source_feat_pyr,
                                       target_pyr, target_feat_pyr):
            curr_result = estimator.estimate_frame(
                source,
                target,
                source_feats=src_feats,
                target_feats=tgt_feats,
                transform=transform,
                device=device)

            if curr_result:
                transform = curr_result.transform

        return curr_result

    def estimate_pcl(self, source_pcl: Union[PointCloud, SurfelCloud],
                     target_pcl: Union[PointCloud, SurfelCloud],
                     transform: Optional[torch.Tensor] = None,
                     device: torch.device = torch.device("cpu")) -> RegistrationResult:
        """
        Runs multiple scale estimation for two point cloud like structures.

        Args:
            source_frame: Source point cloud.
            target_frame: Target point cloud.
            transform: A [4x4] matrix with the initial transformation if any.
            device: Target device to run the estimators.

        Returns:
            The final registration result.
        """

        if device is None:
            device = source_pcl.device
        if transform is not None:
            transform = transform.cpu()

        src = source_pcl.to("cpu")
        tgt = target_pcl.to("cpu")

        pcls = []
        for scale, _ in self.estimators:
            if scale > 0:
                src = src.downsample(scale)
                tgt = tgt.downsample(scale)

            if self.verbose:
                print(
                    f"MultiscaleRegistration: scale {scale} source size {src.size} "
                    f"target size {tgt.size}")
            pcls.append((src, tgt))

        for (_, estimator), (src, tgt) in zip(self.estimators[::-1], pcls[::-1]):
            result = estimator.estimate_pcl(
                src.to(device), tgt.to(device), transform=transform)
            transform = result.transform
        return result

    def __str__(self):
        return f"MultiscaleRegistration({self.estimators})"
