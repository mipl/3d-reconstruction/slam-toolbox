"""
A Frame point cloud is a point cloud shaped like a 2D grid.
"""

from typing import List, Union
import torch
import numpy as np

from .processing import (
    downsample_xyz, downsample_mask, DownsampleXYZMethod, erode_mask)
from ._cslamtb import (Processing as _Processing, EstimateNormalsMethod)
from ._utils import ensure_torch, depth_image_to_uvz
from .pointcloud import PointCloud
from .camera import KCamera, RTCamera
from .frame import Frame


class FramePointCloud:
    """A point cloud still embedded on its frame. This representation is
    useful for retrieving point cloud data by pixel coordinates.

    Attributes:

        image_points: Image of U, V and depth values (H
         x W x 3). Float 32 type.

        mask: Valid mask (bool) of uv coordinates (H x W).

        kcam: Camera intrinsic parameters.

        rt_cam: Camera extrinsic parameters.

        points: Image of 3D points (H x W x 3) located on the camera space. Float32 type.

        colors: Image of point colors (H x W x 3). Uint8 type.

        normals: Image of normal vectors (H x W x 3). Float32 type.

    """

    def __init__(self, image_points: torch.Tensor, mask: torch.Tensor, kcam: KCamera,
                 rt_cam=RTCamera, points: torch.Tensor = None, normals: torch.Tensor = None,
                 colors: torch.Tensor = None):
        self.image_points = image_points
        self.mask = mask
        self.kcam = kcam
        self.rt_cam = rt_cam
        self._points = points
        self._normals = normals
        self.colors = colors

    @classmethod
    def from_frame(cls, frame: Frame, ignore_seg_background: bool = False):
        """Create the frame point cloud from a frame.

        Args:

            frame: Source frame.

            ignore_seg_background: If `True` and frame has segmentation, then it'll discard
             region that are background (seg_image == 0). Default is `False`.

        """

        depth_image = ensure_torch(frame.depth_image)

        image_points = depth_image_to_uvz(depth_image, frame.info.depth_scale,
                                          frame.info.depth_bias)

        mask = (depth_image > 0)
        mask = erode_mask(mask)

        if frame.seg_image is not None and ignore_seg_background:
            mask = torch.logical_and(
                torch.from_numpy(frame.seg_image > 0), mask)

        colors = None
        if frame.rgb_image is not None:
            colors = ensure_torch(frame.rgb_image)

        normals = None
        if frame.normal_image is not None:
            normals = ensure_torch(frame.normal_image)

        return FramePointCloud(image_points, mask, frame.info.kcam, frame.info.rt_cam,
                               normals=normals, colors=colors)

    @classmethod
    def from_depth(cls, depth_image: np.ndarray, kcam: KCamera, depth_scale: float,
                   rt_cam: RTCamera = None, normals: np.ndarray = None, colors: np.ndarray = None):
        """
        Creates a frame point cloud from a depth image.

        Args:
            depth_image: A uint16 or int32 depth image with tensor format [HxW].

            kcam: The intrinsic parameters of the sensor.

            depth_scale: The scaleing that convert the raw depth intengers into
             z coordinates.

            rt_cam: The extrinsic parameters of the sensor.

            normals: A [HxWx3] tensor of floats representing the normal vectors
             of each point.

            colors: A RGB image - a[HxWx3] tensor of uint8.
        """
        image_points = depth_image_to_uvz(depth_image, depth_scale, 0.0)
        mask = depth_image > 0

        return cls(image_points, mask, kcam, rt_cam=rt_cam,
                   normals=ensure_torch(normals),
                   colors=ensure_torch(colors))

    @property
    def points(self) -> torch.Tensor:
        """Image of 3D points on the camera space.

        Returns:
            (H x W x 3) 3D points.
        """

        if self._points is None:
            if self.kcam is None:
                raise RuntimeError("Frame doesn't have intrinsics camera")

            self._points = self.kcam.backproject(
                self.image_points.reshape(-1, 3)).reshape(self.image_points.shape)
        return self._points

    @property
    def normals(self) -> torch.Tensor:
        """Image of 3D normal vectors. It'll compute normals by the central
        differences method if not computed before.

        Returns:
            (H x W x 3) per point normal vectors.
        """

        if self._normals is None:
            self.estimate_normals()

        return self._normals

    @normals.setter
    def normals(self, normals: torch.Tensor):
        """Overwrites the normals vectors.

        Args:
            normals: A normal vector image (H x W x 3).

        """

        self._normals = normals

    def estimate_normals(self, method=EstimateNormalsMethod.CentralDifferences):
        """
        Estimate the normal vectors of each 3D point. After the call the `self.normals`
        properties will be set.

        Args:
            method: Normal estimation method.
        """
        self._normals = torch.empty(self.points.size(0), self.points.size(1), 3,
                                    dtype=self.points.dtype, device=self.points.device)

        _Processing.estimate_normals(
            self.points, self.mask, self._normals,
            method)

    def unordered_point_cloud(self, world_space: bool = True,
                              compute_normals: bool = True) -> PointCloud:
        """Converts into a sparse point cloud.

        Args:

            world_space (bool, optional): If `True`, then the 3d point
             are transformed into world space. Default is `True`.

            compute_normals (bool, optional): If `True` the normals
             are included on the returned point cloud.

        """

        mask = self.mask.flatten()
        if compute_normals:
            normals = self.normals.view(-1, 3)
            normals = normals[mask]
        else:
            normals = None

        pcl = PointCloud(self.points.view(-1, 3)[mask],
                         self.colors.view(-1, 3)[mask]
                         if self.colors is not None else None,
                         normals)

        if world_space and self.rt_cam is not None:
            pcl = pcl.transform(
                self.rt_cam.cam_to_world.to(self.device).float())

        return pcl

    def downsample(self, scale: float, downsample_xyz_method=DownsampleXYZMethod.Nearest,
                   colored: bool = True):
        """Downsample the point cloud.

        Args:
            scale: Scaling.

            downsample_xyz_method (DownsampleXYZMethod): Which sampling method for interpolating
             points and normals.

        Returns:
            Down-sampled frame point cloud.
        """

        points = downsample_xyz(self.points, self.mask, scale,
                                method=downsample_xyz_method)
        normals = None
        if self._normals is not None:
            normals = downsample_xyz(self._normals, self.mask, scale,
                                     method=downsample_xyz_method)

        mask = downsample_mask(self.mask, scale)

        colors = None
        if colored:
            colors = self.colors.permute(2, 0, 1).unsqueeze(0).float()
            colors = torch.nn.functional.interpolate(colors, scale_factor=scale, mode='bilinear',
                                 align_corners=False)
            colors = colors.squeeze(0).permute(1, 2, 0).byte()
        kcam = self.kcam.scaled(scale)

        return FramePointCloud(None, mask, kcam, rt_cam=self.rt_cam,
                               points=points, normals=normals,
                               colors=colors)

    def pyramid(self, scales: List[float], downsample_xyz_method=DownsampleXYZMethod.Nearest,
                colored: bool = False):
        """Create a multiple scale pyramid for this point cloud.

        Args:

            scales: Decreasing scale factors. No scaling is applied for values greater than one,
             repeating the current point cloud.

            downsample_xyz_method: Which sampling method for interpolating points and normals.

        Returns:
            (List[:obj:`FramePointCloud`]): Downsampled pyramid of frame point
             clouds, in increasing order of scales.

        """

        pyramid = []
        current = self
        for scale in scales:
            if scale < 1.0:
                current = current.downsample(
                    scale, downsample_xyz_method=downsample_xyz_method,
                    colored=colored)
            pyramid.append(current)

        pyramid.reverse()
        return pyramid

    def to(self, dst: Union[torch.dtype, torch.device, str]):
        """Change the point cloud device or dtype. Dtype are applied only to
        points and normals.

        Args:
            dst: Dtype or torch device.

        Returns:
            (:obj:`FramePointCloud`): Converted point cloud.

        """
        # pylint: disable=invalid-name

        if isinstance(dst, torch.dtype):
            return FramePointCloud(
                (self.image_points.to(dst) if self.image_points is not None else None),
                self.mask,
                self.kcam,
                self.rt_cam,
                self._points.to(dst) if self._points is not None else None,
                self._normals.to(dst) if self._normals is not None else None,
                self.colors if self.colors is not None else None)

        return FramePointCloud(
            (self.image_points.to(dst)
             if self.image_points is not None else None),
            self.mask.to(dst),
            self.kcam.clone(),
            self.rt_cam.clone() if self.rt_cam is not None else None,
            self._points.to(dst) if self._points is not None else None,
            self._normals.to(dst) if self._normals is not None else None,
            self.colors.to(dst) if self.colors is not None else None)

    def __getitem__(self, *slices):
        """
        Slices the frame point cloud's properties according to a selector.

        Args:
            slices: A selector compatible with a [HxW] shape.

        """
        slices = slices[0]
        return FramePointCloud(self.image_points[slices],
                               self.mask[slices], self.kcam, rt_cam=self.rt_cam,
                               points=self.points[slices],
                               normals=self.normals[slices] if self._normals is not None else None,
                               colors=self.colors[slices])

    @property
    def width(self) -> int:
        """
        The width of the frame.
        """
        return self.mask.size(1)

    @property
    def height(self) -> int:
        """
        The height height of the frame.
        """
        return self.mask.size(0)

    @property
    def device(self) -> torch.device:
        """
        Torch's device.
        """
        return self.mask.device

    def plot_debug(self, show=True):
        """
        Debug ploting using Matplotlib.
        """
        # pylint: disable=import-outside-toplevel
        import matplotlib.pyplot as plt

        plt.figure()
        plt.subplot(2, 3, 1)
        plt.title("X-values")
        plt.imshow(self.points[:, :, 0].cpu().numpy())

        plt.subplot(2, 3, 2)
        plt.title("Y-values")
        plt.imshow(self.points[:, :, 1].cpu().numpy())

        plt.subplot(2, 3, 3)
        plt.title("Z-values")
        plt.imshow(self.points[:, :, 2].cpu().numpy())

        plt.subplot(2, 3, 4)
        plt.title("Mask")
        plt.imshow(self.mask.cpu().numpy())

        plt.subplot(2, 3, 5)
        plt.title("Colors")
        plt.imshow(self.colors.cpu().numpy())

        plt.subplot(2, 3, 6)
        plt.title("Normals")
        plt.imshow(self.normals.cpu().numpy())

        if show:
            plt.show()
