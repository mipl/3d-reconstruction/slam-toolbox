"""
Miscellaneous functions.
"""

import torch
from slamtb._cslamtb import Util as _Util


def masked_yx_to_linear(yx_points: torch.Tensor, image_mask: torch.Tensor) -> torch.Tensor:
    """
    Converts image yx coordinates to linear index considering masked cells.

    Args:
        yx_points: Y and X image coordinates, shape and type should be [Nx2] and int64.
        image_mask: Image's mask with valid positions.

    Returns:
        Tensor of shape [N] with the corresponding linear indices for each yx,
         or -1 if the coordinates were masked out.
    """
    return _Util.masked_yx_to_linear(yx_points, image_mask)
