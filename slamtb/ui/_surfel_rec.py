"""
Viewer for surfel based mapping process.
"""
from enum import Enum

import torch
import cv2
import numpy as np

import tenviz
import tenviz.io
from slamtb.surfel import SurfelModel

from slamtb.viz.surfelrender import SurfelRender, RenderMode
from slamtb.spatial.trigoctree import TrigOctree


class RunMode(Enum):
    """
    Run modes for the UI. Use `PLAY` to continuously read frames,
    or `STEP` for waiting the use to press a key (it should be 'n')
    """
    PLAY = 0
    STEP = 1


class SurfelReconstructionUI:
    """
    Viewer for surfel based mapping process.

    Use it as an iterable (__iter__), advancing every frame for updating the Window.
    """

    def __init__(self, surfel_model: SurfelModel, run_mode=RunMode.PLAY,
                 inverse=False, gt_mesh=None,
                 stable_conf_thresh=10.0, time_thresh=20,
                 title="Surfel Reconstruction"):
        self.run_mode = run_mode
        self.surfel_render = SurfelRender(surfel_model)

        self.stable_conf_thresh = stable_conf_thresh
        self.time_thresh = time_thresh

        scene = [self.surfel_render]
        if gt_mesh is not None:
            with surfel_model.gl_context.current():
                self.gt_mesh_node = tenviz.nodes.create_mesh(
                    gt_mesh.verts, gt_mesh.faces, normals=gt_mesh.normals)
                scene.append(self.gt_mesh_node)

            self.oct_tree = TrigOctree(
                gt_mesh.verts, gt_mesh.faces.long(), 256)
        self.gt_mesh = gt_mesh

        self.viewer = surfel_model.gl_context.viewer(
            scene, cam_manip=tenviz.CameraManipulator.WASD)

        self.viewer.title = title
        self.viewer.reset_view()
        self.surfel_model = surfel_model

        if inverse:
            inv = torch.eye(4, dtype=torch.float32)
            inv[1, 1] = -1
            self.surfel_render.transform = inv

        self._quit_flag = False
        self._read_next_frame = True

        self._use_camera_view = False
        self.rt_camera = None

        self.frame_count = 0

    def next_frame(self):
        """
        Let the viewer loop exit.
        """
        self._read_next_frame = True

    def quit(self):
        """
        Quit from the viewer.
        """
        self._quit_flag = True

    def toggle_run_mode(self):
        """
        Toggle between continuously play mode or step mode.
        """
        if self.run_mode == RunMode.PLAY:
            self.run_mode = RunMode.STEP
            self._read_next_frame = False
            print("Run mode set to step")
        else:
            self.run_mode = RunMode.PLAY
            self._read_next_frame = True
            print("Run mode set to continuous")

    def set_render_mode(self, mode: RenderMode):
        """
        Change surfels rendering mode.
        """
        self.surfel_render.set_render_mode(mode)

    def show_stable_only(self):
        """
        Shows stable surfels only.
        """
        print("Show only stable surfels")
        self.surfel_render.set_stable_threshold(self.stable_conf_thresh)

    def show_unstable_too(self):
        """
        Hides stable surfels only.
        """
        print("Show unstable and stable")
        self.surfel_render.set_stable_threshold(-1)

    def toggle_camera_mode(self):
        """
        Toggle whether the user moves the camera with WASD,
        or places it with the current pose estimate.
        """
        if self._use_camera_view:
            print("Set to free view")
            self._use_camera_view = False
        else:
            print("Set to camera's view")
            self._use_camera_view = True

    def save_model(self, output_filename: str):
        """
        Save the current model into a file.

        Args:
            output_filename: The output 3D file.
        """
        cloud = self.surfel_model.to_surfel_cloud()[0]
        good_confs = cloud.confidences > self.stable_conf_thresh
        cloud = cloud[good_confs]
        tenviz.io.write_3dobject(
            output_filename, cloud.points.cpu().numpy(),
            normals=cloud.normals,
            colors=cloud.colors)

    def __iter__(self):
        """
        Use this class as an iterator to update the UI. Also, if the user selects step
        run mode, it will block the call until a new step is allowed.
        """
        self.frame_count = 0

        key_map = {'q': self.quit,
                   'n': self.next_frame,
                   'm': self.toggle_run_mode,
                   'r': lambda: self.set_render_mode(RenderMode.COLOR),
                   't': lambda: self.set_render_mode(RenderMode.NORMAL),
                   'y': lambda: self.set_render_mode(RenderMode.CONFIDENCE),
                   'u': lambda: self.set_render_mode(RenderMode.TIME),
                   'i': lambda: self.set_render_mode(RenderMode.ID),
                   'o': lambda: self.set_render_mode(RenderMode.GRAY),
                   'f': self.show_stable_only,
                   'g': self.show_unstable_too,
                   'h': self.toggle_camera_mode,
                   'v': lambda: self.save_model(f"model-{self.surfel_model.max_time}.ply")
                   }
        print("""Key controls:
        q: quit
        n: next frame
        m: toggle between step and continuous play mode
        r: render colors
        t: render normals
        y: render confidences
        u: render times
        i: render surfel ids
        o: render gray scale
        f: show stable only
        g: show unstable too
        h: toggle between camera modes
        j: evaluate accuracy
        """)

        while not self._quit_flag:
            if self._read_next_frame:
                try:
                    yield True
                except StopIteration:
                    break

                self._read_next_frame = self.run_mode != RunMode.STEP
                self.frame_count += 1

                with self.surfel_model.gl_context.current():
                    self.surfel_render.set_max_confidence(
                        # self.surfel_model.max_confidence
                        self.stable_conf_thresh
                    )
                    self.surfel_render.set_max_time(self.surfel_model.max_time)

                if self._use_camera_view and self.rt_camera is not None:
                    inv = np.eye(4, dtype=np.float32)
                    inv[1, 1] = -1
                    inv[0, 0] = -1
                    self.viewer.camera_matrix = self.rt_camera.opengl_view_cam.numpy() @ inv

            self.surfel_model.gl_context.clear_color = np.array(
                [0.32, 0.34, 0.87, 1])
            keys = [self.viewer.wait_key(0), cv2.waitKey(1)]

            if keys[0] < 0:
                self.quit()
                yield False

            for key in keys:
                if key & 0xff == 27:
                    self.quit()
                    yield False
                    break
                key = chr(key & 0xff).lower()

                if key in key_map:
                    key_map[key]()

        self.close()

    def close(self):
        """
        Closes the viewer.
        """
        self.viewer.release()
