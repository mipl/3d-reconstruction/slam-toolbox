"""
Common geometry types
"""

import torch

from ._cslamtb import AABB as _AABB


class AABB(_AABB):
    """
    Axis aligned bounding box class
    """
    @classmethod
    def from_points(cls, points: torch.Tensor):
        """
        Create an AABB from a set of points.

        Args:
            points: A [Nx3] tensor of points.
        """
        return cls(points.min(0)[0].cpu().numpy(),
                   points.max(0)[0].cpu().numpy())
