"""
Parser for the sequences from the Indoor Lidar RGB-D (IL-RGBD) dataset, available at:
http://redwood-data.org/indoor_lidar_rgbd/
"""

from pathlib import Path
from typing import List, Union, TextIO

import torch
import cv2
from natsort import natsorted
import numpy as np

from slamtb.frame import Frame, FrameInfo
from slamtb.camera import KCamera, RTCamera

ASUS_KCAM = KCamera(torch.tensor([[525, 0.0, 319.5],
                                  [0.0, 525, 239.5],
                                  [0.0, 0.0, 1.0]], dtype=torch.float))


class ILRGBDDataset:
    """
    Dataset loader for a sequence from the IL-RGBD dataset. Use `__getitem__` to read a frame.
    """

    def __init__(self, depth_images: List[Path], rgb_images: List[Path],
                 trajectory: List[RTCamera]):
        """
        Initializer.

        Args:
            depth_images: A list with the absolute paths of the depth images.
            rgb_images: A list with the absolute paths of the color images.
            trajectory: The ground truth trajectory.
        """
        self.rgb_images = rgb_images
        self.depth_images = depth_images
        self.trajectory = trajectory

    def get_info(self, idx: int) -> Frame:
        """
        Gets a frame information without loading the images.
        """
        rt_cam = self.trajectory[idx]

        return FrameInfo(ASUS_KCAM, 0.001, rt_cam=rt_cam, timestamp=idx)

    def __getitem__(self, idx: int) -> Frame:
        """
        Loads the idx-nth entry.
        """
        rgb_image = cv2.imread(str(self.rgb_images[idx]))
        rgb_image = cv2.cvtColor(rgb_image, cv2.COLOR_BGR2RGB)

        depth_image = cv2.imread(
            str(self.depth_images[idx]), cv2.IMREAD_ANYDEPTH).astype(np.int32)

        info = self.get_info(idx)

        return Frame(info, depth_image, rgb_image)

    def __len__(self):
        """
        Returns the length of the loaded sequence.
        """
        return min(len(self.rgb_images), len(self.depth_images))


def read_log_file_trajectory(stream: TextIO) -> List[RTCamera]:
    """
    Parses a .log file.

    Args:
        stream: Open stream.

    Returns:
        A camera trajectory.
    """
    trajectory = []
    while True:
        line = stream.readline()
        if line == "":
            break
        curr_entry = []
        for _ in range(4):
            line = stream.readline()
            curr_entry.append([float(elem) for elem in line.split()])
            # cam space to world space
        rt_mtx = torch.tensor(curr_entry, dtype=torch.float64)

        assert rt_mtx.shape == (4, 4)
        trajectory.append(RTCamera(rt_mtx))
    return trajectory


_INV_Y_MTX = torch.eye(4, dtype=torch.double)
_INV_Y_MTX[1, 1] = -1


def load_ilrgbd(base_dir: Union[str, Path],
                trajectory_filepath: Union[str, Path]) -> ILRGBDDataset:
    """
    Loads a sequence from the IL-RGBD dataset.
    """
    rgb_images = (Path(base_dir) / "image").glob("*.jpg")
    rgb_images = natsorted(rgb_images, key=str)

    depth_images = (Path(base_dir) / "depth").glob("*.png")
    depth_images = natsorted(depth_images, key=str)

    with open(str(trajectory_filepath), 'r', encoding='ascii') as stream:
        trajectory = read_log_file_trajectory(stream)

    for rt_cam in trajectory:
        rt_cam.matrix = _INV_Y_MTX @ rt_cam.matrix

    return ILRGBDDataset(depth_images, rgb_images, trajectory)
