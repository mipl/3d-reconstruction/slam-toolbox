"""
Module for parsing datasets.
"""
from ._util import set_start_at_eye, TransformCameraDataset
