"""
KLG file reader.
"""

from typing import BinaryIO, Iterable
import struct
import zlib

import numpy as np
import cv2
from tqdm import tqdm

from slamtb.camera import KCamera

from slamtb.frame import FrameInfo, Frame


def _read_frame_header(stream):
    timestamp = struct.unpack('q', stream.read(8))[0]
    depth_size = struct.unpack('i', stream.read(4))[0]
    rgb_size = struct.unpack('i', stream.read(4))[0]

    return timestamp, depth_size, rgb_size


def _read_frame_pointers(stream, num_frames):
    frame_ptrs = []
    for _ in range(num_frames):
        frame_ptrs.append(stream.tell())
        _, depth_size, rgb_size = _read_frame_header(stream)
        stream.seek(depth_size + rgb_size, 1)
    return frame_ptrs


class KLG:
    """
    Dataset for reading .klg files produced by ElasticFusion.
    """

    def __init__(self, filepath: str):
        """
        Opens the file for extracting frames.
        Args:

            filepath: KLG file path.
        """

        with open(filepath, 'rb') as stream:
            num_frames = struct.unpack('i', stream.read(4))[0]
            self.frame_ptrs = _read_frame_pointers(stream, num_frames)

        # pylint: disable=consider-using-with
        self.stream = open(filepath, 'rb')
        self.kcam = KCamera(np.eye(3))
        self.depth_scale = 1.0

    def __del__(self):
        self.stream.close()

    def __getitem__(self, idx):
        seek = self.frame_ptrs[idx]
        self.stream.seek(seek, 0)

        _, depth_size, rgb_size = _read_frame_header(self.stream)
        raw_depth = self.stream.read(depth_size)
        jpg_rgb = self.stream.read(rgb_size)

        rgb_img = cv2.imdecode(np.frombuffer(jpg_rgb, dtype=np.uint8), 1)

        depth_img = np.frombuffer(zlib.decompress(raw_depth), dtype=np.uint16)

        depth_img = depth_img.reshape(rgb_img.shape[0:2])

        rt_cam = None
        info = FrameInfo(kcam=self.kcam, rt_cam=rt_cam,
                         depth_scale=self.depth_scale)
        frame = Frame(info, depth_image=depth_img.astype(
            np.int32), rgb_image=rgb_img)
        return frame

    def __len__(self):
        return len(self.frame_ptrs)


class KLGWriter:
    """
    Writer of .klg files.
    """

    def __init__(self, stream, format_depth_scale: float = None):
        """
        Initializer.

        Args:
            stream: Opened file stream for writing binary data.
            format_depth_scale: The value to scale the depth values.
        """
        self.stream = stream
        self.format_depth_scale = format_depth_scale
        self.count = 0
        stream.write(struct.pack('i', 0))

    def write_frame(self, frame: Frame) -> int:
        """
        Writes a frame. Its depth scale will be changed according to `self.format_depth_Scale`

        Returns:
            The written time stamp, which is the same of the frame's info if its exists,
             otherwise, returns a sequential counter.
        """
        depth_image = frame.depth_image

        if self.format_depth_scale is not None:
            depth_image = depth_image*frame.info.depth_scale
            depth_image = depth_image*self.format_depth_scale

        depth_image = depth_image.astype(np.uint16)
        compress_depth = zlib.compress(depth_image)
        _, jpg_rgb = cv2.imencode('.jpg', frame.rgb_image)

        if frame.info.timestamp is not None:
            if isinstance(frame.info.timestamp, float):
                timestamp = int(frame.info.timestamp*10000000)
            else:
                timestamp = frame.info.timestamp
        else:
            timestamp = self.count + 1

        self.stream.write(struct.pack('q', timestamp))
        self.stream.write(struct.pack('i', len(compress_depth)))
        self.stream.write(struct.pack('i', len(jpg_rgb)))

        self.stream.write(compress_depth)
        self.stream.write(jpg_rgb)

        self.count += 1

        return timestamp

    def finish(self):
        """
        Flushes and closes the writing stream.
        """
        self.stream.seek(0, 0)
        self.stream.write(struct.pack('i', self.count))


def write_klg(dataset: Iterable[Frame], stream: BinaryIO, format_depth_scale: float = None,
              max_frames: int = None):
    """Write a dataset into the .klg file format.

    Args:
        dataset: Any iterable of frames.
        stream: Binary output stream open to write.
        format_depth_scale: Scaling for the depth values.
        max_frames: The maximum number of frames to write.
    """

    if max_frames is not None:
        max_frames = min(max_frames, len(dataset))
    else:
        max_frames = len(dataset)

    writer = KLGWriter(stream, format_depth_scale)

    rt_cams = []
    for i in tqdm(range(max_frames), desc="Writing KLG file"):
        frame = dataset[i]

        timestamp = writer.write_frame(frame)
        if frame.info.rt_cam is not None:
            rt_cam = frame.info.rt_cam
            rt_cams.append((timestamp, rt_cam))

    writer.finish()
