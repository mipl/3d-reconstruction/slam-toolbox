"""Utils to generate submap pose graphes.
"""

from typing import Any, Dict, Iterable, Tuple, Union
import torch

from slamtb.camera import RTCamera
from slamtb.trajectory import Trajectory

from .graph import (PoseGraphNode, PoseGraphEdge,
                    dump_pose_graph)


class PoseGraphBuilder:
    """
    In slam-toolbox, pose graph are dictionaries with the format Dict[int: PoseGraphNode].
    This class helps filling dictionary with the right edges and transformations.
    """

    def __init__(self, start_state: Union[Dict[int: PoseGraphNode], PoseGraphNode]):
        if isinstance(start_state, dict):
            self.pose_graph = start_state
        elif isinstance(start_state, PoseGraphNode):
            self.pose_graph = {start_state.i: start_state}
        else:
            raise RuntimeError(
                f"Unknown start state type {type(start_state)}")

    def add(self, i: int, j: int, transform: torch.Tensor,
            information: torch.Tensor, uncertain: bool = None):
        """
        Add a edge from two nodes. If `j` does not exists, then a new node is created based on
        the `i` pose and the transformation.

        Args:
            i: Source node.
            j: Target node.
            transform: Relative transformation from i to j.
            information: Information matrix [6x6] of doubles with the transformation's uncertainty.
            uncertain (optional): Whether the node should be considered uncertain.
             When not specified, it's is `True` for new node and `False` for already existing nodes.
        """
        if j not in self.pose_graph:
            if uncertain is None:
                uncertain = False

            pose = transform @ self.pose_graph[i].pose.inverse()

            self.pose_graph[j] = PoseGraphNode(j, pose.inverse())
        elif uncertain is None:
            uncertain = True

        self.pose_graph[i][j] = PoseGraphEdge(
            j, transform, information, uncertain=uncertain)


class SubmapPoseGraphBuilder:
    """Pose graph builder for generating pose graphs while submapping from odometry.
    Attributes:
        pose_graph: Pose graph dictionary.
    """

    def __init__(self, pose_graph: Dict[int, PoseGraphNode] = None, start_camera: RTCamera = None):
        self.pose_graph = (pose_graph if pose_graph is not None else {})

        self._current_camera = (
            start_camera if start_camera is not None else RTCamera())
        self._pose = self._current_camera
        self._current_information = None
        self._previous_node_id = None
        self._is_finished = False

    def append(self, curr_node_id: int, curr_node_geometry: Any, next_node_camera: RTCamera,
               next_node_information: torch.Tensor):
        """Appends a new node to the pose graph with pose according to the previous camera.

        Args:
            curr_node_id: The id of the current submap that was just finished its segment.
            curr_node_geometry: The submap geometry that was just finished.
            next_node_camera: The camera that is just starting a new submap.
            next_node_information: The information matrix, [6x6] of doubles, of camera.
        """

        if self._is_finished:
            raise RuntimeError(
                "Trying to append in already finish pose graph builder")

        self.pose_graph[curr_node_id] = PoseGraphNode(
            curr_node_id,
            self._current_camera.matrix,
            geometry=curr_node_geometry)

        if self._previous_node_id is not None:
            prev_node = self.pose_graph[self._previous_node_id]
            prev_node[curr_node_id] = PoseGraphEdge(
                curr_node_id,
                (prev_node.pose.inverse() @ self._current_camera.matrix).inverse(),
                information=self._current_information, uncertain=False)

        if next_node_camera is not None:
            self._current_camera = next_node_camera.clone()
            self._current_information = next_node_information
        self._previous_node_id = curr_node_id

    def finish(self, last_node_id, last_node_geom):
        """
        Appends the last node.
        """
        if last_node_id is not None:
            self.append(last_node_id, last_node_geom, None, None)

        return self.pose_graph

    def get_local_camera(self, global_camera: RTCamera) -> RTCamera:
        """
        Returns the camera that is relative to the current submap.

        Args:
            The current global odometry.
        """
        return global_camera.relative_to(self._current_camera)

    def __str__(self):
        return dump_pose_graph(self.pose_graph)

    def __repr__(self):
        return dump_pose_graph(self.pose_graph)

    def keys(self) -> Iterable[Any]:
        """
        Returns the key-ids of the pose graph nodes.
        """
        return self.pose_graph.keys()

    def values(self) -> Iterable[PoseGraphNode]:
        """
        Returns the pose graph nodes.
        """
        return self.pose_graph.values()

    def items(self) -> Iterable[Tuple[Any, PoseGraphNode]]:
        """
        Returns the pose graph key and nodes.
        """
        return self.pose_graph.items()

    def __iter__(self):
        return iter(self.pose_graph)


def pose_graph_from_trajectory(trajectory: Trajectory) -> Dict[float, PoseGraphNode]:
    """
    Creates a pose graph from a trajectory.
    Note this method does not find loop closures.
    """
    if len(trajectory) == 0:
        return {}

    start_item = trajectory[0]
    prev_time = start_item.time
    builder = PoseGraphBuilder(PoseGraphNode(prev_time,
                                             start_item.camera.matrix))

    information = torch.eye(6)*5000  # Give a very good information
    for time, transform in trajectory.relative_transformations():
        builder.add(prev_time, time, transform.matrix, information)
        prev_time = time

    return builder.pose_graph
