"""Graph for poses optimization using other libraries.
"""
import torch
import yaml
import numpy as np

from slamtb.camera import RTCamera


class PoseGraphEdge:
    """The edge on a pose graph.

    Attributes:

        j (int): Next node Id.

        transform (:obj:`torch.Tensor`): Transformation matrix.

        information: ?
    """

    def __init__(self, j, transform, information=None, uncertain=True):
        self.j = j
        self.transform = transform
        self.information = information
        self.uncertain = uncertain

    @classmethod
    def create_from_frames(cls, j, frame_info_i, frame_info_j):
        """Create an edge using the information from two frames.
        """
        transform = frame_info_i.rt_cam.matrix.inverse() @ frame_info_j.rt_cam.matrix
        transform = transform.inverse()
        return cls(j, transform)

    def clone(self):
        """Creates and return a copy.
        """
        return PoseGraphEdge(self.j, self.transform.clone(),
                             information=(
                                 self.information.clone()
                                 if self.information is not None else None),
                             uncertain=self.uncertain)

    def __str__(self):
        return f"{self.j} @ {self.transform}"

    def __repr__(self):
        return str(self)


class PoseGraphNode:
    """Pose graph node.

    Attributes:

        i (int): This node Id.

        pose_matrix (:obj:`torch.Tensor`): Node pose matrix

        edges (Dict[int, :obj:`PoseGraphEdge`]): Edges.

        max_depth (float, optional): Max z distance of camera view depth.

        geometry (:obj:`object`, optional): Any object to represent geometry or None
    """

    def __init__(self, i, pose, edges=None, max_depth=None, geometry=None):
        self.i = i
        self.pose = pose.double()
        self.edges = {} if edges is None else edges
        self.max_depth = max_depth
        self.geometry = geometry

    def clone(self, no_edges=True, shallow_geometry=True, no_geometry=False):
        """Clone the node.

        Args:

            no_edges (bool): Whatever to copy the edges too.

           shallow_geometry (bool): Whatever to just pass the geometry
            reference or call its .clone method.

        Returns: (:obj:`PoseGraphNode`):
            Copied PoseGraphNode.
        """

        edges = None
        if not no_edges:
            edges = {j: edge.clone()
                     for j, edge in self.edges.items()}

        geometry = None
        if not no_geometry:
            if shallow_geometry:
                geometry = self.geometry
            elif self.geometry is not None:
                geometry = self.geometry.clone()

        return PoseGraphNode(self.i, self.pose.clone(),
                             edges=edges, max_depth=self.max_depth,
                             geometry=geometry)

    def as_camera(self) -> RTCamera:
        """
        Returns the pose matrix as a camera.
        """
        return RTCamera(self.pose.inverse())

    def __str__(self):
        return str(f"Node(i={self.i}, edges={list(self.edges.keys())})")

    def __repr__(self):
        return str(self)

    def __getitem__(self, edge_key):
        return self.edges[edge_key]

    def __setitem__(self, edge_key, edge):
        self.edges[edge_key] = edge


def traverse_pose_graph(pose_graph, callback, nodes=None, ignore_no_edge=False):
    """Traverse a pose graph informing the absolute transformation for
    each node.

    Args:

        pose_graph (Dict[int: :obj:`PoseGraphNode`]): Pose graph.

        callback (Callable[:obj:`PoseGraphNode`, int, torch.Tensor]):
         A calable that accepts the pose graph, the node's integer id,
         and the absolute transformation matrix for that node.

        nodes (List[int], optional): List of node ids for traversal in
         the same order.
    """

    def traverse_all(pose_graph, callback):
        prev_i = None
        for i, node in pose_graph.items():
            callback(pose_graph, i, node.pose, prev_i)
            prev_i = i

    def traverse_nodes(pose_graph, nodes, callback, ignore_no_edge=ignore_no_edge):
        print(f"Showing traverse of {nodes} nodes")

        accum_transform = torch.eye(4, dtype=torch.double)
        callback(pose_graph, nodes[0], accum_transform, None)

        for cnt in range(len(nodes) - 1):
            i = nodes[cnt]
            j = nodes[cnt + 1]

            if j in pose_graph[i].edges:
                transform = pose_graph[i].edges[j].transform
            elif ignore_no_edge:
                transform = pose_graph[j].pose
            else:
                raise RuntimeError(
                    f"No edge between node {i} to {j}")

            accum_transform = transform.double() @ accum_transform
            callback(pose_graph, j, accum_transform, i)

    if nodes is None:
        traverse_all(pose_graph, callback)
    else:
        traverse_nodes(pose_graph, nodes, callback)


def dump_pose_graph(pose_graph):
    """Dump the pose graph structure as yaml.

    Args:
        pose_graph (Dict[int: :obj:`PoseGraphNode`]): slamtb's pose graph.

    Returns: (str):
        Return the yaml representation of the pose graph.
    """
    nodes = {}
    for i, i_node in pose_graph.items():
        nodes[str(i)] = {'edges': list(i_node.edges.keys())}

    return yaml.dump(nodes, indent=2)


def to_open3d_pose_graph(pose_graph):
    """Converts a pose graph into one using Open3D.

    Args:
        pose_graph (Dict[int: :obj:`PoseGraphNode`]): slamtb's pose graph.

    Returns: ((:obj:`open3d.registration.PoseGraph`), Dict[int:int]):
        Open3D's pose graph, and a dictionary that maps the Open3D's
         linear node ids into the original ones of the input pose graph.
    """
    # pylint: disable=import-outside-toplevel, no-member
    import open3d

    o3d_pg = open3d.pipelines.registration.PoseGraph()

    id_to_idx = {id: idx for idx, id in enumerate(pose_graph.keys())}

    for i, node in pose_graph.items():
        o3d_pg.nodes.append(open3d.pipelines.registration.PoseGraphNode(
            node.pose))

    for i, node in pose_graph.items():
        for j, edge in node.edges.items():
            if edge.information is None:
                print("Warning: ", __file__, "edge without information")
            information = (edge.information.numpy()
                           if edge.information is not None
                           else np.eye(6))

            o3d_edge = open3d.pipelines.registration.PoseGraphEdge(
                id_to_idx[i],
                id_to_idx[j],
                edge.transform.numpy(),
                information)
            o3d_edge.uncertain = edge.uncertain

            o3d_pg.edges.append(o3d_edge)

    return o3d_pg, dict(enumerate(pose_graph.keys()))


def from_open3d_pose_graph(o3d_pg, idx_to_id):
    """Converts a Open3D's pose graph into slamtb one.

    Args:
        o3d_pg (:obj:`open3d.registration.PoseGraph`): Open3D's
         pose graph.

        idx_to_id (Dict[int: int]): Conversion map of Open3D's node
         linear index to id.

    Returns: (Dict[int: :obj:`PoseGraphNode`]):
        slamtb's pose graph.
    """
    pose_graph = {}

    for iidx, o3d_node in enumerate(o3d_pg.nodes):
        pose_graph[idx_to_id[iidx]] = PoseGraphNode(
            iidx, torch.from_numpy(o3d_node.pose.copy()))

    for o3d_edge in o3d_pg.edges:
        edge = PoseGraphEdge(
            idx_to_id[o3d_edge.target_node_id],
            torch.from_numpy(o3d_edge.transformation.copy()),
            information=torch.from_numpy(o3d_edge.information.copy()),
            uncertain=o3d_edge.uncertain)
        pose_graph[idx_to_id[o3d_edge.source_node_id]].edges[
            idx_to_id[o3d_edge.target_node_id]] = edge

    return pose_graph


def recalculate_edges_from_poses(pose_graph):
    """
    Calculate the edge relative transformations from the nodes' poses.

    Args:
        pose_graph: The input pose graph.

    Returns:
        A copy of the input pose graph, with the exception of the geometries,
         with modified edges.
    """
    new_pose_graph = {
        node_id: node.clone(no_edges=False, shallow_geometry=True)
        for node_id, node in pose_graph.items()
    }

    for node in new_pose_graph.values():
        for edge in node.edges.values():
            end_node = new_pose_graph[edge.j]
            edge.transform = (node.pose.inverse() @ end_node.pose).inverse()

    return new_pose_graph


def recalculate_poses_from_edges(pose_graph):
    """
    Calculate the poses from the edges relative transformation.

    Only the odometry edges are considered.

    Args:
        pose_graph: The input pose graph.

    Returns:
        A copy of the input pose graph, with the exception of the geometries,
         with modified poses.
    """
    new_pose_graph = {
        node_id: node.clone(no_edges=False, shallow_geometry=True)
        for node_id, node in pose_graph.items()
    }

    nodes = list(pose_graph.keys())
    i = nodes[0]

    odometry = new_pose_graph[i].pose.inverse()
    for j in nodes[1:]:
        ij_transform = new_pose_graph[i].edges[j].transform
        odometry = ij_transform @ odometry
        new_pose_graph[j].pose = odometry.inverse()
        i = j

    return new_pose_graph
