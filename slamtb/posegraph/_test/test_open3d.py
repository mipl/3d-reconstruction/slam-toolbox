#!/usr/bin/env python
"""
Interactive tests of the conversion from/to Open3D along pose graph optimization.
"""
from slamtb.posegraph import (PoseGraphNode, PoseGraphEdge)
import math

import torch
import open3d
from fire import Fire

from slamtb.camera import RigidTransform
from slamtb.posegraph import (
    PoseGraphNode, to_open3d_pose_graph, from_open3d_pose_graph,
    recalculate_edges_from_poses)
from slamtb.viz.posegraphviewer import PoseGraphViewer

_VIEW_MATRIX = torch.tensor(
    [[-0.940493, 1.49012e-08, 0.339814, 0.692414],
     [0.246496, 0.688343, 0.68222, -1.28662],
     [-0.233909, 0.725386, -0.647381, -3.89665],
     [0, 0, 0, 1]])


class _OdometryBuilder:
    """
    Like in Open3D reconstruction system.
    """

    def __init__(self, start_state) -> None:
        self.odometry = torch.eye(4, dtype=torch.float64)
        if isinstance(start_state, dict):
            self.pose_graph = start_state
        elif isinstance(start_state, PoseGraphNode):
            self.pose_graph = {start_state.i: start_state}
        else:
            raise RuntimeError(
                f"Unkown start state type {type(start_state)}")

    def add(self, i, j, transform, information, uncertain=None):
        """
        Add a new pose.
        """
        if j == i + 1:
            self.odometry = transform @ self.odometry
            self.pose_graph[j] = PoseGraphNode(j, self.odometry.inverse())

            if uncertain is None:
                uncertain = False
            self.pose_graph[i][j] = PoseGraphEdge(
                j, transform, information, uncertain=uncertain)
        else:
            if uncertain is None:
                uncertain = True
            self.pose_graph[i][j] = PoseGraphEdge(
                j, transform, information, uncertain=uncertain)


def _make_pose_graph():
    info1 = torch.eye(6)*5000
    info2 = torch.eye(6)*5000

    pg_builder = _OdometryBuilder(PoseGraphNode(
        0, torch.eye(4, dtype=torch.double)))

    # Translation only
    pg_builder.add(0, 1, RigidTransform.from_euler_angles(
        [0, math.radians(0), 0], [0, 0, 1], dtype=torch.float64).matrix.inverse(), info1)

    # Start spining
    pg_builder.add(1, 2, RigidTransform.from_euler_angles(
        [0, math.radians(45), 0], [0, 0, 1], dtype=torch.float64).matrix.inverse(), info1)
    pg_builder.add(2, 3, RigidTransform.from_euler_angles(
        [0, math.radians(30), 0], [0, 0, 1], dtype=torch.float64).matrix.inverse(), info1)
    pg_builder.add(3, 4, RigidTransform.from_euler_angles(
        [0, math.radians(30), 0], [0, 0, 1], dtype=torch.float64).matrix.inverse(), info1)
    pg_builder.add(4, 5, RigidTransform.from_euler_angles(
        [0, math.radians(60), 0], [0, 0, 1], dtype=torch.float64).matrix.inverse(), info1)
    pg_builder.add(5, 6, RigidTransform.from_euler_angles(
        [0, math.radians(30), 0], [0, 0, 1], dtype=torch.float64).matrix.inverse(), info1)
    pg_builder.add(6, 7, RigidTransform.from_euler_angles(
        [0, math.radians(60), 0], [0, 0, 1], dtype=torch.float64).matrix.inverse(), info1)

    # Loop closure
    pg_builder.add(7, 0, RigidTransform.from_euler_angles(
        [0, math.radians(30), 0], [0, 0, 1], dtype=torch.float64).matrix.inverse(), info2,
        uncertain=False)

    return pg_builder.pose_graph


def test_conversion():
    """
    Build a graph using slamtb, show it, and convert to open3d and
    then back into slamtb and shot it again.
    """
    pose_graph = _make_pose_graph()

    PoseGraphViewer(pose_graph, title="").run(view_matrix=_VIEW_MATRIX)

    o3d_pg, idx_to_id = to_open3d_pose_graph(pose_graph)
    pose_graph = from_open3d_pose_graph(o3d_pg, idx_to_id)

    PoseGraphViewer(pose_graph).run(view_matrix=_VIEW_MATRIX)
    PoseGraphViewer(pose_graph,
                    node_traverse=list(pose_graph.keys())).run(view_matrix=_VIEW_MATRIX)


def test_pgo():
    """
    Builds a graph using slamtb, show it, and then uses Open3D's PGO,
    and show it again.
    """

    pose_graph = _make_pose_graph()

    PoseGraphViewer(pose_graph, show_grid=True).run(view_matrix=_VIEW_MATRIX)

    o3d_pg, idx_to_id = to_open3d_pose_graph(pose_graph)

    method = open3d.pipelines.registration.GlobalOptimizationLevenbergMarquardt()
    criteria = open3d.pipelines.registration.GlobalOptimizationConvergenceCriteria(
    )
    option = open3d.pipelines.registration.GlobalOptimizationOption(
        max_correspondence_distance=2.0,
        edge_prune_threshold=0.25,
        preference_loop_closure=5.0,
        reference_node=0)

    open3d.pipelines.registration.global_optimization(
        o3d_pg, method, criteria, option)

    pose_graph = from_open3d_pose_graph(o3d_pg, idx_to_id)

    pose_graph = recalculate_edges_from_poses(pose_graph)
    PoseGraphViewer(pose_graph).run(view_matrix=_VIEW_MATRIX)
    PoseGraphViewer(pose_graph,
                    node_traverse=list(pose_graph.keys())).run(
                        view_matrix=_VIEW_MATRIX)


if __name__ == '__main__':
    Fire({
        "pgo": test_pgo,
        "conversion": test_conversion
    })
