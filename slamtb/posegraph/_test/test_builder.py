#!/usr/bin/env python

import math

import torch
from fire import Fire

from slamtb.camera import RTCamera, RigidTransform
from slamtb.posegraph import (PoseGraphBuilder, SubmapPoseGraphBuilder,
                              PoseGraphNode)
from slamtb.trajectory import Odometry
from slamtb.viz.posegraphviewer import PoseGraphViewer
from slamtb.testing import load_sample2_dataset as load_sample_dataset
from slamtb.pointcloud import PointCloud


def _from_euler_angles(rot, trans):
    return RigidTransform.from_euler_angles(
        rot, trans, dtype=torch.float64).matrix.inverse()


def test_builder():
    """Uses PoseGraphBuilder to build a pose graph and show it two times:
    one traversing the nodes' pose and another traversing the
    edges. Tests wheter the nodes and the edges have equivalent
    information.
    """

    info1 = torch.eye(6)*5000
    info2 = torch.eye(6)*9000
    pg_builder = PoseGraphBuilder(PoseGraphNode(0, torch.eye(4)))
    pg_builder.add(0, 1, _from_euler_angles(
        [0, math.radians(0), 0], [0, 0, 1]), info1)
    pg_builder.add(1, 2, _from_euler_angles(
        [0, math.radians(45), 0], [0, 0, 1]), info1)

    pg_builder.add(2, 3, _from_euler_angles([0, math.radians(30), 0], [0, 0, 1]),
                   info1)
    pg_builder.add(3, 4, _from_euler_angles([0, math.radians(30), 0], [0, 0, 1]),
                   info1)
    pg_builder.add(4, 5, _from_euler_angles([0, math.radians(60), 0], [0, 0, 1]),
                   info1)
    pg_builder.add(5, 6, _from_euler_angles([0, math.radians(30), 0], [0, 0, 1]),
                   info1)
    pg_builder.add(6, 7, _from_euler_angles([0, math.radians(60), 0], [0, 0, 1]),
                   info1)
    pg_builder.add(7, 0, _from_euler_angles([0, math.radians(30), 0], [0, 0, 1]),
                   info2, uncertain=False)
    view_matrix = torch.tensor(
        [[-0.883314, -1.49012e-08, 0.468781, 0.587544],
         [0.382468, 0.578224, 0.720677, -1.32452],
         [-0.27106, 0.815878, -0.510754, -4.30711],
         [0, 0, 0, 1]])
    pose_graph = pg_builder.pose_graph
    PoseGraphViewer(pose_graph).run(view_matrix=view_matrix)

    PoseGraphViewer(pose_graph,
                    node_traverse=list(pg_builder.pose_graph.keys())
                    ).run(view_matrix=view_matrix)


def test_submap_builder():
    """Uses SubmapPoseGraphBuilder to build a pose graph and show it two times:
    one traversing the nodes' pose and another traversing the
    edges. Tests wheter the nodes and the edges have equivalent
    information.
    """

    info1 = torch.eye(6)*5000

    builder = SubmapPoseGraphBuilder()
    odo = Odometry()

    builder.append(0, None, RTCamera(), info1)

    odo.update(_from_euler_angles(
        [0, math.radians(0), 0], [0, 0, 1]))
    builder.append(1, None, odo.to_camera(), info1)

    odo.update(_from_euler_angles(
        [0, math.radians(45), 0], [0, 0, 1]))
    builder.append(2, None, odo.to_camera(), info1)

    odo.update(_from_euler_angles(
        [0, math.radians(30), 0], [0, 0, 1]))
    builder.append(3, None, odo.to_camera(), info1)

    odo.update(_from_euler_angles(
        [0, math.radians(30), 0], [0, 0, 1]))
    builder.append(4, None, odo.to_camera(), info1)

    odo.update(_from_euler_angles(
        [0, math.radians(60), 0], [0, 0, 1]))
    builder.append(5, None, odo.to_camera(), info1)

    odo.update(_from_euler_angles(
        [0, math.radians(30), 0], [0, 0, 1]))
    builder.append(6, None, odo.to_camera(), info1)

    odo.update(_from_euler_angles(
        [0, math.radians(60), 0], [0, 0, 1]))
    builder.append(7, None, odo.to_camera(), info1)

    view_matrix = torch.tensor(
        [[-0.883314, -1.49012e-08, 0.468781, 0.587544],
         [0.382468, 0.578224, 0.720677, -1.32452],
         [-0.27106, 0.815878, -0.510754, -4.30711],
         [0, 0, 0, 1]])

    pose_graph = builder.finish(None, None)
    PoseGraphViewer(builder.pose_graph).run(view_matrix=view_matrix)

    PoseGraphViewer(builder.pose_graph,
                    node_traverse=list(builder.pose_graph.keys())
                    ).run(view_matrix=view_matrix)


def test_geometry():
    """
    Tests if the geometry are correctly show.
    """
    info1 = torch.eye(6)*5000

    dataset = load_sample_dataset()
    frames = list(range(len(dataset)))

    builder = SubmapPoseGraphBuilder(
        start_camera=dataset[frames[0]].info.rt_cam)

    for i in range(1, len(frames)):
        prev_frame = dataset[frames[i-1]]
        prev_frame.bilateral_depth_filter()
        pcl, _ = PointCloud.from_frame(prev_frame, world_space=False)

        next_frame = dataset[frames[i]]
        builder.append(i, pcl, next_frame.info.rt_cam, info1)

    view_matrix = torch.tensor(
        [[0.706985, 1.49012e-08, -0.707229, -4.28965],
         [-0.194537, 0.961424, -0.19447, -0.850897],
         [0.679947, 0.275069, 0.679713, -0.109254],
         [0, 0, 0, 1]])
    PoseGraphViewer(builder.pose_graph).run(view_matrix=view_matrix)


if __name__ == '__main__':
    Fire({'builder': test_builder,
          'submap': test_submap_builder,
          'geometry': test_geometry
          })
