"""
Classes for encapsulating the trajectory and odometry.
"""

from dataclasses import dataclass
from typing import Optional, List, Dict, Iterator, Tuple
from numbers import Number

import torch

from slamtb.camera import RTCamera, RigidTransform


@dataclass
class TrajectoryItem:
    """
    A time and pose tuple for describing the camera pose at a certain time.

    Attributes:
        time: The timestamp of the sampling.
        camera: The sensor's camera.
    """
    time: float
    camera: RTCamera

    def clone(self):
        """
        Generates a copy of the item.
        """
        return TrajectoryItem(self.time, self.camera.clone())

    def __iter__(self):
        """
        Helper for attributing a instance to two variables:

        .. code:
            time, camera = TrajectoryItem(234.0, RTCamera(...))
        """
        return iter((self.time, self.camera))


class Odometry:
    """
    Class for computing frame-to-frame like odometry positions.
    """

    def __init__(self, start_odometry: Optional[torch.Tensor] = None,
                 start_information_matrix: Optional[torch.Tensor] = None):
        """Initialize the odometry accumulation.

        Args:

            start_odometry: The starting odometry, it should be the
             inverse of RTCamera's matrix.

            start_information_matrix: The starting information matrix.
        """
        self.odometry = (
            torch.eye(4, dtype=torch.double)
            if start_odometry is None else start_odometry)
        self._last_information_matrix = start_information_matrix

    def update(self, source_to_target_transform: torch.Tensor,
               information_matrix: Optional[torch.Tensor] = None):
        """Update the odometry accumulation.

        Args:

            source_to_target_transform: The transformation that aligns
             the source frame into the target one.

            information_matrix: Optional information matrix for
             keeping tracking of the estimations.

        """
        self.odometry = source_to_target_transform @ self.odometry
        self._last_information_matrix = information_matrix

    def to_camera(self) -> RTCamera:
        """
        The odometry in the camera to world space order.
        """
        return RTCamera(self.odometry.inverse())

    @property
    def information_matrix(self) -> torch.Tensor:
        """
        Returns the latest information matrix.
        """
        return self._last_information_matrix


class Trajectory:
    """
    Utility for handling a list of timestamps and its camera poses.
    """

    def __init__(self, time_cameras: Optional[List[TrajectoryItem]] = None):
        if time_cameras is not None:
            self.items = time_cameras
        else:
            self.items = []

    @classmethod
    def from_dict(cls, time_camera_dict):
        """
        Creates from a dictionary of timestamps and RTCameras.
        """
        return cls([TrajectoryItem(time, camera)
                    for time, camera in time_camera_dict.items()])

    @classmethod
    def from_dataset(cls, dataset):
        """
        Extract the trajectory from a dataset
        """
        items = []
        for frame_count in range(len(dataset)):
            frame_info = dataset.get_info(frame_count)
            items.append(TrajectoryItem(
                frame_info.timestamp, frame_info.rt_cam))
        return cls(items)

    @classmethod
    def from_posegraph(cls, pose_graph):
        """
        Creates a trajectory from a pose graph.

        The times are the nodes' id.
        """
        return cls([TrajectoryItem(node_id, node.as_camera())
                    for node_id, node in pose_graph.items()])

    def append(self, time: Number, camera: RTCamera):
        """Append a new time and camera pair to the end of the trajectory.
        """
        self.items.append(TrajectoryItem(time, camera))

    def time_sorted(self):
        """
        Sort the trajectory by its time stamps.
        """
        return Trajectory(sorted(self.items, key=lambda item: item.time))

    def to_dict(self) -> Dict[int, RTCamera]:
        """Converts into a dictionary the trajectory vector, using the times
        as keys.
        """
        return dict(self.items)

    def relative_transformations(self) -> Iterator[Tuple[Number, RigidTransform]]:
        """
        Iterates thoughout the trajectory with relative transformations between the poses.
        """
        for i in range(1, len(self.items)):
            time = self.items[i].time
            prev_cam = self.items[i-1].camera
            cam = self.items[i].camera

            yield (time, RigidTransform(
                (prev_cam.matrix.inverse() @ cam.matrix)))

    def aligned_to(self, other_trajectory):
        """
        Creates an aligned version of this trajectory in respect to another one.

        This function uses SVD-based alignment using the cameras' center
         of each trajectory. See `RigidTransform.fit_points`.

        Args:

            other_trajectory: The target trajectory to align to.

        Returns:
            A new version of this trajectory aligned to `other_trajectory`.
        """
        xyz1 = torch.tensor([item.camera.center
                             for item in self.items],
                            dtype=torch.float64)
        xyz2 = torch.tensor([item.camera.center
                             for item in other_trajectory.items],
                            dtype=torch.float64)

        count = min(xyz1.size(0), xyz2.size(0))
        xyz1 = xyz1[:count, :]
        xyz2 = xyz2[:count, :]

        return self @ RigidTransform.fit_points(xyz1, xyz2).matrix

    def start_at_identity(self):
        """
        Creates a trajectory that start at the identity matrix.

        This function will set the first camera to be the identity and
         transform the other ones according.

        Returns:
            A new trajectory where the first camera is at the identity.
        """
        if not self.items:
            return Trajectory()
        return self.left_matmul(self.items[0].camera.matrix.inverse())

    def clone(self):
        """
        Makes a copy of the trajectory.
        """
        return Trajectory([item.clone() for item in self.items])

    def __setitem__(self, index: int, item):
        if not isinstance(item, TrajectoryItem):
            item = TrajectoryItem(item[0], item[1])
        self.items[index] = item

    def __getitem__(self, index: int) -> TrajectoryItem:
        return self.items[index]

    def __len__(self) -> int:
        return len(self.items)

    def __iter__(self):
        for item in self.items:
            yield item.time, item.camera

    def __matmul__(self, transform: torch.Tensor):
        """
        Right multiply all poses by a matrix.
        """
        return Trajectory([
            TrajectoryItem(time, cam @ transform)
            for time, cam in self.items])

    def left_matmul(self, transform: torch.Tensor):
        """
        Left multiply all poses by a matrix.
        """
        return Trajectory([
            TrajectoryItem(item.time, item.camera.left_matmul(transform))
            for item in self.items])

    def __str__(self):
        return str(self.items)

    def __repr__(self):
        return str(self)
