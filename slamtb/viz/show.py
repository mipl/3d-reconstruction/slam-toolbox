"""Quick showing
"""
from pathlib import Path

import torch

import cv2
import tenviz

from slamtb.framepointcloud import FramePointCloud
from slamtb.pointcloud import PointCloud
from slamtb.surfel import SurfelCloud, SurfelModel
from slamtb.camera import RTCamera
from slamtb._utils import NoneCallProxy
from .surfelrender import SurfelRender


def create_tenviz_node_from_geometry(geometry, context=None,
                                     point_size=3):
    """Create a TensorViz node from a geometry.

    Geometries are kept in their local coordinates.

    Args:

        geometry (Object): (Almost) any slamtb geometry type.

        context (:obj:`tenviz.Context`): TensorViz context, required
         for the :class:`SurfelCloud` type.

        point_size (int): Point size for point-cloud types.

    Returns: (:obj:`tenviz.Node`):

        Tenviz node object.

    Raises:

        RuntimeError: If it cannot determine the geometry type.

    """
    if isinstance(geometry, (FramePointCloud, PointCloud)):
        pcl = geometry
        if isinstance(geometry, FramePointCloud):
            pcl = geometry.unordered_point_cloud(
                world_space=False, compute_normals=False)

        node = tenviz.nodes.PointCloud(pcl.points.view(-1, 3),
                                       NoneCallProxy(pcl.colors).view(-1, 3))
        node.style.point_size = int(point_size)
    elif isinstance(geometry, (SurfelCloud, SurfelModel)):
        surfels = geometry
        if isinstance(geometry, SurfelCloud):
            if context is None:
                raise RuntimeError(
                    "`context` is required for SurfelCloud type")
            surfels = SurfelModel.from_surfel_cloud(context, geometry)
        node = SurfelRender(surfels)
    elif isinstance(geometry, SurfelRender):
        node = geometry
    elif isinstance(geometry, RTCamera):
        node = tenviz.nodes.create_virtual_camera(
            tenviz.Projection(left=-0.05877239887227949, right=0.05877239887227949,
                              bottom=-0.04407929915420961, top=0.04407929915420961,
                              near=0.1, far=0.5),
            geometry.opengl_view_cam)
    elif isinstance(geometry, tenviz.geometry.Geometry):
        if geometry.faces is not None:
            node = tenviz.nodes.create_mesh_from_geo(geometry)
        else:
            node = tenviz.nodes.PointCloud(
                geometry.verts, colors=geometry.colors)
    elif isinstance(geometry, (tenviz.nodes.Scene, tenviz.program.DrawProgram)):
        node = geometry
    else:
        raise RuntimeError("Unknown geometry type:", type(geometry))

    return node


def _save_image(prefix, image):
    prefix_path = Path(prefix)
    existing_imgs = [str(path)
                     for path in prefix_path.parent.glob(prefix_path.name + "*")]
    existing_imgs = set(existing_imgs)

    count = 0
    while True:
        filename = f"{prefix}{count:03}.png"
        if not filename in existing_imgs:
            break
        count += 1

    cv2.imwrite(filename, cv2.cvtColor(image, cv2.COLOR_RGB2BGR))


def geoshow(geometries, width=640, height=480, point_size=3,
            title="slamtb.viz.geoshow", invert_y=False,
            view_matrix=None, ctx=None,
            screenshot_prefix="slamtb-",
            background=None):
    """Show Fusion Toolbox geometry data types.

    The geometries display can be toggle using number keys starting at '1'.

    Args:

        geometries (List[Any]): Fusion Toolbox geometry list.

        width (int): Window width.

        height (int): Window height.

        point_size (int): Size of points.

        title (str): Window title.

        invert_y (bool): Whatever to flip the scene.

    """
    # pylint: disable=too-many-branches

    if isinstance(geometries, list):
        if not geometries:
            return

    if not isinstance(geometries, list):
        geometries = [geometries]

    if ctx is None:
        ctx = tenviz.Context(width, height)

    transform = torch.eye(4, dtype=torch.float)
    if invert_y:
        transform[1, 1] = -1

    scene = []
    with ctx.current():
        for geom in geometries:
            node = create_tenviz_node_from_geometry(
                geom, context=ctx, point_size=point_size)
            node.transform = transform
            scene.append(node)

    if background is not None:
        ctx.clear_color = background
    viewer = ctx.viewer(scene, tenviz.CameraManipulator.WASD)
    viewer.title = title
    viewer.reset_view()

    if view_matrix is not None:
        viewer.view_matrix = view_matrix

    while True:
        key = viewer.wait_key(1)
        if key < 0:
            break

        key = chr(key & 0xff).lower()
        if '1' <= key <= '9':
            toggle_idx = int(key) - 1
            if toggle_idx < len(scene):
                scene[toggle_idx].visible = not scene[toggle_idx].visible

        if key == 'p':
            _save_image(screenshot_prefix, tenviz.take_screenshot(viewer))
