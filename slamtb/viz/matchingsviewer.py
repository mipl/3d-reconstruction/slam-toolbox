"""Visualizations for examining matchings and correspondences."""
from pathlib import Path
import random
from typing import Any, List, Tuple

import torch
import matplotlib.pyplot as plt
import numpy as np

import tenviz

from slamtb.camera import RigidTransform

from .show import create_tenviz_node_from_geometry, geoshow


class _MatchingTrackProgram(tenviz.DrawProgram):
    """
    Shader program for drawing matching between 3D geometry.
    """

    def __init__(self, track_points: torch.Tensor, color: torch.Tensor):
        """
        Loads and setup the program.

        Args:

            track_points: A [Nx3] tensor with 3d points.
            color: A [Nx3] tensor with the color of each points.

        """
        shader_dir = Path(__file__).parent / "shaders"
        super().__init__(tenviz.DrawMode.Lines, shader_dir / "pg_edges.vert",
                         shader_dir / "pg_edges.frag")

        indices = torch.tensor(
            [(i, i + 1) for i in range(len(track_points)-1)],
            dtype=torch.int32)
        color = color.repeat(len(track_points), 1)

        self["in_position"] = track_points
        self["in_color"] = color
        self["ProjModelview"] = tenviz.MatPlaceholder.ProjectionModelview
        self.indices.from_tensor(indices)
        self.style.line_width = 3.0

        if track_points.nelement() > 0:
            self.set_bounds(track_points)


def draw_trackings_3d(geometries: List[Any], trackings: List[List[Tuple[float, float, float]]]):
    """
    Shows a 3D visualization window with a series of geometries along tracks.
    """
    ctx = tenviz.Context()

    cmap = plt.get_cmap("hsv", len(trackings))

    with ctx.current():

        x_translation = 0.0
        for geometry in geometries:
            min_x = geometry.points[:, 0].min().item()
            max_x = geometry.points[:, 0].max().item()
            x_translation += abs(min_x)
            transform = RigidTransform.from_translation((x_translation, 0, 0))
            geometry.itransform(transform.matrix)
            x_translation += max_x

        scene = [
            create_tenviz_node_from_geometry(geometry, ctx)
            for geometry in geometries]

        colormap_idxs = list(range(len(trackings)))
        random.shuffle(colormap_idxs)
        for i, track in enumerate(trackings):
            track_points = torch.stack([
                geometries[j].points[point_idx.item()]
                for j, point_idx in enumerate(track)
                if point_idx.item() > -1])

            scene.append(_MatchingTrackProgram(
                track_points, torch.tensor(cmap(colormap_idxs[i])[:3], dtype=torch.float32)))

    geoshow(scene, ctx=ctx, invert_y=True)


def draw_matchings_2d(image1: np.ndarray, image2: np.ndarray,
                      yx_points1: torch.Tensor, yx_points2: torch.Tensor,
                      matchings: torch.Tensor):
    """
    Draw matchings in matplotlib.

    Args:
        image1: RGB or GRAY image.
        image2: RGB or GRAY image.
        yx_points1: Y and X coordinates shape should be
         [Nx2] and type int64.
        yx_points2: Y and X coordinates shape should be
         [Mx2] and type int64.
        matchings: Matching from 1 to 2, shape is
         [Qx2].
    """
    canvas = np.concatenate([image1, image2], axis=1)
    width1 = image1.shape[1]

    plt.imshow(canvas)
    for match in matchings:
        source_idx, target_idx = match[0], match[1]

        src_y, src_x = yx_points1[source_idx]
        tgt_y, tgt_x = yx_points2[target_idx]

        plt.plot([src_x, tgt_x + width1], [src_y, tgt_y])
