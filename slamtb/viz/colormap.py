"""
Simple wrapper around Matplotlib's colormaps.
"""
from matplotlib.colors import Colormap as _Colormap

import matplotlib.pyplot as plt


class ColorMap:
    """
    Getting a color from a colormap by an index is a common operation in many
    SLAM-Toolbox drawings. This class wraps the matplotlib one into our
    expected color tensor format. The goal is to allow passing either
    a string with the colormap's name or an object.
    """

    def __init__(self, colormap, lut_size: int) -> None:
        if isinstance(colormap, str):
            self.colormap = plt.get_cmap(colormap, lut_size)
        elif isinstance(colormap, ColorMap):
            self.colormap = colormap
        elif isinstance(colormap, _Colormap):
            self.colormap = colormap
        else:
            self.colormap = colormap

    def __call__(self, index):
        return self.colormap(index)[:3]
