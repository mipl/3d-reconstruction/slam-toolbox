#version 420

in vec3 out_color;
out vec4 out_frag;

void main() {
  out_frag = vec4(out_color, 1);
}
