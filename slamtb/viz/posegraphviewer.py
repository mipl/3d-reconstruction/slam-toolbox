"""Visualization functions for posegraph
"""
from pathlib import Path

import numpy as np
import torch
import cv2
import pyquaternion

import tenviz
from slamtb.camera import RTCamera
from slamtb.sensor import get_sensor_kcamera, PresetIntrinsics
from slamtb.viz import create_tenviz_node_from_geometry

from .colormap import ColorMap
from ._virtualcamera import create_virtual_camera


class _EdgesDrawProgram(tenviz.DrawProgram):
    def __init__(self, point_graph, cmap):
        shader_dir = Path(__file__).parent / "shaders"
        super().__init__(tenviz.DrawMode.Lines, shader_dir / "pg_edges.vert",
                         shader_dir / "pg_edges.frag")
        points = []
        colors = []
        indices = []

        for i, (i_point, edges) in point_graph.items():
            for j, j_point in edges.items():
                last_idx = len(points)

                points.append(i_point)
                points.append(j_point)

                colors.append(cmap(i))
                colors.append(cmap(j))

                indices.append([last_idx, last_idx + 1])

        points = torch.tensor(points, dtype=torch.float32)
        colors = torch.tensor(colors, dtype=torch.float32)
        indices = torch.tensor(indices, dtype=torch.int32)

        self["in_position"] = points
        self["in_color"] = colors
        self["ProjModelview"] = tenviz.MatPlaceholder.ProjectionModelview
        self.indices.from_tensor(indices)
        self.style.line_width = 3.0

        if points.nelement() > 0:
            self.set_bounds(points)


class _GraphColorMap:
    def __init__(self, cmap, node_ids):
        self.cmap = ColorMap(cmap, len(node_ids))
        self.id_to_idx = {id: idx for idx, id in enumerate(node_ids)}

    def __call__(self, node_id):
        return self.cmap(self.id_to_idx[node_id])


def _traverse_nodes(pose_graph, callback, nodes=None):
    if nodes is not None:
        i = nodes[0]
        accum_transform = pose_graph[i].pose

        callback(i, RTCamera(accum_transform), None)
        for j in nodes[1:]:
            accum_transform = pose_graph[i][j].transform @ accum_transform
            callback(j, RTCamera(accum_transform), i)
            i = j
    else:
        j = None

        for i, node in pose_graph.items():
            callback(i, node.as_camera(), j)
            j = i


class PoseGraphScene(tenviz.nodes.Scene):
    def __init__(self, pose_graph, node_traverse=None,
                 kcam=None, kcam_far=0.3, colormap="rainbow",
                 cam_line_width=3, cam_show_axis=False,
                 tenviz_context=None):
        super().__init__()

        self.cameras = {}
        self.geometries = {}

        if kcam is None:
            kcam = get_sensor_kcamera(PresetIntrinsics.ASUS_XTION, 640, 480)

        cmap = _GraphColorMap(colormap, pose_graph.keys())

        point_graph = {}

        self.camera_scene = tenviz.nodes.Scene()

        def _add_to_scene(i, camera, prev_i):
            color = cmap(i)[:3]
            node = pose_graph[i]

            transform = camera.matrix.inverse()

            vcam = create_virtual_camera(
                kcam, camera, far_dist=(node.max_depth if node.max_depth is not None
                                        else kcam_far),
                color=color, line_width=cam_line_width,
                show_axis=cam_show_axis)
            vcam.transform = transform
            self.camera_scene.add(vcam)
            self.cameras[i] = vcam

            if node.geometry is not None:
                geom_node = create_tenviz_node_from_geometry(
                    node.geometry, tenviz_context)
                geom_node.transform = transform
                self.add(geom_node)
                self.geometries[i] = geom_node

            point = transform[:3, 3].tolist()
            point_graph[i] = (point, {})
            if prev_i is not None:
                point_graph[prev_i][1][i] = point

        _traverse_nodes(
            pose_graph, _add_to_scene,
            nodes=node_traverse)

        self.add(self.camera_scene)
        for i in point_graph:
            for j in pose_graph[i].edges:
                if j in point_graph:
                    point_graph[i][1][j] = point_graph[j][0]

        self.edges = _EdgesDrawProgram(point_graph, cmap)
        self.add(self.edges)


class TransformationController:
    _XROT = "X rot"
    _YROT = "Y rot"
    _ZROT = "Z rot"
    _XT = "X"
    _YT = "Y"
    _ZT = "Z"
    _TSIZE = 2000

    def __init__(self, title="Align tool"):
        self.transformation = torch.eye(4)
        self.invx = 1.0
        self.invy = 1.0
        self.invz = 1.0
        self.title = title
        self._is_show = False

    def show(self):
        if self._is_show:
            return

        cv2.namedWindow(self.title)
        cv2.createTrackbar(TransformationController._XROT, self.title,
                           0, 360, self._update_align)
        cv2.createTrackbar(TransformationController._YROT, self.title,
                           0, 360, self._update_align)
        cv2.createTrackbar(TransformationController._ZROT, self.title,
                           0, 360, self._update_align)
        cv2.createTrackbar(TransformationController._XT, self.title,
                           int(TransformationController._TSIZE/2),
                           TransformationController._TSIZE,
                           self._update_align)
        cv2.createTrackbar(TransformationController._YT, self.title,
                           int(TransformationController._TSIZE/2),
                           TransformationController._TSIZE,
                           self._update_align)
        cv2.createTrackbar(TransformationController._ZT, self.title,
                           int(TransformationController._TSIZE/2),
                           TransformationController._TSIZE,
                           self._update_align)
        self._is_show = True

    def hide(self):
        if self._is_show:
            cv2.destroyWindow(self.title)
            self._is_show = False

    def update(self):
        if self._is_show:
            cv2.waitKey(1)

    def _update_align(self, _=None):
        rotx = pyquaternion.Quaternion(
            axis=[1, 0, 0],
            angle=np.deg2rad(cv2.getTrackbarPos(self._XROT, self.title)))
        roty = pyquaternion.Quaternion(
            axis=[0, 1, 0],
            angle=np.deg2rad(cv2.getTrackbarPos(self._YROT, self.title)))
        rotz = pyquaternion.Quaternion(
            axis=[0, 0, 1],
            angle=np.deg2rad(cv2.getTrackbarPos(self._ZROT, self.title)))

        rot = (rotx*roty*rotz).transformation_matrix
        trans = np.eye(4)
        trans[0, 3] = (cv2.getTrackbarPos(
            self._XT, self.title) / self._TSIZE)*20 - 10
        trans[1, 3] = (cv2.getTrackbarPos(
            self._YT, self.title) / self._TSIZE)*20 - 10
        trans[2, 3] = (cv2.getTrackbarPos(
            self._ZT, self.title) / self._TSIZE)*20 - 10
        scale = np.eye(4)
        scale[0, 0] = self.invx
        scale[1, 1] = self.invy
        scale[2, 2] = self.invz

        self.transformation = torch.from_numpy(scale @ trans @ rot).float()


class PoseGraphViewer:
    def __init__(self, pose_graph, title="PoseGraph", node_traverse=None,
                 kcam=None, kcam_far=0.3,
                 colormap="rainbow", cam_line_width=3, cam_show_axis=False,
                 show_grid=False):
        """View a pose graph.

        Args:

            pose_graph (Dict[int,
            :obj:`slamtb.posegraph.core.PoseGraphNode`]): Pose graph.

            kcam (:obj:`slamtb.camera.KCamera`, optional): Camera
            intrinsic for drawing the virtual cameras.

            near (float): Near plane distance.

            far (float): Far plane distance.

            traverse_nodes (List[int], optional): Specify a node traversal
            order when building the scene.

            title (str): Window's title.

            colormap (str): Matplotlib's colormap name.
        """
        self.ctx = tenviz.Context()
        self.title = title

        with self.ctx.current():
            self.scene = PoseGraphScene(
                pose_graph, node_traverse=node_traverse,
                kcam=kcam, kcam_far=kcam_far,
                colormap=colormap,
                cam_line_width=cam_line_width, cam_show_axis=cam_show_axis,
                tenviz_context=self.ctx)
            if show_grid:
                self.scene.add(tenviz.nodes.create_axis_grid(-5, 5, 11))
        self._transform_tool = TransformationController(title)

    def run(self, view_matrix=None):
        viewer = self.ctx.viewer(
            self.scene, cam_manip=tenviz.CameraManipulator.WASD)
        viewer.title = self.title

        if view_matrix is not None:
            viewer.view_matrix = view_matrix
        toggle_keys = {48: 9}
        toggle_keys.update({num_key + 49: num_key for num_key in range(10)})
        toggle_keys.update({
            num_key + 290: num_key + 10 for num_key in range(12)})

        node_ids = list(self.scene.cameras.keys())
        cam_nodes = list(self.scene.cameras.values())
        geom_nodes = list(self.scene.geometries.values())
        while True:
            key = viewer.wait_key(1)
            if key < 0:
                break

            if key in toggle_keys:
                toggle_idx = toggle_keys[key]

                if toggle_idx < len(cam_nodes):
                    cam_nodes[toggle_idx].visible = not cam_nodes[toggle_idx].visible
                if toggle_idx < len(geom_nodes):
                    geom_nodes[toggle_idx].visible = not geom_nodes[toggle_idx].visible

                visible_set = {node_ids[i] for i in range(
                    len(cam_nodes)) if cam_nodes[i].visible}
                viewer.title = f"{self.title} - {visible_set}"

            key = chr(key & 0xff).lower()
            if key == 'm':
                self.scene.edges.visible = not self.scene.edges.visible
            elif key == 'n':
                self.scene.camera_scene.visible = not self.scene.camera_scene.visible
            elif key == 'b':
                self._transform_tool.show()

            self._transform_tool.update()
            self.scene.transform = self._transform_tool.transformation
