"""Trajectory viewer.
"""

from typing import Dict, Any, Union
import torch

import tenviz

from slamtb.camera import KCamera, RTCamera
from slamtb.trajectory import Trajectory

from .colormap import ColorMap
from ._virtualcamera import create_virtual_camera


def create_tenviz_trajectory_node(trajectory: Union[Trajectory, Dict[Any: RTCamera]],
                                  kcam: KCamera, cam_far: float = 0.1,
                                  colormap: Union[ColorMap, str, None] = None,
                                  line_width: float = 3,
                                  show_axis: bool = True) -> tenviz.nodes.Scene:
    """
    Returns a Tensorviz Scene with a sequence of virtual cameras representing a given trajectory.

    Note: A Tensorviz's context must be binded.

    Args:
        trajectory: The given trajectory.
        kcam: Sensor's camera intrinsics for drawing.
        cam_far: The length of each virtual camera far plane.
        colormap: Indexable colormap for coloring each virtual camera.
        line_width: The size of virtual cameras' line.
        show_axis: Whether it should draw an X (red),Y (blue) and Z (gree) axes for each camera
         pose.

    Returns:
        The trajectory represented in TensorViz.
    """
    if isinstance(trajectory, Trajectory):
        trajectory = trajectory.to_dict()

    if colormap is None:
        colormap = "Blues"

    cmap = ColorMap(colormap, len(trajectory))
    cam_nodes = []
    for i, rt_cam in enumerate(trajectory.values()):
        color = cmap(i)[:3]
        cam_nodes.append(create_virtual_camera(kcam, rt_cam, far_dist=cam_far,
                                               color=color, line_width=line_width,
                                               show_axis=show_axis))

    return tenviz.nodes.Scene(cam_nodes)


class TrajectoryViewer:
    """Viewer for camera trajectories only"""

    def __init__(self, trajectories, kcam=None, cam_far=0.1, colormaps=None,
                 title=None):
        """Initialize the viewer.

        Args:

            trajectories (List[Dict[float:
             :obj:`slamtb.camera.RTCamera`]]): List of trajectory
             dictionary, keys are the timestamps, and values are the
             absolute camera's pose.

            kcam (:obj:`slamtb.camera.KCamera`, optional): Intrinsic camera
             frustum shape.

            cam_far (float): Cameras' far plane.

            colormaps (List[str], optional): Matplotlib colormap names
             for coloring each trajectory.

            title (str, optional): Window title.

        """
        self.gl_context = tenviz.Context()
        self._title = title
        if kcam is None:
            self.kcam = KCamera.from_params(525, 525, (525/2, 525/2))
        else:
            self.kcam = kcam
        self.cam_far = cam_far
        self._scene = []

        self._default_cmaps = ["Blues", "Greens", "Reds", "Purples"]
        self._cmap_count = 0

        if colormaps is None:
            colormaps = [None]*len(trajectories)

        cxs = []
        czs = []
        for traj, color in zip(trajectories, colormaps):
            if isinstance(traj, Trajectory):
                traj = traj.to_dict()
            self.add_trajectory(traj, color)

            cxs.extend([rt_cam.center[0] for rt_cam in traj.values()])
            czs.extend([rt_cam.center[2] for rt_cam in traj.values()])

        with self.gl_context.current():
            axis = tenviz.nodes.create_axis_grid(
                min(min(cxs), min(czs)),
                max(max(cxs), max(czs)), 10)
            self._scene.append(axis)

    def add_trajectory(self, trajectory, colormap=None):
        """Add a trajectory.

        Args:

            trajectory (Dict[float: :obj:`slamtb.camera.RTCamera`]): A trajectory.

            colormap (str): Matplotlib colormap name for coloring the
             trajectory frustums. Each pose will receive a colormap
             gammut.

        """

        if colormap is None:
            colormap = self._default_cmaps[self._cmap_count % len(
                self._default_cmaps)]
            self._cmap_count += 1

        with self.gl_context.current():
            self._scene.append(create_tenviz_trajectory_node(
                trajectory, self.kcam, cam_far=self.cam_far, colormap=colormap))

    def run(self):
        """Show viewer window.
        """
        viewer = self.gl_context.viewer(
            self._scene, cam_manip=tenviz.CameraManipulator.WASD)
        if self._title is not None:
            viewer.title = self._title
        while True:
            key = viewer.wait_key(1)
            if key < 0:
                break

            key = chr(key & 0xff)
            if key == '1':
                viewer.view_matrix = torch.tensor(
                    [[1, 0, 0, 0],
                     [0, 0, 1, 0],
                     [0, 1, 0, -5],
                     [0, 0, 0, 1]], dtype=torch.float)
            elif key == '2':
                viewer.view_matrix = torch.tensor(
                    [[1, 0, 0, 0],
                     [0, 1, 0, 0],
                     [0, 0, -1, -5],
                     [0, 0, 0, 1]], dtype=torch.float)


def _test():
    # pylint: disable=import-outside-toplevel
    from slamtb.testing import load_sample2_dataset

    dataset = load_sample2_dataset()
    trajectory = {i: dataset.get_info(i).rt_cam for i in range(len(dataset))}
    viewer = TrajectoryViewer([trajectory])

    viewer.run()


if __name__ == '__main__':
    _test()
