"""
Convenience for drawing cameras in 3D visualizations.
"""
from pathlib import Path
from typing import Tuple

import torch

import tenviz

from slamtb.camera import RTCamera, KCamera

_SHADER_DIR = Path(__file__).parent / 'shaders'


def create_virtual_camera(k_camera: KCamera, rt_camera: RTCamera, far_dist: float = 1.0,
                          color: Tuple[float, float, float] = None, show_axis: bool = True,
                          line_width: float = 3.0) -> tenviz.DrawProgram:
    """Creates a simple representation of a camera's frustum.

    Args:

        kcamera: Projection parameters.

        rt_camera: Camera.

        far_dist: Size of the camera's far plane.

        color: Wireframe's color.

        show_axis: Show a small camera's axis.

        line_width: Wireframe's line width.

    Returns:
        Tensorviz's DrawProgram for showing the camera.
    """
    proj = tenviz.Projection.from_intrinsics(
        k_camera.matrix, far_dist*0.01, far_dist)

    verts = torch.tensor([
        [0, 0, 0],                                    # 0
        [proj.far_left, proj.far_top, proj.far],     # 1
        [proj.far_left, proj.far_bottom, proj.far],  # 2
        [proj.far_right, proj.far_bottom, proj.far],  # 3
        [proj.far_right, proj.far_top, proj.far],    # 4
    ], dtype=torch.float)

    indices = torch.tensor([[0, 1], [0, 2], [0, 3], [0, 4],
                            [1, 2], [2, 3], [3, 4], [4, 1]],
                           dtype=torch.int)

    draw = tenviz.DrawProgram(tenviz.DrawMode.Lines, _SHADER_DIR / "camera.vert",
                              _SHADER_DIR / "camera.frag")

    if color is None:
        color = torch.tensor([0.6, 0.8, 0.75])
    else:
        color = torch.tensor(color).float()

    draw['in_position'] = verts
    draw['Color'] = color
    draw['ProjModelview'] = tenviz.MatPlaceholder.ProjectionModelview

    draw.indices.from_tensor(indices)
    draw.set_bounds(verts)

    draw.style.line_width = float(line_width)

    length = proj.near
    scene = [draw]

    if show_axis:
        axis = tenviz.nodes.create_quiver(torch.zeros(3, 3),
                                          torch.tensor([[1.0, 0.0, 0.0],
                                                        [0.0, 1.0, 0.0],
                                                        [0.0, 0.0, -1.0]])*length,
                                          torch.tensor([[255, 0, 0],
                                                        [0, 255, 0],
                                                        [0, 0, 255]], dtype=torch.uint8))
        scene.append(axis)

    scene = tenviz.nodes.Scene(scene)
    scene.transform = rt_camera.matrix.inverse()
    return scene
