"""
A point cloud structure.
"""
from typing import List

import torch
import numpy as np

from .camera import KCamera, RTCamera, RigidTransform
from .feature import IntFeatureDict, KeypointFeatures

from ._utils import NoneCallProxy, ensure_torch
from ._cslamtb import (PointCloudBase as _PointCloudBase,
                       PointVolumeBase as _PointVolumeBase)



class PointVolume(_PointVolumeBase):
    """
    A discrete point set in 3D. Useful for downsampling and joining point clouds.
    """

    def __init__(self, aabb, voxel_size, feature_size=None):
        super().__init__(aabb[0, :], aabb[1, :],
                         voxel_size,
                         (feature_size
                          if feature_size is not None else -1))

    def to_point_cloud(self):
        point_cloud = PointCloud.empty(
            self.size,
            feature_size=self.feature_size)
        super().to_point_cloud(point_cloud)
        return point_cloud


class PointCloud(_PointCloudBase):
    """Point cloud data structure.

    Point, color and normals are stored as torch tensor. Slicing
     operator is avaliable for capturing a new point cloud with all attributes.

    Attributes:

        points (:obj:`torch.Tensor`): 3D points (N x 3) of float.

        colors (:obj:`torch.Tensor`): Point colors (N x 3) of uint8.

        normals (:obj:`torch.Tensor`): Point normal vectors (N x 3) of float.

        features (:obj:`torch.Tensor`): Associated point features (FxN) of floats.
         F is the feature size.

    """

    @classmethod
    def from_open3d(cls, pcl):
        """Converts a :obj:`open3d.PointCloud` object to this project's Point
        Cloud.
        """
        return cls(torch.from_numpy(np.array(pcl.points, dtype=np.float32)),
                   torch.from_numpy((np.array(pcl.colors)*255.0)).byte(),
                   torch.from_numpy(np.array(pcl.normals, dtype=np.float32)))

    @staticmethod
    def from_frame(frame, world_space=False, compute_normals=True):
        """Creates a point cloud from a frame.

        Args:

            world_space (bool, optional): `True` to set the points on
             world space, according to frame's extrinsic
             parameters. Default is `True`.

            compute_normals (bool, optional): Use normals. Default is `True`.

        Returns: ((PointCloud, torch.Tensor)): Point cloud and a (H x
         W) bool tensor mask of the frame pixels included in the point
         cloud.

        """
        # pylint: disable=import-outside-toplevel,cyclic-import
        from .framepointcloud import FramePointCloud

        if isinstance(frame, FramePointCloud):
            fpcl = frame
        else:
            fpcl = FramePointCloud.from_frame(frame)

        pcl = fpcl.unordered_point_cloud(
            world_space=world_space,
            compute_normals=compute_normals)

        return pcl, fpcl.mask

    @staticmethod
    def from_keypoints_2d(frame,
                          keypoint_features: KeypointFeatures,
                          with_normals: bool = True):
        """
        Creates a pointcloud using only those points that have a 2D features
        from a frame or frame point cloud.

        Args:

        """
        # pylint: disable=import-outside-toplevel
        # To avoid cyclic import
        from .framepointcloud import FramePointCloud

        if isinstance(frame, FramePointCloud):
            fpcl = frame
        else:
            fpcl = FramePointCloud.from_frame(frame)

        keypoint_features = keypoint_features.masked_select(fpcl.mask)

        yx = keypoint_features.keypoints
        points = fpcl.points[yx[:, 0], yx[:, 1], :]

        colors = normals = None
        if fpcl.colors is not None:
            colors = fpcl.colors[yx[:, 0], yx[:, 1], :]

        if with_normals:
            normals = fpcl.normals[yx[:, 0], yx[:, 1], :]

        return (PointCloud(points, colors=colors, normals=normals,
                           features=keypoint_features.descriptors),
                keypoint_features)

    @classmethod
    def from_depth(cls, depth_image: torch.Tensor, kcam: KCamera, depth_scale: float,
                   colors: torch.Tensor = None, rt_cam: RTCamera = None, world_space: bool = False,
                   compute_normals: bool = True) -> torch.Tensor:
        """
        Creates a point cloud from a depth image.
        See :class:`slamtb.frame.FramePointCloud` for arguments.
        """
        # pylint: disable=import-outside-toplevel
        from .framepointcloud import FramePointCloud

        fpcl = FramePointCloud.from_depth(depth_image, kcam, depth_scale,
                                          rt_cam=rt_cam, colors=colors)
        return fpcl.unordered_point_cloud(
            world_space=world_space,
            compute_normals=compute_normals)

    @classmethod
    def empty(cls, size: int, feature_size: int = 0, colors: bool = True, normals: bool = True):
        """
        Creates a point cloud with non-initialized values (i.e., using torch.empty)

        Args:
            size: The number of points.

            feature_size: The number of channels of each points' feature.
             Use 0 for none per point features.
        """
        return cls(torch.empty(size, 3, dtype=torch.float32),
                   (torch.empty(size, 3, dtype=torch.uint8)
                    if colors else None),
                   (torch.empty(size, 3, dtype=torch.float32)
                    if normals else None),
                   (torch.empty(feature_size, size, dtype=torch.float32)
                    if feature_size > 0 else None))

    def __init__(self, points: torch.Tensor, colors: torch.Tensor = None,
                 normals: torch.Tensor = None, features: torch.Tensor = None):
        super().__init__(ensure_torch(points, torch.float32),
                         ensure_torch(colors, torch.uint8),
                         ensure_torch(normals, torch.float32),
                         ensure_torch(features, torch.float32))

    def clone(self):
        """
        Create a copy of the current instance.

        Returns:
            Instance's copy.
        """
        # pylint: disable=no-member
        return PointCloud(
            self.points.clone(),
            NoneCallProxy(self.colors).clone(),
            NoneCallProxy(self.normals).clone(),
            NoneCallProxy(self.features).clone())

    def transform(self, matrix: torch.Tensor):
        """Transform the points and normals and return and new point cloud.
        Colors and features are shared.

        Args:
            matrix: A (4 x 4) or (3 x 4) transformation matrix.

        Returns:
            (:obj:`PointCloud`): New transformed point cloud.
        """

        if self.size == 0:
            return PointCloud(self.points)

        rit = RigidTransform(matrix)
        points = rit @ self.points

        normals = None
        if self.normals is not None:
            normals = rit.transform_normals(self.normals)

        return PointCloud(points, self.colors, normals, self.features)

    def itransform(self, matrix: torch.Tensor):
        """Transform the points and normals inplace.

        Args:

            matrix: A (4 x 4) or (3 x 4) transformation matrix.
        """

        transform = RigidTransform(matrix.float().to(self.device))
        transform.inplace(self.points)
        if self.normals is not None:
            transform.inplace_normals(self.normals)

    def index_select(self, index: torch.Tensor):
        """
        Slices the point cloud according to an index array.

        Args:
            index: A int64 array with indices to selected points.

        Returns:
            (:obj:`PointCloud`): A new point cloud with the sliced entries.
        """
        # pylint: disable=unsubscriptable-object
        return PointCloud(
            self.points[index],
            self.colors[index] if self.colors is not None else None,
            self.normals[index] if self.normals is not None else None,
            self.features[:, index] if self.features is not None else None)

    def to_open3d(self):
        """Converts the point to cloud to a :obj:`open3d.PointCloud`.

        Returns: ((:obj:`open3d.geometry.PointCloud`,
                   :obj:`open3d.pipelines.registration.Feature`)):
            Open3D point cloud along with the features from this point cloud if they exists
        """
        # pylint: disable=import-outside-toplevel, no-member
        import open3d as o3d

        pcl = o3d.geometry.PointCloud()

        if self.is_empty():
            return pcl

        pcl.points = o3d.utility.Vector3dVector(self.points.cpu().numpy())
        if self.colors is not None:
            pcl.colors = o3d.utility.Vector3dVector(
                self.colors.cpu().float().numpy()/255.0)

        if self.normals is not None:
            pcl.normals = o3d.utility.Vector3dVector(
                self.normals.cpu().numpy())
        features = None
        if self.features is not None:
            features = o3d.pipelines.registration.Feature()
            features.data = self.features.transpose(1, 0).cpu().numpy()
        return pcl, features

    def is_empty(self):
        """Returns whatever the point cloud is empty.
        """
        return self.size == 0

    def to(self, dst):
        r"""Change the point cloud device or dtype. Dtype are applied only to
        points and normals.

        Args:

            dst (torch.dtype, torch.device, str): Dtype or torch device.

        Returns:
            (:obj:`slamtb.pointcloud.PointCloud`): Converted point cloud.
        """
        # pylint: disable=no-member
        return PointCloud(self.points.to(dst),
                          NoneCallProxy(self.colors).to(dst),
                          NoneCallProxy(self.normals).to(dst),
                          NoneCallProxy(self.features).to(dst))

    @property
    def size(self) -> int:
        r"""
        Returns:
            The point cloud size.
        """
        # pylint: disable=no-member
        return self.points.size(0)

    @property
    def device(self) -> torch.device:
        r"""
        Returns:
            The device in which this point cloud is stored.
        """
        # pylint: disable=no-member
        return self.points.device

    @property
    def has_features(self) -> bool:
        """
        Returns:
             Whatever this instance has features.
        """
        # pylint: disable=no-member
        return self.features is not None

    @property
    def feature_size(self) -> int:
        """
        Returns:

             (int): The size of the feature channel.
        """
        # pylint: disable=no-member
        if self.features is not None:
            return self.features.size(0)
        return 0

    def downsample(self, voxel_size: int):
        """Downsample the surfel cloud by discretizing it into a volume of a
        given voxel size.

        Args:

            voxel_size (float): The discrete volume's voxel size.

        Returns:

            (:obj:`slamtb.surfel.SurfelCloud`): New surfel cloud. If
             `voxel_size` <= 0.0 then returns self.

        """
        # pylint: disable=no-member

        if voxel_size <= 0.0:
            return self

        min_pos = self.points.min(0)[0].tolist()
        max_pos = self.points.max(0)[0].tolist()

        volume = PointVolume(
            torch.tensor([[min_pos[0], min_pos[1], min_pos[2]],
                          [max_pos[0], max_pos[1], max_pos[2]]]),
            voxel_size, self.feature_size)
        volume.accumulate(self)
        return volume.to_point_cloud()

    def featured_select(self, features: IntFeatureDict):
        """
        Select only those indices matching with feature dictionary and return
         as a new point cloud with the corresponding features.

        Args:
            features: Features and its indices to slice the point cloud.

        Returns:
            A new point cloud with the descriptors from the feature dictionary.
        """
        # pylint: disable=unsubscriptable-object
        key_points = features.keys()
        return PointCloud(
            self.points[key_points, :],
            NoneCallProxy(self.colors)[key_points, :],
            NoneCallProxy(self.normals)[key_points, :],
            features.descriptors())

    def __getitem__(self, *args):
        """Slicing operator for all instance properties.

        Returns: (PointCloud): Sliced point cloud.
        """
        # pylint: disable=no-member
        return PointCloud(
            self.points.__getitem__(*args),
            NoneCallProxy(self.colors).__getitem__(*args),
            NoneCallProxy(self.normals).__getitem__(*args),
            NoneCallProxy(self.features).__getitem__(*args))

    def __len__(self):
        return self.size

    def __str__(self):
        has_colors = self.colors is not None
        has_normals = self.normals is not None
        return f"PointCloud(len({len(self)}), with colors={has_colors}, with normals={has_normals})"

    def __repr__(self):
        return str(self)


def stack_pcl(pcl_list: List[PointCloud]) -> PointCloud:
    """Concatenate point clouds into one.

    All point cloud need to have the same set of attributes, that is,
    it will raise a error when join point cloud that have normals with
    ones that don't.

    Args:

        pcl_list: Point cloud list.

    Returns:
        Joined point cloud.
    """

    pcl_list = [pcl for pcl in pcl_list
                if not pcl.is_empty()]
    if not pcl_list:
        return PointCloud(torch.Tensor([], dtype=torch.float))

    point_count = sum((pcl.points.size(0) for pcl in pcl_list))
    normal_count = sum((pcl.normals.size(0) for pcl in pcl_list
                        if pcl.normals is not None))
    color_count = sum((pcl.colors.size(0) for pcl in pcl_list
                       if pcl.colors is not None))
    feature_count = sum((pcl.feature.size(1) for pcl in pcl_list
                         if pcl.feature is not None))

    if normal_count > 0 and normal_count != point_count:
        raise RuntimeError(
            """Mixed point clouds with and without normals""")

    if color_count > 0 and color_count != point_count:
        raise RuntimeError(
            """Mixed point clouds with and without colors""")

    if feature_count > 0 and feature_count != point_count:
        raise RuntimeError(
            """Mixed point clouds with and without features""")

    points = torch.cat([pcl.points for pcl in pcl_list
                        if not pcl.is_empty()])
    normals = torch.cat([pcl.normals for pcl in pcl_list
                         if not pcl.is_empty()])
    colors = torch.cat([pcl.colors for pcl in pcl_list
                        if not pcl.is_empty()])
    return PointCloud(points, colors, normals)
