"""View FTB datasets
"""
import argparse

from slamtb.data.ftb import load_ftb
from slamtb.viz.datasetviewer import DatasetViewer


def _main():
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument("input_ftb", metavar="input-ftb",
                        help="Input FTB directory")

    args = parser.parse_args()

    dataset = load_ftb(args.input_ftb)
    DatasetViewer(dataset).run()


if __name__ == '__main__':
    _main()
