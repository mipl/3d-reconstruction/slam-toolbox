"""Common filtering functions used by 3d reconstruction on frames.
"""

from enum import Enum
from typing import Iterable, List, Union

import torch
import torch.nn.functional
from torchvision.transforms.functional import to_tensor
import numpy as np
import kornia
import cv2

from slamtb.frame import FrameInfo
from slamtb._cslamtb import (Processing as _Processing, EstimateNormalsMethod,
                             DownsampleXYZMethod)
from slamtb._utils import ensure_torch, empty_ensured_size, depth_image_to_uvz


def bilateral_depth_filter(depth: Union[torch.Tensor, np.ndarray],
                           mask: Union[torch.Tensor, np.ndarray] = None,
                           out_tensor: Union[torch.Tensor, np.ndarray] = None,
                           filter_width: int = 6,
                           sigma_color: float = 29.9999880000072,
                           sigma_space: float = 4.50000000225,
                           depth_scale: float = 1.0) -> Union[torch.Tensor, np.ndarray]:
    """Apply a bilateral filter for smoothing depth images.

    Default argument values are the same ones used by ElasticFusion.
    It can work with numpy as torch tensors.

    Args:

        depth: Input depth image. May
         have any dtype.

        mask: Mask of where depth image's valid pixels.

        out_tensor (optional): Optional output tensor.

        filter_width: Bilateral filter size. Default is 6.

        sigma_color: The standard deviation of the gaussian filter
         for smoothing the depth values.

        sigma_space: The standard deviation of the depth discontinuities
         for filtering then. Larger values make sharper images but may reduce
         the depth smoothing.

        depth_scale: Scaling factor for depth values before
         filtering. Default is no scaling.

    """

    depth = ensure_torch(depth)
    if mask is None:
        mask = depth > 0
    else:
        mask = ensure_torch(mask, dtype=torch.bool)

    if depth.is_cuda:
        out_tensor = empty_ensured_size(out_tensor, depth.size(0), depth.size(1),
                                        dtype=depth.dtype, device=depth.device)
        _Processing.bilateral_depth_filter(depth, mask, out_tensor,
                                           filter_width, sigma_color, sigma_space, depth_scale)
    else:
        # OpenCV has a faster implementation.
        out_tensor = torch.from_numpy(cv2.bilateralFilter(
            depth.float().numpy(), filter_width, sigma_color, sigma_space)).int()

    return out_tensor


class BilateralDepthFilter:
    """
    Class for caching the bilateral filter required buffers.
    """

    def __init__(self, filter_width=6, sigma_color=29.9999880000072,
                 sigma_space=4.50000000225, depth_scale=1.0):
        self.filter_width = filter_width
        self.sigma_color = sigma_color
        self.sigma_space = sigma_space
        self.depth_scale = depth_scale

        self._out_tensor = None

    def __call__(self, depth, mask):
        self._out_tensor = empty_ensured_size(self._out_tensor,
                                              depth.size(0), depth.size(1),
                                              device=depth.device,
                                              dtype=depth.dtype)
        _Processing.bilateral_depth_filter(depth, mask, self._out_tensor,
                                           self.filter_width, self.sigma_color,
                                           self.sigma_space, self.depth_scale)
        return self._out_tensor


#############################
# Normals
#############################

def estimate_normals(depth_image: Union[torch.Tensor, np.ndarray],
                     frame_info: FrameInfo, mask: Union[torch.Tensor, np.ndarray],
                     method: EstimateNormalsMethod = EstimateNormalsMethod.CentralDifferences,
                     out_tensor: torch.Tensor = None) -> torch.Tensor:
    """
    Estimate the normals of a depth image.

    Args:
        depth_image: Input depth image [HxW].
        frame_info: The depth frame information to convert into 3D.
        mask: Bool image of valid depth pixels [HxW].
        method: Normal computation method.
        out_tensor (optional): Optional output tensor for this function
         not allocating a new one. It size and type must be [HxWx3] float.
    """
    image_points = depth_image_to_uvz(ensure_torch(depth_image), frame_info.depth_scale,
                                      frame_info.depth_bias)
    xyz_img = frame_info.kcam.backproject(
        image_points.reshape(-1, 3)).reshape(image_points.shape)

    if out_tensor is None:
        out_tensor = torch.empty(xyz_img.size(0), xyz_img.size(1), 3, dtype=xyz_img.dtype,
                                 device=xyz_img.device)

    _Processing.estimate_normals(xyz_img, ensure_torch(mask, dtype=torch.bool),
                                 out_tensor, method)

    return out_tensor

#############################
# Downsample
#############################


def downsample_xyz(xyz_image: torch.Tensor, mask: torch.Tensor, scale: float,
                   dst: torch.Tensor = None,
                   method: DownsampleXYZMethod = DownsampleXYZMethod.Nearest):
    """
    Downsample [HxWx3] float tensors.

    Args:
        xyz_image: Input tensor.
        mask: Bool [HxW] mask of valid positions.
        scale: Downsample scale in the range [0-1].
        dst: Optional output tensor for avoiding allocation of a new output tensor.
         Its size must be [(H*scale)x(W*scale)x3].
        method: Downsampling method selector. Only nearest neighbor is available.

    Returns:
        Downsampled image.
    """
    dst = empty_ensured_size(dst, int(xyz_image.size(0)*scale),
                             int(xyz_image.size(1)*scale),
                             xyz_image.size(2),
                             dtype=xyz_image.dtype,
                             device=xyz_image.device)

    _Processing.downsample_xyz(xyz_image, mask, scale, dst,
                               method)

    return dst


def downsample_mask(mask: torch.Tensor, scale: torch.Tensor, dst=None) -> torch.Tensor:
    """
    Downsample [HxW] masks.

    Args:
        mask: Input tensor.
        scale: Downsample scale in the range [0-1].
        dst: Optional output tensor for avoiding allocation of a new output tensor.
         Its size must be [(H*scale)x(W*scale)x3].

    Returns:
        Downsampled mask.
    """

    dst = empty_ensured_size(dst, int(mask.size(0)*scale),
                             int(mask.size(1)*scale),
                             dtype=mask.dtype,
                             device=mask.device)
    _Processing.downsample_mask(mask, scale, dst)

    return dst


def downsample_features(feature_image: torch.Tensor, scale: float,
                        dst: torch.Tensor = None) -> torch.Tensor:
    """Downsample using nearest neighbors a multi-channel image.

    Args:
        feature_image: A multi-channel image with shape [CxHxW].
        scale: Downsampling scale in the range [0-1]
        dst: Optional output tensor for avoiding allocation of a new output tensor.
         Its size must be [Cx(H*scale)x(W*scale)].
    Returns:
        Downsampled tensor.
    """
    dst = empty_ensured_size(dst,
                             feature_image.size(0),
                             int(feature_image.size(1)*scale),
                             int(feature_image.size(2)*scale),
                             dtype=feature_image.dtype,
                             device=feature_image.device)
    _Processing.downsample_features(feature_image, scale, dst)
    return dst


_gaussian_kernel = 1/16 * torch.tensor([[
    [1.0, 4.0, 6.0, 4.0, 1.0],
    [4.0, 16.0, 24.0, 16.0, 4.0],
    [6.0, 24.0, 36.0, 24.0, 6.0],
    [4.0, 16.0, 24.0, 16.0, 4.0],
    [1.0, 4.0, 6.0, 4.0, 1.0]]])


def feature_pyramid(feature_map: torch.Tensor, scales: Iterable[float]) -> List[torch.Tensor]:
    """Build a gaussian pyramid for featuremaps.

    Args:
        feature_map: Input feature map.
        scales: List of scales from the higher to the lower.
         Values higher than 1.0 are produce feature map with
         at the same current scale.

    Returns:
        Feature map pyramid.
    """

    pyramid = []

    for scale in scales:
        if scale < 1.0:
            feature_map = downsample_features(
                kornia.filters.filter2d(feature_map.unsqueeze(
                    0), _gaussian_kernel, 'replicate').squeeze(0),
                scale)
        pyramid.append(feature_map)

    pyramid.reverse()
    return pyramid

################################
# Erode
################################


def erode_mask(in_mask: torch.Tensor) -> torch.Tensor:
    """
    Erode a mask to filter small non-masked regions

    Args:
        in_mask: Input boolean mask with shape [HxW].

    Returns:
        The mask afther the erode operation.
    """
    out_mask = torch.empty(in_mask.size(), dtype=torch.bool,
                           device=in_mask.device)

    _Processing.erode_mask(in_mask, out_mask)
    return out_mask

################################
# Color
################################


class ColorSpace(Enum):
    """Specify color spaces for preprocessing functions.
    """
    RGB = 0
    GRAY = 1
    INTENSITY = 1
    LAB = 2
    HSV = 3
    RGBA = 4


def to_color_feature(rgb_image: np.ndarray,
                     color_space: ColorSpace = ColorSpace.RGB) -> torch.Tensor:
    """Preprocess and reshape a rgb image into the feature map shape and type.

    Args:
        rgb_image: A [HxWx3] RGB image.
        color_space: Target colorspace, default is RGB.

    Returns:
        Feature map representation with [CxHXW] shape and float type.
    """
    features = rgb_image

    stb2cv2 = {
        ColorSpace.LAB: cv2.COLOR_RGB2LAB,
        ColorSpace.HSV: cv2.COLOR_RGB2HSV,
        ColorSpace.GRAY: cv2.COLOR_RGB2GRAY,
        ColorSpace.INTENSITY: cv2.COLOR_RGB2GRAY
    }

    if color_space != ColorSpace.RGB:
        features = cv2.cvtColor(features, stb2cv2[color_space])

    return to_tensor(features)
