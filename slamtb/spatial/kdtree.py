"""
Encapsulates KDTree searchs with a differentiable layer.
"""

from pykdtree.kdtree import KDTree as _KDTree  # pylint: disable=no-name-in-module
import torch.autograd
import numpy

from slamtb._cslamtb import (
    FeatureKNNInterpolation as _FeatureKNNInterpolation)


class KNNQuery:
    """Spatial 3D KNN query result.

    Attributes:
        target_points: The KNN points, [Nx3].
        source_points: Queued points, [Mx3].
        distances: Queued distances, [MxK]
        index: Queued index, [MxK].
        tree_size: Number of points in the tree.
    """

    def __init__(self, target_points: torch.Tensor, source_points: torch.Tensor,
                 distances: torch.Tensor, index: torch.Tensor, tree_size: int):
        self.target_points = target_points
        self.source_points = source_points
        self.distances = distances
        self.index = index
        self.tree_size = tree_size

    def interpolate_features(self, target_features: torch.Tensor) -> torch.Tensor:
        """Use KNN distances to interpolate a set of features.

        Args:
            target_features: Feature to interpolate,
             shape is [CxN].
        Returns:

        """

        ref_dtype = target_features.dtype
        ref_device = target_features.device

        out_features = torch.empty(target_features.size(0),
                                   self.source_points.size(0),
                                   dtype=ref_dtype, device=ref_device)

        _FeatureKNNInterpolation.forward(self.distances, self.index, target_features,
                                         out_features)
        return out_features

    def dxyz_features(self, target_features: torch.Tensor) -> torch.Tensor:
        """Finite-differences gradients w.r.t. the queued points.

        Args:
            target_features: Features that were
             used to interpolate, shape is [CxN].
        """
        ref_dtype = target_features.dtype
        ref_device = target_features.device

        epsilon_distances = torch.empty(self.index.size(0), 6,
                                        self.index.size(1),
                                        dtype=ref_dtype,
                                        device=ref_device)

        _FeatureKNNInterpolation.compute_epsilon_distances(
            self.target_points, self.source_points,
            self.index, epsilon_distances)

        dl_xyz = torch.empty(self.index.size(0), target_features.size(0), 3,
                             dtype=ref_dtype,
                             device=ref_device)

        _FeatureKNNInterpolation.compute_dxyz_features(
            epsilon_distances, self.index,
            target_features, dl_xyz)

        return dl_xyz

    @property
    def valid_mask(self) -> torch.Tensor:
        """
        Returns the query elements which have a found corresponding point.
        """
        return self.index[:, 0] < self.tree_size


class KDTree:
    """
    KDTree wrapper over PyKDTree for querying 3D points.
    """

    def __init__(self, target_points: torch.Tensor):
        """
        Args:
            target_points:  Points
             that should be indexed for querying
        """
        self.tree = _KDTree(target_points.cpu().numpy(), leafsize=32)
        self.target_points = target_points

    def query(self, source_points: torch.Tensor, k: int = 1,
              distance_upper_bound: float = numpy.inf) -> KNNQuery:
        """
        Runs a KNN query.

        Args:
            source_points: Query points.
            k: Number of points to retrieve.
            distance_upper_bound: Maximum spatial point distances.

        Returns:
            Query object.
        """
        ref_device = source_points.device
        ref_dtype = source_points.dtype

        distance, index = self.tree.query(
            source_points.detach().cpu().numpy(), k=k,
            distance_upper_bound=distance_upper_bound,
            eps=0.5, sqr_dists=True)

        distance = torch.from_numpy(distance).to(
            ref_device).to(ref_dtype).view(-1, k)
        index = torch.from_numpy(index.astype(
            numpy.int64)).to(ref_device).view(-1, k)

        return KNNQuery(self.target_points, source_points,
                        distance, index, self.tree.n)
