"""
Module for establishing matches between 3D points, either spatially or in a grid.
"""

from typing import Union
import torch

from slamtb._cslamtb import (
    CorrespondenceMatching as _CorrespondenceMatching)

from slamtb._utils import empty_ensured_size

_CORRESP_INF = 0x0fffffff7f800000


def make_indexmap(height: int, width: int, device: Union[str, torch.device] = None) -> torch.Tensor:
    """
    Indexmaps are 2D tensors that stores in each cell a index to a neighbor generate by matching
    3D points that can be layout as a grid, like for example, finding by 3D point projection.

    This function will create one with empty index values.

    Args:
        height: Map's height.
        width: Map's width.
        device: Which device the map should be allocated.

    Returns:
        A [HxW] tensor of type int64.
    """
    return torch.full((height, width), _CORRESP_INF, dtype=torch.int64,
                      device=device)


def indexmap_to_correspondences(indexmap: torch.Tensor) -> torch.Tensor:
    """
    Converts an indexmap into a list of correspondences.

    Args:
        indexmap: A [HxW] map of int64.

    Returns:
        A [Cx2] int64 tensor where C is the number of correspondences and each row has the
         is a corresponding target and source indices, in that order.
    """
    correspondences = torch.empty(
        indexmap.size(0), indexmap.size(1), 2,
        dtype=torch.int64,
        device=indexmap.device)

    selection_mask = torch.empty(indexmap.size(0), indexmap.size(1),
                                 dtype=torch.bool,
                                 device=indexmap.device)
    _CorrespondenceMatching.convert_indexmap_to_correspondences(
        indexmap, correspondences, selection_mask)

    return correspondences[selection_mask]


class MatchByProjection:
    """Generates correspondences between points to an image
    using pinhole camera projection.

    This class will reuse previous allocated Tensors.

    Attributes:
        distance_threshold: The maximum 3D distance to make a match.
        normals_angle_thresh: The maximum angle between point's normal to make a match.
    """

    def __init__(self, distance_threshold: float, normals_angle_thresh: float):
        self.distance_threshold = distance_threshold
        self.normals_angle_thresh = normals_angle_thresh

        self._correspondences = None
        self._selection_mask = None

    def match(self, source_points: torch.Tensor, source_normals: torch.Tensor,
              transform: torch.Tensor, target_points: torch.Tensor, target_normals: torch.Tensor,
              target_mask: torch.Tensor, kcam: torch.Tensor) -> torch.Tensor:
        """Match source points by projecting it on the target's image.

        Args:
            source_points: Source point-cloud points. Tensor with shape [Nx3].

            source_normals: Source point-cloud normals. Tensor with shape [Nx3]. The normals are
             checked to have angle less than the attribute `normals_angle_thresh`.
            transform: Rigid transformation. Tensor with shape [4x4] or [3x4] on cpu.
            target_points: Target point-cloud points. Tensor with shape [HxWx3].
            target_normals: Target point-cloud normals. Tensor with shape [HxWx3].
            target_mask: Target point-cloud positions. Bool tensor with shape [HxW].
            kcam: Intrinsic camera matrix. Tensor with shape [3x3] or [2x3].

        Returns:
            Target to source correspondences. Each row is a tuple with the destination index
             (linearized) and  the source index. Format is [Mx2] of type int64, M is the number of
             valid correspondences.
        """
        device = source_points.device
        indexmap = make_indexmap(target_points.size(0), target_points.size(1),
                                 device=device)

        _CorrespondenceMatching.match_by_projection(
            source_points.view(-1, 3), source_normals.view(-1, 3),
            transform,
            target_points, target_normals, target_mask,
            kcam, indexmap, float(self.distance_threshold),
            float(self.normals_angle_thresh))

        self._correspondences = empty_ensured_size(
            self._correspondences,
            indexmap.size(0), indexmap.size(1), 2,
            dtype=torch.int64,
            device=indexmap.device)

        self._selection_mask = empty_ensured_size(
            self._selection_mask,
            indexmap.size(0), indexmap.size(1),
            dtype=torch.bool,
            device=indexmap.device)
        _CorrespondenceMatching.convert_indexmap_to_correspondences(
            indexmap, self._correspondences, self._selection_mask)

        return self._correspondences[self._selection_mask]


class MatchKNNNormals:
    """
    Refines the correspondences from the results of K-nearest neighbors query result by verying
     the points' normals.

    Attributes:
        normals_angle_thresh: The maximum angle between point's normal to make a match.
    """

    def __init__(self, normals_angle_thresh: float):
        self.normals_angle_thresh = normals_angle_thresh

    def match(self, knn_distances: torch.Tensor, knn_index: torch.Tensor,
              source_normals: torch.Tensor, target_normals: torch.Tensor,
              transform: torch.Tensor) -> torch.Tensor:
        """
        Filters the correspondences with incompatible normals in nearest distance query.
        The normals are checked to have angle less than the attribute `normals_angle_thresh`.

        Args:
            knn_distances: A tensor [NxK] with the distances from N nearest neighbor queries with K
             results each.
            knn_index: A tensor [NxK] with the indices from N nearest neighbor queries with K
             results each.
            source_normals: Source point-cloud normals. Tensor with shape [Bx3].
            target_normals: Target point-cloud normals. Tensor with shape [Ax3].
            transform: Rigid transformation. Tensor with shape [4x4] or [3x4] on cpu.

        Returns:
            Target to source correspondences. Each row is a tuple with the destination index
             (linearized) and  the source index. Format is [Mx2] of type int64, M is the number of
             valid correspondences.
        """
        device = source_normals.device
        indexmap = make_indexmap(target_normals.size(0), 1, device=device)

        _CorrespondenceMatching.match_knn_normals(
            knn_distances, knn_index,
            source_normals, transform.float(), target_normals,
            indexmap, self.normals_angle_thresh)
        return indexmap_to_correspondences(indexmap)
