# pylint: disable=unused-import
"""
Implement an Octree data structure for storing triangle meshes.
"""
from slamtb._cslamtb import TrigOctree
