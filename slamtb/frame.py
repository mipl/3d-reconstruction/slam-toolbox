"""Frame related data structures.
"""

from typing import Dict, Optional, Union
import torch
import cv2
import numpy as np

from .camera import KCamera, RTCamera


class FrameInfo:
    """A common description for frames. Holds information about camera
    parameters, depth scaling, and time.

    Attributes:

        kcam: The intrinsic camera information.
        depth_scale: Scaling factor for raw depth values directly from files.
        depth_bias: Constant add to the raw depth values.
        depth_max: Sensor maximum depth value.
        rt_cam: The extrinsic camera.
        timestamp: The frame timestamp.
    """

    def __init__(self, kcam: KCamera = None, depth_scale: float = 1.0, depth_bias: float = 0.0,
                 depth_max: float = 4500.0, timestamp: Union[float, int] = None,
                 rgb_kcam: KCamera = None, rt_cam: RTCamera = None):
        self.kcam = kcam
        self.depth_scale = depth_scale
        self.depth_bias = depth_bias
        self.depth_max = depth_max

        self.rgb_kcam = rgb_kcam
        self.rt_cam = rt_cam
        self.timestamp = timestamp

    @classmethod
    def from_json(cls, json: Dict):
        """Creates a from FTB's JSON representation.
        Args:
            json: Input JSON dict.
        """

        kcam = json.get('kcam', None)
        if kcam is not None:
            kcam = KCamera.from_json(kcam)

        rgb_kcam = json.get('rgb_kcam', None)
        if rgb_kcam is not None:
            rgb_kcam = KCamera.from_json(rgb_kcam)

        rt_cam = json.get('rt_cam', None)
        if rt_cam is not None:
            rt_cam = RTCamera.from_json(rt_cam)

        return cls(kcam, json.get('depth_scale', 1.0),
                   json.get('depth_bias', 0.0),
                   json.get('depth_max', 4500.0),
                   json.get('timestamp', None),
                   rgb_kcam, rt_cam)

    def to_json(self) -> Dict:
        """Converts the frame info to its FTB JSON dictionary representation.

        Returns:
            JSON-compatible dictionary.

        """
        json = {}
        for name, value in vars(self).items():
            if value is None:
                continue
            if hasattr(value, 'to_json'):
                value = value.to_json()

            json[name] = value

        return json

    def clone(self):
        """Creates a copy of the instance.

        Returns:
            New copy.
        """

        return FrameInfo(
            self.kcam.clone(), self.depth_scale, self.depth_bias,
            self.depth_max, self.timestamp,
            None if self.rgb_kcam is None else self.rgb_kcam.clone(),
            None if self.rt_cam is None else self.rt_cam.clone())

    def __str__(self):
        return ("Intrinsic: {{{self.kcam}}}"
                ", depth scale: {self.depth_scale}"
                ", depth bias: {self.depth_bias}"
                ", timestamp: {self.timestamp}"
                ", Rigid Transformation: {{{self.rt_cam}}}").format(self=self)

    def __repr__(self):
        return str(vars(self))


class Frame:
    """A RGB-D frame, either outputted from a sensor or dataset.

    Attributes:

        info: Information about depth and camera parameters.
        depth_image: Depth image of size (H x W) and int32 type.
        rgb_image: RGB image of size (H x W x 3) and uint8 type.
        seg_image: Segmentation image of size (H x W) and int16 type. Value 0 means background.
        normal_image: Image containing per point normal vectors.
         Size is (H x W x 3) with float type.
    """

    def __init__(self, info: FrameInfo, depth_image: np.ndarray,
                 rgb_image: Optional[np.ndarray] = None, seg_image: Optional[np.ndarray] = None,
                 normal_image: Optional[np.ndarray] = None):
        self.info = info
        self.info.kcam.image_size = (depth_image.shape[1],
                                     depth_image.shape[0])
        self.depth_image = depth_image
        self.rgb_image = rgb_image
        self.seg_image = seg_image
        self.normal_image = normal_image
        self._mask = None

    def clone(self, shallow: bool = False):
        """Copy the instance.

        Args:
            shallow: If `False`, then the images are also cloned. Otherwise,
             it'll just create another instance of the frame.

        Returns:
            New copy.
        """

        def _clone(tensor):
            if torch.is_tensor(tensor):
                return tensor.cpu().clone().numpy()
            return tensor.copy()

        if not shallow:
            return Frame(self.info.clone(), self.depth_image.copy(),
                         None if self.rgb_image is None else self.rgb_image.copy(),
                         None if self.seg_image is None else self.seg_image.copy(),
                         None if self.normal_image is None else _clone(self.normal_image))

        return Frame(self.info, self.depth_image,
                     self.rgb_image, self.seg_image, self.normal_image)

    def scaled(self, xscale: float, yscale: float = None,
               interpolation=cv2.INTER_LINEAR):
        """
        Creates a scaled version of this frame.

        Args:
            xscale: The scaling factor in the horizontal direction.
            yscale: The scaling factor in the vertical direction.

        Returns:
            A new scaled frame.
        """
        if yscale is None:
            yscale = xscale

        rgb_image = None
        if self.rgb_image is not None:
            rgb_image = cv2.resize(self.rgb_image, (0, 0),
                                   None, xscale, yscale, interpolation)

        depth_image = cv2.resize(
            self.depth_image.astype(np.uint16), (0, 0), None, xscale, yscale,
            interpolation).astype(np.int32)

        seg_image = None
        if self.seg_image is not None:
            seg_image = cv2.resize(self.seg_image, (0, 0),
                                   None, xscale, yscale, cv2.INTER_NEAREST)

        normal_image = None
        if self.normal_image is not None:
            normal_image = cv2.resize(
                self.normal_image, (0, 0), None, xscale, yscale, cv2.INTER_LINEAR)

        info = FrameInfo(
            kcam=self.info.kcam.scaled(xscale, yscale),
            depth_scale=self.info.depth_scale,
            depth_bias=self.info.depth_bias,
            depth_max=self.info.depth_max,
            timestamp=self.info.timestamp,
            rgb_kcam=(self.info.rgb_kcam.scaled(xscale, yscale)
                      if self.info.rgb_kcam is not None else None),
            rt_cam=self.info.rt_cam)

        return Frame(info, depth_image, rgb_image, seg_image, normal_image)

    def __str__(self):
        has_depth = self.depth_image is not None
        has_rgb = self.rgb_image is not None
        has_seg = self.seg_image is not None
        has_normal = self.normal_image is not None

        return (
            "Frame with shape ({self.depth_image.shape[0]}x{self.depth_image.shape[1]})"
            " depth: {has_depth}, RGB: {has_rgb}, segmentation: {has_seg}, Normal: {has_normal}"
        ).format(self=self, has_depth=has_depth, has_rgb=has_rgb,
                 has_seg=has_seg, has_normal=has_normal)

    @property
    def mask(self) -> np.ndarray:
        """
        Compute the frame mask from the depth image, that is, where the depth image is nonzero.
        The mask is cached in the instance.
        """
        if self._mask is None:
            self._mask = self.depth_image > 0
        return self._mask

    def dirty_mask(self):
        """
        Discard the mask cache.
        """
        self._mask = None

    def clamp_max_depth(self, max_value: float):
        """
        Transform the depth values larger than `max_value` to 0,
        making then not part of the mask.  This operation will dirty
        the mask.

        Args:

            max_value: Maximum value that the depth image can have.
             This value is in the space after scaling by self.info.depth_scale.
        """
        max_depth = max_value // self.info.depth_scale
        self.depth_image[self.depth_image > max_depth] = 0
        self.dirty_mask()

    def bilateral_depth_filter(self, filter_size: int = 6,
                               sigma_color: float = 29.9999880000072,
                               sigma_space: float = 4.50000000225):
        """
        Helper function for directly applying the bilateral filter
        on this frame's depth image.
        """
        # pylint: disable=import-outside-toplevel
        from slamtb.processing import bilateral_depth_filter
        self.depth_image = bilateral_depth_filter(
            self.depth_image, self.mask, filter_width=filter_size,
            sigma_color=sigma_color, sigma_space=sigma_space)

    def __repr__(self):
        return str(self)
