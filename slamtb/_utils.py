"""
Intern utilities.
"""
import contextlib
import cProfile

import numpy as np
import torch


def depth_image_to_uvz(depth_image, depth_scale, depth_bias):
    """Converts an depth image to a mesh grid of u (columns), v (rows) an z
     coordinates.

    Args:

        depth_image (:obj:`torch.Tensor`): [WxH] depth image.

        finfo (:obj:`FrameInfo`): The source frame description.

    Returns:
        (:obj:`torch.Tensor`): [WxHx3] the depth image with the u
         and v pixel coordinates.

    """

    depth_image = (depth_image.float()*depth_scale + depth_bias)
    device = depth_image.device
    dtype = depth_image.dtype
    ys, xs = torch.meshgrid(torch.arange(depth_image.size(0), dtype=dtype),
                            torch.arange(depth_image.size(1), dtype=dtype))

    image_points = torch.stack(
        [xs.to(device), ys.to(device), depth_image], 2)

    return image_points


def ensure_torch(x, dtype=None):
    """Ensure that x is torch tensor. If x is a torch tensor, it's returned as it is.

    Args:
        x (:obj:`numpy.ndarray`, list, :obj:`torch.Tensor`): Any array object or torch's tensor.

        dtype (:obj:`torch.dtype`, optional): Target type.

    Returns:

    """
    if x is None:
        return None
    if isinstance(x, (np.ndarray)):
        x = torch.from_numpy(x)
        if dtype is not None:
            x = x.type(dtype)
    elif isinstance(x, (list, tuple)):
        x = torch.tensor(x, dtype=dtype)
    else:
        if dtype is None:
            x = x.to(dtype)
    return x


def ensure_numpy(x, dtype=None):
    """Ensure that x is a numpy array. If x is already a numpy array, it's returned as it is.

    Args:
        x (:obj:`numpy.ndarray`, list, :obj:`torch.Tensor`): Any arrayable object.

        dtype (:obj:`numpy.dtype`, optional): Target type.

    Returns: (:obj:`numpy.ndarray`)
        Numpy array version of the object.
    """
    if x is None:
        return None

    if isinstance(x, (torch.Tensor)):
        x = x.numpy()
        if dtype is not None:
            x = x.astype(dtype)
        return x

    if isinstance(x, (list, tuple)):
        return np.array(x, dtype=dtype)

    if dtype is None:
        x = x.astype(dtype)

    return x


def empty_ensured_size(tensor, *sizes, dtype=torch.float, device=None):
    """Returns an empty allocated tensor if the input tensor is None or
    if its sizes are different than input.

    """

    if tensor is None or tensor.size() != sizes:
        return torch.empty(sizes, dtype=dtype, device=device)

    return tensor


def zero_ensured_size(tensor, *sizes, dtype=torch.float, device=None):
    """Returns an zero allocated tensor if the input tensor is None or
    if its sizes are different than input.

    """

    if tensor is None or tensor.size() != sizes:
        return torch.zeros(sizes, dtype=dtype, device=device)

    return tensor


@contextlib.contextmanager
def profile(output_file, really=True):
    """
    Context manager for cProfilling a scope.
    Args:
        output_file (str): Where to output the .cprof file.
        really (bool): Conditional profile.
    """
    if really:
        prof = cProfile.Profile()
        try:
            prof.enable()
            yield
        finally:
            prof.disable()
            prof.dump_stats(str(output_file))
    else:
        yield


class NoneCallProxy:
    """
    Proxy to fake call functions of None objects.
    If `obj` is not None, then the actual method is returned,
    else returns a method that always returns None.
    """

    def __init__(self, obj):
        self.obj = obj

    def __getattribute__(self, name):
        obj = super().__getattribute__('obj')
        if obj is not None:
            return obj.__getattribute__(name)

        def method(*_, **__):
            return None

        return method

    def __getitem__(self, *args):
        obj = super().__getattribute__('obj')
        if obj is not None:
            return obj.__getitem__(*args)

        return None
