#!/usr/bin/env python

from pathlib import Path
import math
import unittest
import sys

from fire import Fire
import torch


from slamtb.data.tumrgbd import read_trajectory
from slamtb.viz.posegraphviewer import PoseGraphViewer
from slamtb.viz.trajectoryviewer import TrajectoryViewer
from slamtb.camera import RigidTransform
from slamtb.posegraph.builder import pose_graph_from_trajectory, PoseGraphBuilder
from slamtb.posegraph import PoseGraphNode
from slamtb.trajectory import Trajectory
from slamtb.metrics.posegraph import (edges_rotational_rmse,
                                      edges_translational_rmse,
                                      absolute_trajectory_error)


def _load_trajectories():
    traj_root = Path(__file__).parent / "../../../test-data/trajectory/"

    gt_traj = read_trajectory(
        traj_root / "freiburd1_desk_gt.traj")
    pred_traj = read_trajectory(
        traj_root / "freiburd1_desk_pred.traj")
    return Trajectory.from_dict(gt_traj), Trajectory.from_dict(pred_traj)


def large_trajectory(show=None):
    """
    Visual test with a large trajectory.

    show (str): Options are "pred" and "gt".
    """
    gt_traj, pred_traj = _load_trajectories()
    gt_pose_graph = pose_graph_from_trajectory(gt_traj)
    pred_pose_graph = pose_graph_from_trajectory(pred_traj)

    #pose_graph = pose_graph_from_trajectory(gt_traj)
    #PoseGraphViewer(pose_graph, kcam_far=0.005).run()

    print("Rotation: ", edges_rotational_rmse(gt_pose_graph, pred_pose_graph))
    print("Translation: ", edges_translational_rmse(
        gt_pose_graph, pred_pose_graph))
    print("Absolute trajectory error: ", absolute_trajectory_error(
        gt_pose_graph, pred_pose_graph))

    if show == "gt":
        PoseGraphViewer(gt_pose_graph, title="Ground truth").run()
    elif show == "pred":
        PoseGraphViewer(pred_pose_graph, title="Predicted").run()


def _make_trajectory(is_ground_truth=True):
    info1 = torch.eye(6)*5000
    info2 = torch.eye(6)*9000
    pg_builder = PoseGraphBuilder(PoseGraphNode(0, torch.eye(4)))
    pg_builder.add(0, 1, RigidTransform.from_euler_angles(
        [0, math.radians(0), 0], [0, 0, 1], dtype=torch.float64).matrix, info1)

    if is_ground_truth:
        pg_builder.add(1, 2, RigidTransform.from_euler_angles(
            [0, math.radians(45), 0], [0, 0, 1], dtype=torch.float64).matrix, info1)
    else:
        pg_builder.add(1, 2, RigidTransform.from_euler_angles(
            [0, math.radians(65), 0], [0, 0, 1], dtype=torch.float64).matrix, info1)

    pg_builder.add(2, 3, RigidTransform.from_euler_angles(
        [0, math.radians(30), 0], [0, 0, 1], dtype=torch.float64).matrix, info1)
    pg_builder.add(3, 4, RigidTransform.from_euler_angles(
        [0, math.radians(30), 0], [0, 0, 1], dtype=torch.float64).matrix, info1)
    pg_builder.add(4, 5, RigidTransform.from_euler_angles(
        [0, math.radians(60), 0], [0, 0, 1], dtype=torch.float64).matrix, info1)
    pg_builder.add(5, 6, RigidTransform.from_euler_angles(
        [0, math.radians(30), 0], [0, 0, 1], dtype=torch.float64).matrix, info1)
    pg_builder.add(6, 7, RigidTransform.from_euler_angles(
        [0, math.radians(60), 0], [0, 0, 1], dtype=torch.float64).matrix, info1)
    pg_builder.add(7, 0, RigidTransform.from_euler_angles(
        [0, math.radians(30), 0], [0, 0, 1], dtype=torch.float64).matrix, info2, uncertain=False)

    return pg_builder.pose_graph


def small_trajectory(show=None):
    """
    Visual test with a small trajectory.

    show (str): Options are "pred" and "gt".
    """
    gt_pose_graph = _make_trajectory(is_ground_truth=True)
    pred_pose_graph = _make_trajectory(is_ground_truth=False)

    print("Rotation: ", edges_rotational_rmse(gt_pose_graph, pred_pose_graph))
    print("Translation: ", edges_translational_rmse(
        gt_pose_graph, pred_pose_graph))
    print("Absolute trajectory error: ", absolute_trajectory_error(
        gt_pose_graph, pred_pose_graph))

    view_matrix = torch.tensor(
        [[-0.883314, -1.49012e-08, 0.468781, 0.587544],
         [0.382468, 0.578224, 0.720677, -1.32452],
         [-0.27106, 0.815878, -0.510754, -4.30711],
         [0, 0, 0, 1]])

    if show == "gt":
        PoseGraphViewer(gt_pose_graph, title="Ground truth").run(
            view_matrix=view_matrix)
    elif show == "pred":
        PoseGraphViewer(pred_pose_graph, title="Predicted").run(
            view_matrix=view_matrix)


class TestPoseGraphMetrics(unittest.TestCase):
    def setUp(self):
        gt_traj, pred_traj = _load_trajectories()
        self.gt_pg = pose_graph_from_trajectory(gt_traj)
        self.pred_pg = pose_graph_from_trajectory(pred_traj)

    def test_gt_equivalance(self):
        rot_rmse = edges_rotational_rmse(self.gt_pg, self.gt_pg)
        trans_rmse = edges_translational_rmse(self.gt_pg, self.gt_pg)
        ate = absolute_trajectory_error(self.gt_pg, self.gt_pg)

        self.assertAlmostEqual(rot_rmse, 0.0, places=5)
        self.assertAlmostEqual(trans_rmse, 0.0, places=5)
        self.assertAlmostEqual(ate, 0.0, places=5)


def run_unit_test1():
    suite = unittest.TestSuite()
    suite.addTest(TestPoseGraphMetrics())
    runner = unittest.TextTestRunner()
    runner.run(suite)


if __name__ == '__main__':
    Fire({
        "small-traj": small_trajectory,
        "large-traj": large_trajectory,
        "unit": lambda: unittest.main(argv=sys.argv[:1])
    })
