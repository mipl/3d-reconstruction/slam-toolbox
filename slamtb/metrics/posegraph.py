"""
Metrics for evaluating pose graphs.
"""

import math
from statistics import mean

import torch

from slamtb.camera import RigidTransform

from .transformation import (translational_distance,
                             rotational_distance)


def align_pose_graph_trajectories(source_pg, target_pg):
    xyz1 = torch.tensor([node.as_camera().center
                         for node in source_pg.values()],
                        dtype=torch.float64)
    xyz2 = torch.tensor([node.as_camera().center
                         for node in target_pg.values()],
                        dtype=torch.float64)

    count = min(xyz1.size(0), xyz2.size(0))
    xyz1 = xyz1[:count, :]
    xyz2 = xyz2[:count, :]

    return RigidTransform.fit_points(xyz1, xyz2).matrix


def edges_translational_rmse(gt_pose_graph, pred_pose_graph, align_matrix=None,
                             only_uncertain_edges=False):
    trans_errors = []
    if align_matrix is None:
        align_matrix = torch.eye(4, dtype=torch.double)

    for pred_i, pred_node in pred_pose_graph.items():
        if pred_i not in gt_pose_graph:
            continue

        gt_node = gt_pose_graph[pred_i]

        for pred_j, pred_edge in pred_node.edges.items():
            if pred_j not in gt_node.edges:
                continue

            if only_uncertain_edges and not pred_edge.uncertain:
                continue

            pred_transform = pred_edge.transform
            gt_transform = gt_node.edges[pred_j].transform

            trans_errors.append(translational_distance(
                gt_transform @ pred_transform.inverse())**2.0)
    return math.sqrt(mean(trans_errors))


def edges_rotational_rmse(gt_pose_graph, pred_pose_graph, align_matrix=None,
                          only_uncertain_edges=False):
    rot_errors = []

    if align_matrix is None:
        align_matrix = torch.eye(4, dtype=torch.double)

    for pred_i, pred_node in pred_pose_graph.items():
        if pred_i not in gt_pose_graph:
            continue

        gt_node = gt_pose_graph[pred_i]

        for pred_j, pred_edge in pred_node.edges.items():
            if pred_j not in gt_node.edges:
                continue

            if only_uncertain_edges and not pred_edge.uncertain:
                continue

            pred_transform = pred_edge.transform
            gt_transform = gt_node.edges[pred_j].transform
            rot_err = math.degrees(
                rotational_distance(gt_transform @ pred_transform.inverse()))
            rot_errors.append(rot_err**2.0)

    return math.sqrt(mean(rot_errors))
