"""Metrics for evaluating camera trajectories.
"""

import torch

from slamtb.camera import RTCamera
from slamtb.trajectory import Trajectory

from .transformation import (translational_distance,
                             rotational_distance)


def _ensure_matrix(cam_or_matrix):
    if isinstance(cam_or_matrix, RTCamera):
        return cam_or_matrix.matrix

    return cam_or_matrix.double()


def _get_absolute_residual(matrix_true, matrix_pred):
    """Computes the abosulute resitual transformation of a ground truth
    pose and a prediction.

    Args:

        matrix_true (:obj:`torch.Tensor`): Ground truth pose
         matrix. Must be invertible.

        matrix_pred (:obj:`torch.Tensor`): Predicted pose matrix.

    Returns: (:obj:`torch.Tensor`):
        Residual transformation matrix.

    """

    return matrix_true.inverse() @ matrix_pred


def absolute_translational_error(trajectory_true: Trajectory,
                                 trajectory_pred: Trajectory) -> torch.Tensor:
    """Computes the per trajectory absolute translational error as defined
    in Sturm, Jürgen, Nikolas Engelhard, Felix Endres, Wolfram
    Burgard, and Daniel Cremers. "A benchmark for the evaluation of
    RGB-D SLAM systems." In 2012 IEEE/RSJ International Conference on
    Intelligent Robots and Systems, pp. 573-580. IEEE, 2012.

    Args:

        trajectory_true: The ground truth trajectory.

        trajectory_pred: The predicted ground truth trajectory.

    Returns: (obj:`torch.Tensor`): Per pose absolute translation
     error. Use `.mean().sqrt()` for having the RMSE

    """

    traj_errors = []
    for i, (_, cam_true) in enumerate(trajectory_true):
        if i >= len(trajectory_pred):
            break
        cam_pred = _ensure_matrix(trajectory_pred[i].camera.matrix)
        cam_true = _ensure_matrix(cam_true.matrix)

        diff = translational_distance(
            _get_absolute_residual(cam_true, cam_pred))
        traj_errors.append(diff**2)

    return torch.tensor(traj_errors)


def absolute_rotational_error(trajectory_true: Trajectory,
                              trajectory_pred: Trajectory) -> torch.Tensor:
    """Computes the per trajectory absolute rotational error as defined
    in Sturm, Jürgen, Nikolas Engelhard, Felix Endres, Wolfram
    Burgard, and Daniel Cremers. "A benchmark for the evaluation of
    RGB-D SLAM systems." In 2012 IEEE/RSJ International Conference on
    Intelligent Robots and Systems, pp. 573-580. IEEE, 2012.

    Args:

        trajectory_true: The ground truth trajectory.

        trajectory_pred: The predicted ground truth trajectory.

    Returns: (obj:`torch.Tensor`): Per pose absolute rotational
     error. Use `.mean().sqrt()` for having the RMSE

    """

    rot_errors = []
    for k, cam_true in trajectory_true.items():
        cam_pred = trajectory_pred[k]
        cam_pred = _ensure_matrix(trajectory_pred[k])
        cam_true = _ensure_matrix(cam_true)

        diff = rotational_distance(_get_absolute_residual(
            cam_true.inverse(), cam_pred))
        rot_errors.append(diff**2)

    return torch.tensor(rot_errors)


def get_relative_residual(matrix_i_true, matrix_j_true,
                          matrix_i_pred, matrix_j_pred):
    """Computes the residual transformation relative to absolute
    poses. Its output can be used with
    :func:`translational_distance` and :func:`rotational_distance`.

    Definition in Sturm, Jürgen, Nikolas Engelhard, Felix Endres, Wolfram
    Burgard, and Daniel Cremers. "A benchmark for the evaluation of
    RGB-D SLAM systems." In 2012 IEEE/RSJ International Conference on
    Intelligent Robots and Systems, pp. 573-580. IEEE, 2012.

    Args:

        matrix_i_true (:obj:`torch.Tensor`): Ground truth matrix of
         the first frame. Must be invertible.

        matrix_j_true (:obj:`torch.Tensor`): Ground truth matrix of
         the second frame. Must be invertible.

        matrix_i_pred (:obj:`torch.Tensor`): Predicted matrix of the
         first frame. Must be invertible.

        matrix_j_pred (:obj:`torch.Tensor`): Predicted matrix of the
         second frame.

    Returns: (:obj:`torch.Tensor`): The residual matrix.

    """
    return ((matrix_i_true.inverse() @ matrix_j_true).inverse()
            @ (matrix_i_pred.inverse() @ matrix_j_pred))


def _get_relative_matrices(trajectory_true: Trajectory,
                           trajectory_pred: Trajectory):
    keys = list(range(min(len(trajectory_true), len(trajectory_pred))))
    diff_matrices = []

    for k in range(len(keys) - 1):
        i = keys[k]
        j = keys[k + 1]

        cam_true_i = _ensure_matrix(trajectory_true[i].camera.matrix)
        cam_pred_i = _ensure_matrix(trajectory_pred[i].camera.matrix)

        cam_true_j = _ensure_matrix(trajectory_true[j].camera.matrix)
        cam_pred_j = _ensure_matrix(trajectory_pred[j].camera.matrix)

        diff_matrix = get_relative_residual(cam_true_i, cam_true_j,
                                            cam_pred_i, cam_pred_j)
        diff_matrices.append(diff_matrix)

    return diff_matrices


def relative_translational_error(trajectory_true: Trajectory,
                                 trajectory_pred: Trajectory):
    """Computes the per trajectory relative translational error as defined
    in Sturm, Jürgen, Nikolas Engelhard, Felix Endres, Wolfram
    Burgard, and Daniel Cremers. "A benchmark for the evaluation of
    RGB-D SLAM systems." In 2012 IEEE/RSJ International Conference on
    Intelligent Robots and Systems, pp. 573-580. IEEE, 2012.

    Args:

        trajectory_true (Dict[float: :obj:`slamtb.camera.RTCamera`]):
         The ground truth trajectory.

        trajectory_pred (Dict[float: :obj:`slamtb.camera.RTCamera`]):
         The predicted ground truth trajectory.

    Returns: (obj:`torch.Tensor`): Per pose relative translational
     error. Use `.mean().sqrt()` for having the RMSE

    """

    return torch.tensor(
        [translational_distance(diff_matrix)**2
         for diff_matrix in _get_relative_matrices(
            trajectory_true, trajectory_pred)])


def relative_rotational_error(trajectory_true, trajectory_pred):
    """Computes the per trajectory relative rotational error as defined
    in Sturm, Jürgen, Nikolas Engelhard, Felix Endres, Wolfram
    Burgard, and Daniel Cremers. "A benchmark for the evaluation of
    RGB-D SLAM systems." In 2012 IEEE/RSJ International Conference on
    Intelligent Robots and Systems, pp. 573-580. IEEE, 2012.

    Args:

        trajectory_true (Dict[float: :obj:`slamtb.camera.RTCamera`]):
         The ground truth trajectory.

        trajectory_pred (Dict[float: :obj:`slamtb.camera.RTCamera`]):
         The predicted ground truth trajectory.

    Returns: (obj:`torch.Tensor`): Per pose relative rotational
     error. Use `.mean().sqrt()` for having the RMSE

    """
    return torch.tensor(
        [rotational_distance(diff_matrix)**2
         for diff_matrix in _get_relative_matrices(
            trajectory_true, trajectory_pred)])
