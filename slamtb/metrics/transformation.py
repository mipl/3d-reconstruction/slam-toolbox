"""
Metrics for comparing transformations.
"""

import math
import torch


def translational_distance(diff_matrix):
    """Returns the translation from a pose difference matrix.

Args:

        diff_matrix (obj:`torch.Tensor`): [3x4] or [4x4] matrix.
    """
    return (diff_matrix[:3, 3]).norm(2).item()


def rotational_distance(diff_matrix):
    """Returns the rotation angle from a pose difference matrix.

    Args:

        diff_matrix (obj:`torch.Tensor`): [3x4] or [4x4] matrix.

    Returns: (float): Rotation angle in radians.
    """
    return math.acos(min(1.0, max(-1.0, (torch.trace(diff_matrix[0:3, 0:3]) - 1.0)/2.0)))
