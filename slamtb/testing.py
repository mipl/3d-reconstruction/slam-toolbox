"""General testing facilitators for testing.
"""

from pathlib import Path
import os
from typing import Tuple

import cv2
import numpy as np
import torch

from slamtb.processing import (estimate_normals, bilateral_depth_filter,
                               ColorSpace, to_color_feature)
from slamtb.data.ftb import FTBDataset, load_ftb


def preprocess_frame(frame, scale=1, filter_depth=True, color_space=ColorSpace.LAB,
                     blur=False, compute_normals=False, max_depth=None):
    """Preprocess a frame and extract color features.
    """

    height, width = frame.depth_image.shape
    height, width = int(height*scale), int(width*scale)

    if scale < 1:
        frame.depth_image = cv2.resize(frame.depth_image.astype(
            np.uint16), (width, height)).astype(np.int32)
        frame.rgb_image = cv2.resize(frame.rgb_image,
                                     (width, height))
        frame.info.kcam = frame.info.kcam.scaled(scale)

    if max_depth is None:
        mask = frame.depth_image > 0
    else:
        max_depth = int(max_depth / frame.info.depth_scale)
        mask = (frame.depth_image > 0) & (frame.depth_image <= max_depth)

    if filter_depth:
        frame.depth_image = bilateral_depth_filter(
            frame.depth_image,
            mask).numpy()

    if compute_normals:
        normal_depth_image = frame.depth_image
        if not filter_depth:
            normal_depth_image = bilateral_depth_filter(
                torch.from_numpy(frame.depth_image).to("cuda:0"),
                torch.from_numpy(mask).to("cuda:0")).cpu().numpy()

        frame.normal_image = estimate_normals(normal_depth_image, frame.info,
                                              mask).cpu().numpy()

    features = to_color_feature(
        cv2.blur(frame.rgb_image, (5, 5)) if blur else frame.rgb_image,
        color_space=color_space)

    return frame, features


def load_sample1_dataset() -> FTBDataset:
    """
    Load the sample1 RGB-D set.
    The RGB-D dataset is a small segment of the scene 045 from SceneNN dataset.

    Returns:
        Loaded dataset.
    """
    return load_ftb(Path(__file__).parent / "../test-data/rgbd/sample1")


def get_sample1_matchings() -> Tuple[int, torch.Tensor, int, torch.Tensor]:
    """
    Get 2D coordinates from two frames of the sample1 dataset that are correspondent.

    Returns:
        A 4-tuple with frame index 1, y-x points 1, index 2, y-x points 2.
    """
    return (0, torch.tensor([[272, 476],
                             [292, 62],
                             [84, 511],
                             [292, 252],
                             [438, 215]], dtype=torch.int64),
            14, torch.tensor([[255, 471],
                              [264, 59],
                              [67, 511],
                              [270, 248],
                              [413, 210]], dtype=torch.int64))


def load_sample2_dataset():
    """
    Load the sample2 RGB-D set.
    The RGB-D dataset is a small segment of the a scene from the ReplicaDataset.

    Returns:
        Loaded dataset.
    """
    return load_ftb(Path(__file__).parent / "../test-data/rgbd/sample2")


def load_sample3_dataset():
    """
    Load the sample3 RGB-D set if available.
    This scene is larger than the other ones so it's not available yet.

    Returns:
        Loaded dataset.
    """
    path = Path(__file__).parent / "../test-data/rgbd/sample3"
    if not path.exists():
        raise RuntimeError("The sample3 dataset isn't available yet")
    return load_ftb(Path(__file__).parent / "../test-data/rgbd/sample3")


def get_device() -> str:
    """Get the device from the `SLAMTB_DEVICE` environment variable.
    Returns "cpu" if no variable is set.

    Returns:
        Pytorch's device name.
    """
    return os.environ.get("SLAMTB_DEVICE", "cpu")
