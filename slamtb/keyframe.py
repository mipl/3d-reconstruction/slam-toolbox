"""
Keyframe detection algorithms.
"""


class IntervalKeyFrameDetector:
    """
    Simplest key frame detector that yield a key frame by a fixed interval count.

    Attributes:

        interval_size: Number of frames to yield a new keyframe
    """

    def __init__(self, interval_size: int):
        self.interval_size = interval_size
        self._keyframe_count = 0

    def is_keyframe(self, frame_count, *_, **__) -> bool:
        """
        Returns whatever a new keyframe should be output.
        """
        if (frame_count + 1) % self.interval_size == 0:
            self._keyframe_count += 1
            return True
        return False

    @property
    def keyframe_count(self) -> int:
        """
        Returns the current keyframe count.
        """
        return self._keyframe_count

    def get_frame_keyframe(self, frame_num: int) -> int:
        """
        Returns the first keyframe that a given frame is after,
            or if it's a keyframe, then return itself.

        Args:
            frame_num: The query frame number.

        Returns:
            The keyframe that is before `frame_num`.
        """
        return frame_num // self.interval_size
